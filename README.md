# matlabJapc

# Documentation
A useful page for documention/information is:
https://wikis.cern.ch/display/ST/Matlab-CO+interaction 

# how to start MATLAB
The present matlab.sh, matlabWin.cmd start an instance of 
MATLAB for AFS (or locally in Windows)
loading some JAPC related jars, and some matlab libraries to Get, Set
and Subscribe parameters available in the CERN accelerator complex.

## NOTE for Windows users:
If you install this library on windows computers, remember to substitute the 
javaclasspath.txt file with the provided javaclasspathWindows.txt.

# Disclaimer
All the libraries are provided "as is", and no support or warranties are
provided. 

davide.gamba@cern.ch

