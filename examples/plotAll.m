function plotAll( data )
%PLOTALL just a simple callback function that plots all the signal arriving

%plot((data.headerCycleStamps - data.headerCycleStamps(1))*10^-9)
data.seqNumber
%return


auxSignals = data.parameters;
disp('----------------------')
clf
subplot(2,1,1)
hold all
for i=1:length(auxSignals)
    auxData = matlabDataAndSignalsHelper.simpleExtractSingleSignal(data, auxSignals{i});
    auxName = matlabDataAndSignalsHelper.decomposeSignal(auxSignals{i});
    if ~isempty(auxData)
        if length(auxData) > 1
            plot(auxData,'DisplayName', auxName);
        elseif (isnan(auxData))
            auxError = matlabDataAndSignalsHelper.simpleExtractSingleSignalError(data, auxSignals{i});
            disp(['Error for ' auxSignals{i} ' = ' auxError])
        else
            disp(['Value for ' auxSignals{i} ' = ' num2str(auxData)])
        end
    else
        disp(['Missing ', auxName])
    end
end
hold off
legend SHOW
title(unixtime2HumanTime(data.headerCycleStamps(1)*10^-9))

subplot(4,1,3)
plot((data.headerCycleStamps - data.headerCycleStamps(1))*10^-9)
title('Cyclestamps')
ylabel('[s]')
subplot(4,1,4)
plot((data.headerTimeStamps - data.headerTimeStamps(1))*10^-9,'r')
ylabel('[s]')
title('Timestamps')


end

