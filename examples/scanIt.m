classdef scanIt<handle
    properties
        myIndex
        myVector
        mySelector
        myParameter
    end    
    methods        
        function obj=scanIt(mySelector,myParameter,myArray,myIndex)
            obj.myIndex=myIndex;
            obj.myVector=myArray;
            obj.mySelector=mySelector;
            obj.myParameter=myParameter;
        end            
        function varyIt(obj)
            i= obj.myIndex;
            if i>length(obj.myVector)
                disp('Scan ended!')
            else
                obj.myVector(i)
                obj.myIndex=obj.myIndex+1;
                matlabJapc.staticSetSignal(obj.mySelector,obj.myParameter, obj.myVector(i))
            end     
        end      
    end
end

