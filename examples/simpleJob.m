% Davide - Sep 2022
%    =Outdated= simple example
%    now returning after doing a GET and plot


matlabJapc.staticINCAify('CTF')
samplerSamples = 'CK.PKI15A-ST/Samples#samples';
mySelector = 'SCT.USER.SETUP';
auxArray = matlabJapc.staticGetSignal(mySelector,samplerSamples);
plot(auxArray,'sk-');
return

%%
close all
clc
clear all
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Po_Rs422 example

signalToGet = 'CB.BHB1100/Acquisition#currentAverage';
signalToSet = 'CB.BHB1100/SettingPPM#current';
mySelector = 'SCT.USER.SETUP';

% get them
auxValue1 = matlabJapc.staticGetSignal(mySelector,signalToGet)
auxValue2 = matlabJapc.staticGetSignal(mySelector,signalToSet)

% set it
%matlabJapc.staticSetSignal(mySelector,signalToSet, auxValue2)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% POW-V example

signalToGet = 'CL.QDA0415/AQN#value'
signalToSet = 'CL.QDA0415/CCV#value'
mySelector = 'SCT.USER.SETUP';

% get themfalse
auxValue1 = matlabJapc.staticGetSignal(mySelector,signalToGet)
auxValue2 = matlabJapc.staticGetSignal(mySelector,signalToSet)
% set it
%matlabJapc.staticSetSignal(mySelector,signalToSet, auxValue2)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% xeneric sampler 
samplerSamples = 'CL.SABPE0125S/Samples#samples';
mySelector = 'SCT.USER.SETUP';

auxArray = matlabJapc.staticGetSignal(mySelector,samplerSamples);
plot(auxArray,'sk-');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% xeneric sampler complete
samplerSamples = 'CL.SABPE0125S/Samples#samples';
samplerSamplingTrain = 'CL.SABPE0125S/Samples#samplingTrain';
samplerFirstSampleTime = 'CL.SABPE0125S/Samples#firstSampleTime';
samplerTimeUnitFactor = 'CL.SABPE0125S/Samples#timeUnitFactor';
samplerUnit = 'CL.SABPE0125S/Unit#unit';
mySelector = 'SCT.USER.SETUP';

% create a JAPC interface class
myJAPC=matlabJapc(mySelector);
% get all the data
mySamplerData = myJAPC.JGetAll({samplerSamples, ...
    samplerSamplingTrain, ...
    samplerFirstSampleTime, ...
    samplerTimeUnitFactor, ...
    samplerUnit});
% do the plot
auxData = mySamplerData.CL.SABPE0125S.Samples.samples.value;
auxNPoints = length(auxData);
auxTimeLine = ((1:auxNPoints)-1);
auxTimeLine = auxTimeLine .* mySamplerData.CL_SABPE0125S.Samples.samplingTrain.value;
auxTimeLine = auxTimeLine +  mySamplerData.CL_SABPE0125S.Samples.firstSampleTime.value;

plot(auxTimeLine, auxData,'sk-');
ylabel(['[',char(mySamplerData.CL_SABPE0125S.Unit.unit.value),']']);
xlabel(['[',num2str(mySamplerData.CL_SABPE0125S.Samples.timeUnitFactor.value),' s]'])
title('CL.SABPE0125S')
grid on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% oasis simple 
oasisSamples = 'CL.SCOPE02.CH01/Acquisition#value';
mySelector = 'SCT.USER.SETUP';

auxArray = matlabJapc.staticGetSignal(mySelector,oasisSamples);
plot(auxArray,'g-');
set(gca,'Color','k');
set(gcf,'Color','k');
set(gca,'XColor','w');
set(gca,'YColor','w');
grid on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% oasis complete 
oasisSamples = 'CL.SCOPE02.CH01/Acquisition#value';
oasisSamplingTrain = 'CL.SCOPE02.CH01/Acquisition#sampleInterval';
oasisFirstSampleTime = 'CL.SCOPE02.CH01/Acquisition#firstSampleTime';
oasisUnit = 'CL.SCOPE02.CH01/Acquisition#value_units';
oasisSensitivity = 'CL.SCOPE02.CH01/Acquisition#sensitivity';
% not useful here, but important
oasisYscale = 'CL.SCOPE02.CH01/Sensibility#value'; % full Y scale
oasisOffsetGet = 'CL.SCOPE02.CH01/Acquisition#offset'; % offset in Y
oasisOffsetSet = 'CL.SCOPE02.CH01/Offset#value'; % offset in Y
oasisXscale= 'CL.SCOPE02/TimeBase#value'; % full time scale
oasisXdelay = 'CL.SCOPE02/Delay#value'; % offset in time
oasisStandby = 'CL.SCOPE02/Standby#value'; % start/stop scope (boolean)
mySelector = 'SCT.USER.SETUP';

% create a JAPC interface class
myJAPC=matlabJapc(mySelector);
% get all the data
myOasisData = myJAPC.JGetAll({oasisSamples, ...
    oasisSamplingTrain, ...
    oasisFirstSampleTime, ...
    oasisUnit, ...
    oasisSensitivity});false
% do the plot
auxData = myOasisData.CL.SCOPE02.CH01.Acquisition.value.value;
auxData = double(auxData) .* myOasisData.CL_SCOPE02_CH01.Acquisition.sensitivity.value;
auxNPoints = length(auxData);
auxTimeLine = ((1:auxNPoints)-1);
auxTimeLine = auxTimeLine .* myOasisData.CL_SCOPE02_CH01.Acquisition.sampleInterval.value;
auxTimeLine = auxTimeLine +  myOasisData.CL_SCOPE02_CH01.Acquisition.firstSampleTime.value;


plot(auxTimeLine, auxData,'g-');
ylabel(['[',char(myOasisData.CL_SCOPE02_CH01.Acquisition.value_units.value),']']);
xlabel('[ns]') % not found in FESA, to be checked.
title('CL.SCOPE02.CH01','Color','w')


set(gca,'Color','k');
set(gcf,'Color','k');
set(gca,'XColor','w');
set(gca,'YColor','w');
grid on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% WaveFormGenerator example -> similar to CGAFG
close all
waveFormSignalToGetAndSet = 'CK.WFGMKS15/Profile#profile';
waveFormSignalToGetAndSetSize = 'CK.WFGMKS15/Profile#profileSize';
mySelector = 'SCT.USER.SETUP';

auxArray = matlabJapc.staticGetSignal(mySelector,waveFormSignalToGetAndSet);
auxSize = matlabJapc.staticGetSignal(mySelector,waveFormSignalToGetAndSetSize);
% warning: the length of auxArray is always 5000 points
disp(['Full array length = ',num2str(length(auxArray))]);
% but only this size are interesting
disp(['Interesting values ',num2str(auxSize)]);

auxTimeLine = auxArray(1:2:2*auxSize);
auxValues = auxArray(2:2:2*auxSize);
plot(auxTimeLine,auxValues,'r-');

% virtually one could set it as
%matlabJapc.staticSetSignal(mySelector,waveFormSignalToGetAndSet,auxArray);
% where one has to give the same array structure/length as given by the
% get.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% magnet subscription 
signalsToMonitor = {'CL.QDA0415/AQN#value', ...
    'CB.BHB1100/Acquisition#currentAverage'};
mySelector = 'SCT.USER.SETUP';

myMonitor = matlabMonitor(mySelector, ...
    signalsToMonitor, ...
    @(data)disp('New data!'), ... % external function to call (now null)
    'two magnets'... % comment
    );
myMonitor.saveDataPath = fullfile('./');
myMonitor.saveData = false;
%% start
myMonitor.start
%% stop
myMonitor.stop

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% subscription with plot 
samplerSamples = {'CL.SABPE0125S/Samples#samples'};
mySelector = 'SCT.USER.SETUP';

myMonitor = matlabMonitor(mySelector, ...
    samplerSamples, ...
    @(data)plot(data.CL_SABPE0125S.Samples.samples.value), ... % external function to call
    'a Xeneric Sampler'... % comment
    );
myMonitor.saveDataPath = fullfile('./');
myMonitor.saveData = false;
%% start
myMonitor.start
%% stop
myMonitor.stop

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% magnet subscription
signalsToMonitor = {'CP.QFC0210/AQN#value','CP.QDC0215/AQN#value'};
mySelector = 'SCT.USER.SETUP';

myMonitor = matlabMonitor(mySelector, ...
    signalsToMonitor, ...
    @(data)disp('New data!'), ... % external function to call (now null)
    'A scan'... % comment
    );
myMonitor.saveDataPath = fullfile('./');
myMonitor.saveData = true;
%%
myScan=scanIt(mySelector,'CP.QDC0205/CCV#value',[9.6 9.6 9.6 8.6 8.6 8.6 7.6 9.6],1)
addlistener(myMonitor, 'newBeam', @(h,e) myScan.varyIt)
%% start
myMonitor.start
%% stop
myMonitor.stop

