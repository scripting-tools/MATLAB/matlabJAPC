
% small test script of matlabSolver class
% The class is the core of the linearFeedback project, and it is used to
% solve linear systems with limits/weights

nObservables = 1;
nExcitors = 20;
nRandomExcitations = 30;
noiseOnObservablesLevel = 1.5;
noiseOnExcitationsLevel = 1.5;
nTeoreticalExcitors = 10; % it should be like nExcitors, but one might not know it!
nFaultyShots = 10; %round(nRandomExcitations); % 10%

% generate a real 
realRM = 2*rand(nObservables, nExcitors) - 1;

% generate some excitations
realExcitations = randn(nExcitors, nRandomExcitations);
choosenExcitorsIdx = 1:nTeoreticalExcitors;
if nTeoreticalExcitors < nExcitors
    % don't excite all the excitors, but only a subset of them
    choosenExcitorsIdx = randperm(nExcitors);
    choosenExcitorsIdx = choosenExcitorsIdx(1:nTeoreticalExcitors);
    auxIdx = true(nExcitors,1);
    auxIdx(choosenExcitorsIdx) = false;
    realExcitations(auxIdx, :) = 0;
end
%
% compute real outcome
realOutcome = realRM*realExcitations;

% generate some noise for observables
realObservationsNoise = noiseOnObservablesLevel*randn(nObservables, nRandomExcitations);

% compute data available to scientist:
readObservations = realOutcome+realObservationsNoise;
% same for excitations:
readExcitations = realExcitations;
if nTeoreticalExcitors > nExcitors
    % add some other random excitors to read excitation that actually don't
    % participate in the system
    readExcitations = [readExcitations; ...
        randn(nTeoreticalExcitors-nExcitors,nRandomExcitations)];
end

% add noise to excitations
realExcitationNoise = noiseOnExcitationsLevel*randn(size(readExcitations));
readExcitations = readExcitations + realExcitationNoise;

% add faulty shots
if nFaultyShots > 0
    disp('add fauly shots')
    readExcitations(:,(end+1):(end+nFaultyShots)) = 10*randn(size(readExcitations,1), nFaultyShots);
    readObservations(:,(end+1):(end+nFaultyShots)) = 10*randn(size(readObservations,1), nFaultyShots);
end

% here start working with solver

% create solver object
mySolver = matlabSolver(nObservables, nTeoreticalExcitors);

% try to identify system without regulariztion
mySolver.improve(readObservations, readExcitations(choosenExcitorsIdx,:));
computedRM = mySolver.responseMatrix;
computedRMerrors = mySolver.responseMatrixErrors;

% compare RM
subplot(3,2,2)
plot(realRM(1,:),'x-')
hold on
errorbar(choosenExcitorsIdx, computedRM(1,:),computedRMerrors(1,:),'o')
hold off
grid
xlabel('Observables idx')
title('RM comparison. dashed is the learned one','FontSize',14)

% try to use regularization
nSteps = 40;
minLambda = 0;
maxLambda = 20;
nIterationsPerStep = 100;

[bestLambda, myCostsTrain, myCostsCv] = mySolver.findBestRegularizationWeight(minLambda, maxLambda, nSteps, nIterationsPerStep);
subplot(3,2,3)
plot(linspace(minLambda,maxLambda,nSteps),myCostsTrain)
hold on
plot(linspace(minLambda,maxLambda,nSteps),myCostsCv,'r')
plot([bestLambda,bestLambda],ylim,'r--')
hold off
xlabel('\lambda')
ylabel('J [au]')
legend('Training set', 'Control set')

% create solver2  object
% and try to identify system with regulariztion
mySolver2 = matlabSolver(nObservables, nTeoreticalExcitors);
disp(['Using learningRegolarizationWeight = ',num2str(bestLambda)]);
mySolver2.learningRegolarizationWeight = bestLambda;
%mySolver2.learningRegolarizationWeight = 100;

mySolver2.improve(readObservations, readExcitations(choosenExcitorsIdx,:));
computedRM2 = mySolver2.responseMatrix;
computedRMerrors2 = mySolver2.responseMatrixErrors;

% compare RM
subplot(3,2,4)
plot(realRM(1,:),'x-')
hold on
errorbar(choosenExcitorsIdx,computedRM2(1,:),computedRMerrors2(1,:),'o')
hold off
grid
xlabel('Observables idx')
title('RM comparison with regularztion. dashed is the learned one','FontSize',14)


% try to reconstruct data
auxObservations = computedRM*readExcitations(choosenExcitorsIdx,:);
auxObservations2 = computedRM2*readExcitations(choosenExcitorsIdx,:);

subplot(3,2,5)
plot(realOutcome(1,:),'b--')
hold on
plot(readObservations(1,:),'bo-')
plot(auxObservations(1,:),'rx')
hold off

subplot(3,2,6)
plot(realOutcome(1,:),'b--')
hold on
plot(readObservations(1,:),'bo-')
plot(auxObservations2(1,:),'rx')
hold off
title('with reg')

figure(1)
