#!/bin/bash
#
# This script generates the javaclasspath.txt file for Matlab from the ctf-datamonitor project -> PRO release.
#
# - Modification to force using the new slf4j version
# Davide Gamba - 20/02/2020


echo '#'
echo '# automatically-generated javaclasspath.txt'
echo '# by generateJavaClassPath.sh script'
echo '# '
echo "# date $(date)"

echo -e "\n\n"
echo -e "# CERN libraries"

## default
ls -l /user/pcrops/ap/deployments/applications/cern/ctf/ctf-datamonitor/PRO/lib | grep jar$ | cut -f 2 -d '>' -s | tr -d ' ' | grep -v '/slf4j/'
## temporary version before LS release
#ls -l /user/pcrops/ap/deployments-dev/applications/cern/ctf/ctf-datamonitor/PRO/lib/ | grep jar$ | cut -f 2 -d '>' -s | tr -d ' ' | grep -v slf4j-log4j12


echo -e "\n\n"
echo -e "#Those libraries must be put at the beginning of the MATLAB Static Java Path."
echo -e "<before>"
ls -l /user/pcrops/ap/deployments/applications/cern/ctf/ctf-datamonitor/PRO/lib | grep jar$ | cut -f 2 -d '>' -s | tr -d ' ' | grep '/slf4j/'





