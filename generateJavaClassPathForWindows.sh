#!/bin/bash
#
# This script generates the javaclasspathWindows.txt file content for Matlab from the ctf-datamonitor project -> PRO release.
# It also copies to the specified directory the necessary jars.
#
# Davide Gamba - 20/05/2015



# GET input directory from user
if [ "$#" -ne 1 ]; then
  echo "Please specify an existing directory where to copy the jars!"
  exit 1
fi
# check that it is a directory
if ! [[ -d $1 ]]; then
  echo "$1 is not a valid directory where to copy jars!"
  exit 1
fi

# get current version of library
VERSION=`cat $(dirname $0)/VERSION`

echo '#'
echo '# automatically-generated javaclasspath.txt'
echo '# by generateJavaClassPath.sh script'
echo '# '
echo "# date $(date)"
echo "# matlabJapc version $VERSION"

echo -e "\n\n"
echo -e "# CERN libraries"


# get the list of all jars needed
ALL_JARS=`ls -l /user/pcrops/dist/ctf/ctf-datamonitor/PRO/lib | grep jar$ | cut -f 2 -d '>' -s | tr -d ' ' | grep -v slf4j-log4j12.jar`

# copy files in the given directory
for i in $ALL_JARS; do 
  cp $i $1
  aux= echo '\\cern.ch\dfs\Users\c\ctf3op\JavaCoInterface2_'$VERSION'\jars\'$(echo $i | awk -F/ '{print $NF}')
done

#
#echo -e "# ctf-datamonitor itself (even if obsolete!)"
#cp /user/pcrops/dist/ctf/ctf-datamonitor/PRO/build/dist/ctf-datamonitor/ctf-datamonitor.jar $1
#echo '\\cern.ch\dfs\Users\c\ctf3op\JavaCoInterface2_'$VERSION'\jars\ctf-datamonitor.jar'

#
echo -e "# local libraries"
echo '\\cern.ch\dfs\Users\c\ctf3op\JavaCoInterface2_'$VERSION'\jars_local\matlabJapcMonitorListener.jar'


