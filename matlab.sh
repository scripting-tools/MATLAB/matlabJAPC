#!/bin/sh
# 
# Starts matlab with the correct classpath to be used with dataMonitor.jar
# 
# davide.gamba@cern.ch 21-Jan-2016


# use local machine java instead of matlab one
MATLAB_JAVA="/usr/java/jdk/jre"
export MATLAB_JAVA

# go to the current directory, where javaclasspath.txt file is
cd `dirname $0`
# start matlab from AFS
/afs/cern.ch/project/parc/bin/matlab $@
