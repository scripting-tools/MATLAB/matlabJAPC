classdef linkedMatrixReadable < handle
    %
    %
    % Davide Gamba - Guido Sterbini
    % feb 2013
    

    properties
        myLink
        myMatrix
        handle
        myListener
    end
    
    methods
        % constructor
        function obj=linkedMatrixReadable(currentAxes,myLink,myMatrix,myEventForRedrawing)            
            auxx = myLink.(myMatrix);
            
            % imagesc is an horible code... I need to do this to preseve
            % the ButtonDownFcn of the parent axes
            % - davideg Jan 2015
            auxButtonDownBefore = get(currentAxes, 'ButtonDownFcn');
            obj.handle=imagesc(auxx,'Parent',currentAxes);
            set(currentAxes, 'ButtonDownFcn', auxButtonDownBefore);
            
            obj.myLink=myLink;
            obj.myMatrix=myMatrix;
            
            obj.myListener=addlistener(myLink,myEventForRedrawing,@obj.redraw);
            
            % define buttonDownFunction a bit complex...
            set(obj.handle,'ButtonDownFcn',@obj.myButtonDownFcn);
        end
               
        function redraw(obj,h,e)
            aux=[ h.(obj.myMatrix) ];
            
            if ishandle(obj.handle)
                set(obj.handle,'CData',aux);
            else
                warning(['Looks like I lost the handle to my axes for ',obj.myMatrix])
                disp('Removing listener...');
                delete(obj.myListener);
            end

            if ~isempty(aux)
                set(get(obj.handle,'Parent'),'xLim',[0.5 size(aux, 2)+.5])
                set(get(obj.handle,'Parent'),'yLim',[0.5 size(aux, 1)+.5])
                set(get(obj.handle,'Parent'),'YDir','reverse')
            end
        end
        
        function myButtonDownFcn(obj, hObject, eventdata, handles)
            auxMyParent = get(obj.handle,'Parent');
            set(obj.handle,'UIContextMenu',get(auxMyParent,'UIContextMenu'));
            auxMyParentFunction = get(auxMyParent,'ButtonDownFcn');
            auxMyParentFunction(auxMyParent, eventdata);
        end
        
        function delete(obj)
           delete(obj.myListener);
        end
        
    end
end