classdef linkedPlotReadable < handle
    %
    % Handles of a polygonal line
    %
    % Davide Gamba - Guido Sterbini
    % feb 2013

    properties
        myLink
        myLinkY
        handle
        x
        myListener
    end
    
    methods
        % constructor
        function obj=linkedPlotReadable(myAxes,myLink,myLinkY,myEventForRedrawing)            
            %obj=linkedPlotReadable(myAxes,myLink,myLinkY,myEventForRedrawing)
            
            y=myLink.(myLinkY);
            auxx=1:length(y);
            
            if isempty(y)
                obj.handle=plot([NaN],'Parent',myAxes);
            else
                obj.handle=plot(auxx', y','Parent',myAxes);
            end
            obj.myLink=myLink;
            
            obj.myLinkY=myLinkY;
            obj.x=auxx;
            
            obj.myListener=addlistener(myLink,myEventForRedrawing,@obj.redraw);
        end

        function redraw(obj,h,e)
            aux=[ h.(obj.myLinkY) ];
            try
                if ishandle(obj.handle)
                    set(obj.handle,'YData',aux,'XData',1:length(aux));
                else
                    warning(['Looks like I lost the handle to my axes for ',obj.myLinkY])
                    disp('Removing listener...');
                    delete(obj.myListener);
                end
            catch exc
                error(['linkedPlotReadable:: Impossible to redraw: ', exc.message])
            end
        end
        
        function delete(obj)
           delete(obj.myListener);
        end
        
        function setColor(obj, color)
            set(obj.handle,'Color',color);
        end
        
        function setLineStyle(obj, style)
            set(obj.handle,'LineStyle',style);
        end
        
        function setLineWidth(obj, width)
            set(obj.handle,'LineWidth',width);
        end
        
        function setDisplayName(obj, name)
            set(obj.handle,'DisplayName',name);
        end
        
    end
end