classdef linkedPlotReadableWithError < handle
    %
    % Handles of a polygonal line
    %
    % Davide Gamba - Guido Sterbini
    % feb 2013

    properties
        myLink
        myLinkY
        myLinkYError
        handle
        myx
        myListener
    end
    
    methods
        % constructor
        function obj=linkedPlotReadableWithError(myAxes, myLink, myLinkY, myLinkYError, myEventForRedrawing)
            %obj=linkedPlotReadableWithError(myAxes, myLink, myLinkY, myLinkYError, myEventForRedrawing)
            
            y=myLink.(myLinkY);
            x=1:length(y);
            e=myLink.(myLinkYError);
            
            if isempty(y)
                obj.handle = errorbar([NaN], [NaN],'Parent',myAxes);
            else
                obj.handle = errorbar(x', y', e','Parent',myAxes);
            end
            
            obj.myLink  = myLink;
            obj.myLinkY = myLinkY;
            obj.myLinkYError = myLinkYError;
            
            obj.myx = x;
            
            obj.myListener=addlistener(myLink,myEventForRedrawing,@obj.redraw);
        end
        
        function redraw(obj,h,e)
            aux = [ h.(obj.myLinkY) ];
            auxError = [ h.(obj.myLinkYError) ];
            try
                if ishandle(obj.handle)
                    set(obj.handle,'YData',aux,'XData',1:length(aux),'UData',auxError,'LData',auxError);
                else
                    warning(['Looks like I lost the handle to my axes for ',obj.myLinkY])
                    disp('Removing listener...');
                    delete(obj.myListener);
                end
            catch exc
                error(['linkedPlotReadableWithError::Impossible to redraw: ',exc.message])
            end
        end
        
        function delete(obj)
           delete(obj.myListener);
        end
        
        function setColor(obj, color)
            set(obj.handle,'Color',color);
        end
        
        function setLineStyle(obj, style)
            set(obj.handle,'LineStyle',style);
        end
        
        function setLineWidth(obj, width)
            set(obj.handle,'LineWidth',width);
        end
        
        function setDisplayName(obj, name)
            set(obj.handle,'DisplayName',name);
        end
    end
    
end
