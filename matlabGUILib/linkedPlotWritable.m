classdef linkedPlotWritable < impoly
    %
    % Handles of a polygonal line
    %
    % Davide Gamba - Guido Sterbini
    % feb 2013
    
    properties
        myLink
        myLinkY
        x
        myListener
        myAxes
    end
    
    methods
        % constructor
        function obj=linkedPlotWritable(myAxes,myLinkObject, myLinkYfield, myEventForRedrawing)
            auxy=[myLinkObject.(myLinkYfield)];
            auxx=1:length(auxy);
            obj@impoly(myAxes,[auxx(:) auxy(:)])
            obj.x=auxx(:);
            obj.myLink=myLinkObject;
            obj.myLinkY=myLinkYfield;
            obj.myAxes=myAxes;
            
            obj.setClosed(0);
            obj.setPositionConstraintFcn(@obj.myPointFiltering)
            % for debug, just comment this!
            obj.myListener=addlistener(myLinkObject, myEventForRedrawing, @obj.redraw);
            
            %%% update context menus
            % remove default context menus
            obj.api.setVertexContextMenu('');
            obj.api.setContextMenu('');
            
            % try to retrive current figure handler form axes
            auxFigH = myAxes;
            while ~isgraphics(auxFigH, 'Figure')
                try
                    auxFigH = get(auxFigH,'Parent');
                catch e
                    auxFigH = [];
                    break
                end
            end
            
            if ~isempty(auxFigH)
                % create a new vertex menu
                myVertexMenu = uicontextmenu('Parent', auxFigH);
                uimenu(myVertexMenu, 'Label','Set Point Value', 'Callback', @obj.mySetValue);
                obj.api.setVertexContextMenu(myVertexMenu)
                
                % create a new line menu
                myLineMenu = uicontextmenu('Parent', auxFigH);
                uimenu(myLineMenu, 'Label','Set Line Value', 'Callback', @obj.mySetLineValue);
                obj.api.setContextMenu(myLineMenu);
            else
                disp('linkedPlotWritable: Impossible to find figure handle.. no context menu added.');
            end
        end
        
        
        function redraw(obj,h,e)
            % for debug
            %disp(['redraw for ',obj.myLinkY])
            
            % Jan 2014 -> why this is needed?
            %aux=get(get(obj,'Parent'),'yLim');
            %set(get(obj,'Parent'),'yLim',aux);
            
            myY = [h.(obj.myLinkY)];
            myX = 1:length(myY);
            aux=[myX(:) myY(:)];
            
            obj.setPosition(aux);
        end
        
        function setDisplayName(obj, name)
            set(obj,'DisplayName',name);
            aux = get(obj,'Annotation');
            if isempty(name)
                aux.LegendInformation.IconDisplayStyle = 'off';
            else
                aux.LegendInformation.IconDisplayStyle = 'on';
            end
        end
        
        function delete(obj)
            delete(obj.myListener);
        end
    end
    
    methods(Access=protected)
        function mySetValue(obj, hObject, event)
            auxCurrentX = get(obj.myAxes,'CurrentPoint');
            auxCurrentX = round(auxCurrentX(1));
            auxCurrentPosition = obj.getPosition();
            
            % prompt for new position
            prompt={['Enter new value for idx = ',num2str(auxCurrentX)]};
            name='Input value';
            numlines=1;
            defaultanswer={num2str(auxCurrentPosition(auxCurrentX,2))};
            auxReply = inputdlg(prompt,name,numlines,defaultanswer);
            if ~isempty(auxReply)
                auxCurrentPosition(auxCurrentX,2) = str2double(char(auxReply));
                obj.setPosition(auxCurrentPosition);
                % update also link position
                obj.myLink.(obj.myLinkY) = auxCurrentPosition(:,2);
            end
        end
        
        function mySetLineValue(obj, hObject, event)
            auxCurrentPosition = obj.getPosition();
            % prompt for new position
            prompt={'Enter new value for all points'};
            name='Input value';
            numlines=1;
            defaultanswer={num2str(mean(auxCurrentPosition(:,2)))};
            auxReply = inputdlg(prompt,name,numlines,defaultanswer);
            if ~isempty(auxReply)
                auxCurrentPosition(:,2) = str2double(char(auxReply));
                obj.setPosition(auxCurrentPosition);
                % update also link position
                obj.myLink.(obj.myLinkY) = auxCurrentPosition(:,2);
            end
        end
        
        
        function x = myPointFiltering(obj,x)
            % why?! davideg Mar 2015
            %aux=get(get(obj,'Parent'),'yLim');
            %set(get(obj,'Parent'),'yLim',aux)
            
            % for the x to stay the same!
            x=[obj.x x(:,2)];
            aux=x(:,2);
            obj.myLink.(obj.myLinkY) = aux;
            
            % why?! davideg Jan 2015
            %aux=get(get(obj,'Parent'),'xLim');
            %set(get(obj,'Parent'),'xLim',aux)
        end
    end
end