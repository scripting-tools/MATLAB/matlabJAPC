classdef linkedPlotXYReadable < handle
    %
    % Handles of a polygonal line
    %
    % Davide Gamba - Guido Sterbini
    % feb 2014
    
    properties
        myLink
        myLinkY
        myLinkX
        handle
        myListener
    end
    
    methods
        % constructor
        function obj=linkedPlotXYReadable(myAxes,myLink,myLinkX,myLinkY,myEventForRedrawing)
            y=myLink.(myLinkY);
            x=myLink.(myLinkX);
            
            if isempty(y)
                obj.handle=plot([NaN],'Parent',myAxes);
            elseif numel(x) ~= numel(y)
                disp(['Something wrong with x,y dimensions of ',...
                    myLink,' x=',myLinkX,' y=',myLinkY,...
                    ]);
                obj.handle=plot([NaN],'Parent',myAxes);
            else
                obj.handle=plot(x', y','Parent',myAxes);
            end
            obj.myLink=myLink;
            
            obj.myLinkY=myLinkY;
            obj.myLinkX=myLinkX;
            
            obj.myListener=addlistener(myLink,myEventForRedrawing,@obj.redraw);
        end
        
        function redraw(obj,h,e)
            auxX = [ h.(obj.myLinkX) ];
            auxY = [ h.(obj.myLinkY) ];
            try
                if ishandle(obj.handle)
                    set(obj.handle,'YData',auxY,'XData',auxX);
                else
                    warning(['Looks like I lost the handle to my axes for ',obj.myLinkY])
                    disp('Removing listener...');
                    delete(obj.myListener);
                end
            catch exc
                error(['linkedPlotXYReadable::Impossible to redraw: ',exc.message])
            end
        end
        
        function delete(obj)
            delete(obj.myListener);
        end
        
    end
end
