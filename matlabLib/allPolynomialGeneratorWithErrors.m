function [valueOutput, errorOutput] = allPolynomialGeneratorWithErrors( values, valuesErrors, maxDegree, addZeroDegree)
%ALLPOLI Summary of this function goes here
%   Detailed explanation goes here

% some reshaping for convenience
sizes=size(values);
nvalues=sizes(1)*sizes(2);
myvalues=reshape(values,nvalues,1);
myvalueserrors=reshape(valuesErrors,nvalues,1);

% check format
if ~isnumeric(myvalues) && ~iscellstr(myvalues)
    error('Unknown format of values. Only arrays are supported')
end

if nargin == 4 && logical(addZeroDegree)
    % degree zero is the simplest = 1;
    valueOutput(1) = 1;
    errorOutput(1) = 0;
    outputindex = 1;
else
    outputindex = 0;
end


% work on the degrees > 0
for degree=1:maxDegree
    % create matrix of my permutations...
    mytmpmat=repmat(myvalues,1,degree);
    
    mytmpXindices=ones(1,degree);
    lineIndex=0;
    lines(1,1:degree)=zeros(1,degree);
    linesNumEl(1,1:nvalues)=zeros(1,nvalues); % used to calculate error
    while true
        lineIndex=lineIndex+1;
        lines(lineIndex,:)=mytmpXindices;
        for iNumEl = 1:nvalues
            linesNumEl(lineIndex,iNumEl) = numel(find(mytmpXindices == iNumEl));
        end
        
        % define mytmpXindeces for the next line
        mytmpXindices(end)=mytmpXindices(end)+1;
        for tmpDegree=degree:-1:2
            if mytmpXindices(tmpDegree) > nvalues
                mytmpXindices(tmpDegree-1) = mytmpXindices(tmpDegree-1) + 1;
                for tmpCol=tmpDegree:degree
                    mytmpXindices(tmpCol)=mytmpXindices(tmpDegree-1);
                end
            end
        end
        if mytmpXindices(1) > nvalues
            break
        end
    end
    
    % very good! all my values at this degree are defined as multiplication
    % of the elements:
    %   mytmpmat(lines(i),1:nvalues)
    % for each i of the 'lines' table
    for i=1:lineIndex
        outputindex=outputindex+1;
        tmpValue=1;
        for j=1:degree
            tmpValue=tmpValue*mytmpmat(lines(i,j),j);            
        end
        valueOutput(outputindex) = tmpValue;
        
        % now the error
        tmpValue = 0;
        for j=1:nvalues
            if (values(j) == 0)
                continue
            end
            tmpValue=tmpValue+power(myvalueserrors(j)*linesNumEl(i,j)/values(j),2);
        end
        errorOutput(outputindex) = abs(sqrt(tmpValue)*valueOutput(outputindex));
    end
end

end

