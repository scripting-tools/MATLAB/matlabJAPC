function output = getNanoSecondsFromEpoch(inputGMTDate, inputGMTDateFormat)
% output = getNanoSecondsFromEpoch(inputDate[, inputGMTDateFormat]);
%
% It returns the the [ns] from 1st Jan 1970 to the given date
% (GMT). The given date has to be either a number like
% the one coming from MATLAB "now" command (e.g. 7.3612e+05 = days from 0 Jan 0?)
% either a string coming from MATLAB "datestr" command (e.g.
% '03-Jun-2015 11:32:33' or '06-03-2015' for the same date)
% One can also specify the dateFormat of the input using the same
% conventions of datestr function.
%
% NB: be carefull in choosing a GMT/UTC time! ("now" will give you
% the local time!)
%
% if no inputDate is given, the current GMT/UTC time is used.

if nargin == 0
    output = java.lang.System.currentTimeMillis*1e6;
elseif nargin == 1
    output = etime(datevec(inputGMTDate), [1970,1,1,0,0,0])*1e9;
elseif nargin == 2
    output = etime(datevec(inputGMTDate, inputGMTDateFormat), [1970,1,1,0,0,0])*1e9;
end

end



