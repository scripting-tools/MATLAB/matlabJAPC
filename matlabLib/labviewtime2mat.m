function matlab_time = labviewtime2mat(labviewTime_time)
% matlab_time = labviewtime2mat(labviewTime_time)
%
% it converts a labview timestamp (seconds for 1st Jan 1904 12:00 a.m.)
% into a Matlab serial date number (decimal days since Jan 1 0000)

labviewTime = datenum(1904,1,1,0,0,0);
matlab_time = labviewTime+labviewTime_time/86400;
