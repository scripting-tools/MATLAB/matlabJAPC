classdef matlabDataAndSignalsHelper
    %MATLABDATASIGNALHELPER Collection of Static methods fo JAPC classes
    %   the static methods defined in this class are usefull to play with
    %   data coming from the matlabJapc classes...
    
    properties
    end
    
    methods
    end
    
    methods(Static)
        function [devName, propName, tagName] = decomposeSignal(signal)
            % from a full signal name like: signal = 'device/property#tag'
            % it will extract:
            %    devName = device
            %    propName = property
            %    tagName = tag
            % tagName can be '' if no tag is present in the signal
            
            myStrings = regexp(signal, '/', 'split');
            devName=myStrings{1};
            if (length(myStrings) > 1)
                myStrings=regexp(myStrings{2}, '#', 'split');
                propName=myStrings{1};
                if (length(myStrings) > 1)
                    tagName = myStrings{2};
                else
                    tagName = '';
                end
            else
                propName='';
                tagName='';
            end
        end
        
        %
        function output = simpleExtractArray(dataStruct,signals)
            %output = simpleExtractArray(dataStruct,signals)
            % general useful function to treat data...
            % for each signal it will extract its value from data structure.
            % if the value is an array, it will take the mean.
            output = zeros(length(signals),1);
            for i=1:length(signals)
                output(i) = mean(matlabDataAndSignalsHelper.simpleExtractSingleSignal(dataStruct,signals(i)));
            end
        end
        
        %
        
        function output = convertSignalToStructPath(signal)
            %output = convertSignalToStructPath(signal)
            % it converts a signal like "device/property#field" in
            % "device.property.field"
            output=strrep(signal,'://','_');
            output=strrep(output,'-','_');
            output=strrep(output,'.','_');
            output=strrep(output,'/','.');
            output=strrep(output,'#','.');
        end
        
        %
        
        function output = simpleExtractSingleSignal(dataStruct, signal)
            %output = simpleExtractSingleSignal(dataStruct, signal)
            % it extracts from a dataStruct the value of the requested signal
            myStr = matlabDataAndSignalsHelper.convertSignalToStructPath(char(signal));
            % get data
            myStr = strsplit(myStr,'.');
            %output = getfield(dataStruct, myStr{:}, 'value');
            switch length(myStr)
                case 1
                    output = dataStruct.(myStr{1}).value;
                case 2
                    output = dataStruct.(myStr{1}).(myStr{2}).value;
                case 3
                    output = dataStruct.(myStr{1}).(myStr{2}).(myStr{3}).value;
                case 4
                    output = dataStruct.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).value;
                case 5
                    output = dataStruct.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).(myStr{5}).value;
                otherwise
                    error(['Signal ' signal ' has a too long/short structure. Not supported! Write to davide.gamba@cern.ch!'])
            end
        end
        
        %
        function output = simpleExtractSingleSignalError(dataStruct, signal)
            %output = simpleExtractSingleSignalError(dataStruct, signal)
            % it extracts from a dataStruct the "error" of the requested signal
            myStr = matlabDataAndSignalsHelper.convertSignalToStructPath(char(signal));
            % get data
            myStr = strsplit(myStr,'.');
            switch length(myStr)
                case 1
                    output = dataStruct.(myStr{1}).error;
                case 2
                    output = dataStruct.(myStr{1}).(myStr{2}).error;
                case 3
                    output = dataStruct.(myStr{1}).(myStr{2}).(myStr{3}).error;
                case 4
                    output = dataStruct.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).error;
                case 5
                    output = dataStruct.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).(myStr{5}).error;
                otherwise
                    error(['Signal ' signal ' has a too long/short structure. Not supported! Write to davide.gamba@cern.ch!'])
            end
        end
        
        %
        function outputDataset = simpleSetvalue2Struct(signalName, signalNewValue, inputDataset)
            %outputDataset = simpleSetvalue2Struct(signalName, signalNewValue, inputDataset)
            % it will add or replace to the provided inputDataset (must be a struct) the new signalNewValue
            % for the signalName signal
            if (nargin > 2 && isstruct(inputDataset) )
                outputDataset = inputDataset;
            else
                warning('matlabDataAndSignalsHelper:: No good inputDataset provided (it must be a struct). Creating a new one.');
                outputDataset = struct;
            end
            
            myStr = matlabDataAndSignalsHelper.convertSignalToStructPath(char(signalName));
            % insert data
            myStr = strsplit(myStr,'.');
            switch length(myStr)
                case 1
                    outputDataset.(myStr{1}).value = signalNewValue;
                case 2
                    outputDataset.(myStr{1}).(myStr{2}).value = signalNewValue;
                case 3
                    outputDataset.(myStr{1}).(myStr{2}).(myStr{3}).value = signalNewValue;
                case 4
                    outputDataset.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).value = signalNewValue;
                case 5
                    outputDataset.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).(myStr{5}).value = signalNewValue;
                otherwise
                    error(['Signal ' signalName ' has a too long/short structure. Not supported! Write to davide.gamba@cern.ch!'])
            end
        end
        
        %
        function outputDataset = simpleSetvalueError2Struct(signalName, signalNewError, inputDataset)
            %outputDataset = simpleSetvalueError2Struct(signalName, signalNewError, inputDataset)
            % it will add or replace to the provided inputDataset (must be
            % a struct) the new error field (signalNewError) for the parameter idefiend by signalName
            %
            
            if (nargin > 2 && isstruct(inputDataset) )
                outputDataset = inputDataset;
            else
                warning('matlabDataAndSignalsHelper:: No good inputDataset provided (it must be a struct). Creating a new one.');
                outputDataset = struct;
            end
            
            myStr = matlabDataAndSignalsHelper.convertSignalToStructPath(char(signalName));
            % insert data
            myStr = strsplit(myStr,'.');
            switch length(myStr)
                case 1
                    outputDataset.(myStr{1}).error = signalNewError;
                case 2
                    outputDataset.(myStr{1}).(myStr{2}).error = signalNewError;
                case 3
                    outputDataset.(myStr{1}).(myStr{2}).(myStr{3}).error = signalNewError;
                case 4
                    outputDataset.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).error = signalNewError;
                case 5
                    outputDataset.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).(myStr{5}).error = signalNewError;
                otherwise
                    error(['Signal ' signalName ' has a too long/short structure. Not supported! Write to davide.gamba@cern.ch!'])
            end
        end
        
        
        %
        function outputDataset = simpleSetAll2Struct(signalName, data, inputDataset)
            %outputDataset = simpleSetAll2Struct(signalName, data, inputDataset)
            % it will add or replace into the provided inputDataset (must
            % be a struct) the new data provided for the parameter idefiend
            % by signalName.
            %   "data" must be a structure with the following fields:
            %  value -> the matlab value of the parameter.
            %  error -> a string representative of the error if any.
            %  headerTimeStamp  \
            %  headerCycleStamp   -> Additional informations
            %  headerSelector   /
            %
            if (nargin > 2 && isstruct(inputDataset) )
                outputDataset = inputDataset;
            else
                warning('matlabDataAndSignalsHelper:: No good inputDataset provided (it must be a struct). Creating a new one.');
                outputDataset = struct;
            end
            
            myStr = matlabDataAndSignalsHelper.convertSignalToStructPath(char(signalName));
            % insert data
            myStr = strsplit(myStr,'.');
            switch length(myStr)
                case 1
                    outputDataset.(myStr{1}).value = data.value;
                    outputDataset.(myStr{1}).timeStamp = data.headerTimeStamp;
                    outputDataset.(myStr{1}).cycleStamp = data.headerCycleStamp;
                    outputDataset.(myStr{1}).cycleName = data.headerSelector;
                    outputDataset.(myStr{1}).error = data.error;
                case 2
                    outputDataset.(myStr{1}).(myStr{2}).value = data.value;
                    outputDataset.(myStr{1}).(myStr{2}).timeStamp = data.headerTimeStamp;
                    outputDataset.(myStr{1}).(myStr{2}).cycleStamp = data.headerCycleStamp;
                    outputDataset.(myStr{1}).(myStr{2}).cycleName = data.headerSelector;
                    outputDataset.(myStr{1}).(myStr{2}).error = data.error;
                case 3
                    outputDataset.(myStr{1}).(myStr{2}).(myStr{3}).value = data.value;
                    outputDataset.(myStr{1}).(myStr{2}).(myStr{3}).timeStamp = data.headerTimeStamp;
                    outputDataset.(myStr{1}).(myStr{2}).(myStr{3}).cycleStamp = data.headerCycleStamp;
                    outputDataset.(myStr{1}).(myStr{2}).(myStr{3}).cycleName = data.headerSelector;
                    outputDataset.(myStr{1}).(myStr{2}).(myStr{3}).error = data.error;
                case 4
                    outputDataset.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).value = data.value;
                    outputDataset.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).timeStamp = data.headerTimeStamp;
                    outputDataset.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).cycleStamp = data.headerCycleStamp;
                    outputDataset.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).cycleName = data.headerSelector;
                    outputDataset.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).error = data.error;
                case 5
                    outputDataset.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).(myStr{5}).value = data.value;
                    outputDataset.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).(myStr{5}).timeStamp = data.headerTimeStamp;
                    outputDataset.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).(myStr{5}).cycleStamp = data.headerCycleStamp;
                    outputDataset.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).(myStr{5}).cycleName = data.headerSelector;
                    outputDataset.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).(myStr{5}).error = data.error;
                otherwise
                    error(['Parameter ' char(signalName) ' has a too long/short structure. Not supported! Write to davide.gamba@cern.ch!'])
            end
        end
        
        function parametersList = filterSetParameterList(parametersList, oldDataStruct, newDataStruct)
            %parametersList = filterSetParameterList(parametersList, oldDataStruct, newDataStruct)
            % it will filter the parameter list, removing parameters that
            % didn't change value from the previous set.
            for i=length(parametersList):-1:1
                oldValue = matlabDataAndSignalsHelper.simpleExtractSingleSignal(oldDataStruct, parametersList{i});
                newValue = matlabDataAndSignalsHelper.simpleExtractSingleSignal(newDataStruct, parametersList{i});
                
                if oldValue == newValue
                    parametersList(i)=[];
                end
            end
        end
    end
end

