
classdef matlabJapc < handle
    %MATLABJAPC JAPC interface for linearFeedback and general porpuse.
    %
    % It allows to perform Get/Set of Accelerators Parameters through the
    % CERN JAPC interface
    %
    % Davide Gamba - Guido Sterbini
    % Jun 2014
    % LS2 baseline update in Dec 2018
    %   http://artifactory.cern.ch/ls2-baseline-release/cern/japc/japc/7.0.10/japc-7.0.10-javadoc.jar!/index.html
    %   http://artifactory.cern.ch/ls2-baseline-release/cern/japc/japc-value/7.0.10/japc-value-7.0.10-javadoc.jar!/index.html
    
    % %%%%%%%%%%%%%%%%%%%%
    properties
        % just what you expect
        verbose = false;
        
        % if true, it will not set anything.. just print to console what
        % it wants to do
        safemode = false;
    end
    
    % %%%%%%%%%%%%%%%%%%%%
    properties (Access=protected)
        % cycle used for data acquisition
        cycleName
        % Japc parameter factory
        parameterFactory
        parameterSelector
        parameterSelectorAdapter
        
        % object from where I read references
        % OUTDATED - JAPC-994
        % referenceService
        
        % matlabJapcExtractor instance. I just create it once forever!
        myExtractor = matlabJapcExtractor();
        
        % if true, it will return JAPC values instead of MATLAB-translate values:
        getJavaPlaneObjects = false;
    end
    
    % %%%%%%%%%%%%%%%%%%%%
    methods
        function obj=matlabJapc(cycleName, dataFilterStruct)
            %matlabJapc(cycleName, dataFilter)) class constructor.
            % cycleName (default = []) is a string in the form like 'SCT.USER.SETUP', i.e.
            %   'accelerator.USER.period'
            % dataFilterStruct (default = []) if specified, it is supposed to be
            %   either a value or a matlab structure from which construct the
            %   dataFilter necessary for the acquisition of the data
            %   required.
            
            % default values
            if nargin < 2
                dataFilterStruct = [];
            end
            if nargin < 1
                cycleName = [];
            end
            
            %
            % some general initializations
            obj.parameterFactory = cern.japc.core.factory.ParameterFactory.newInstance();
            
            % for references reading/setting % OUTDATED - JAPC-994
            %obj.referenceService = cern.japc.ext.dirservice.RefArchive.getImpl();
            
            % create selector
            obj.privateSetSelector(cycleName, dataFilterStruct);
        end
        
        function setSelector(obj, cycleName, dataFilterStruct)
            %setSelector(obj, cycleName, dataFilterStruct)
            % cycleName (default = []) is a string in the form like 'SCT.USER.SETUP', i.e.
            %   'accelerator.USER.period'
            % dataFilterStruct (default = []) if specified, it is supposed to be
            %   either a value or a matlab structure from which construct the
            %   dataFilter necessary for the acquisition of the data
            %   required.
            
            % default values
            if nargin < 3
                dataFilterStruct = [];
            end
            if nargin < 2
                cycleName = [];
            end
            
            % Just call the private method to re-initialize selector
            obj.privateSetSelector(cycleName, dataFilterStruct)
        end
        
        function delete(obj)
            % delete(obj)
            % it deletes the object...
            % actually it does nothing at the moment...
        end
        
        %%
        function setGetJavaPlaneObjects(obj, setValue)
            %setGetJavaPlaneObjects(obj, setValue)
            % If you set it to true, then it will return the acquired
            % Simple values as JAPC value objects.
            % for MAP values, it will return a structure with inside the
            % set of JAPC values corresponding to the various fields.
            obj.getJavaPlaneObjects = logical(setValue);
        end
        function value = isGetJavaPlaneObjects(obj)
            %value = isGetJavaPlaneObjects(obj)
            % It returns true if the object is set such to return JAPC
            % value objects instead of MATLAB-converted values.
            value = obj.getJavaPlaneObjects;
        end
        
        
        %%
        function JSetAll(obj, setSignals, setValuesDataset)
            %JSetAll(obj, setSignals, setValuesDataset)
            % It sets the new values included in the setValuesDaset structure to all setSignals signals
            % The setValuesDataset structure is supposed to be compatible with the one used in the class
            % matlabJapcMonitor!!!
            % It might be convenient to use some static methods from the
            % "matlabDataAndSignalsHelper" class.
            %
            warning('matlabJapc: JSetAll is DEPRECATED - use JSetSignals instead');
            
            obj.JSetSignals( setSignals, setValuesDataset);
        end
        
        %%
        function JSetSignals(obj, setSignals, setValuesDataset)
            %JSetSignals(obj, setSignals, setValuesDataset)
            % It sets the new values included in the setValuesDaset structure to all setSignals (cell array) signals
            % The setValuesDataset structure is supposed to be compatible with the one used in the class
            % matlabJapcMonitor!!!
            % It might be convenient to use some static methods from the
            % "matlabDataAndSignalsHelper" class to create this Dataset...
            %
            % WARNING! If there is an error in setting a parameter, a warning will
            % be provided, but the process will continue for the other
            % parameters!
            %
            
            % convert single char signal to cell
            if ischar(setSignals)
                setSignals = {setSignals};
            end
            
            for i=1:length(setSignals)
                %
                try
                    newError = matlabDataAndSignalsHelper.simpleExtractSingleSignalError(setValuesDataset, setSignals{i});
                catch e
                    newError = '';
                    obj.printOut(['The given data structure doesn''t have the error field for ', setSignals{i}, '.', char(10), ...
                        'This might be unsafe, cause it might try to set faulty values!']);
                end
                
                if ~isempty(newError)
                    warning(['There was an error in the acquired value of ', setSignals{i}, '.', char(10), ...
                        '   so I will NOT set its value beacuse it might be faulty!']);
                else
                    try
                        newValue = matlabDataAndSignalsHelper.simpleExtractSingleSignal(setValuesDataset, setSignals{i});
                        obj.JSetFesaSignal(setSignals{i}, newValue);
                    catch eSet
                        warning(['Impossible to SET ', setSignals{i}, ' (but I will continue with the others):', char(10), ...
                            eSet.getReport])
                    end
                end
            end
        end
        
        %%
        function JSetFesa(obj, deviceName, propertyName, valueTag, newValue)
            %JSetFesa(obj, deviceName, propertyName, valueTag, newValue)
            % function to set a new value to a device/property#tag fesa device.
            % valueTag can be empty.
            % If you are trying to set a full property (with many fields),
            % newValue has to be a struct with all the fields you want to
            % set. The other fields will be kept as the current value.
            
            if (isempty(valueTag))
                obj.JSetFesaSignal([deviceName,'/',propertyName], newValue);
            else
                obj.JSetFesaSignal([deviceName,'/',propertyName,'#',valueTag], newValue);
            end
        end
        
        function JSetFesaSignal(obj, signal, newValue)
            %JSetFesaSignal(obj, signal, newValue)
            % function to set a new value to a device/property[#tag] fesa signal.
            % If you are trying to set a full property
            %   (i.e. a MAP parameter with many fields),
            % newValue has to be a struct with all the fields you want to
            % set. The other fields will be kept as the current value.
            
            % if safe mode.. no set!
            if (obj.safemode)
                try
                    disp(['matlabJapc: I would like to (but not!) set ',signal,' => ', num2str(newValue')]);
                catch e
                    disp(['matlabJapc: I would like to (but not!) set something in ',signal,'.']);
                end
                return
            end
            
            % get the data from the machine as Java object
            auxParameterHandle          = obj.parameterFactory.newParameter(signal);
            auxParameterValueDescriptor = auxParameterHandle.getValueDescriptor();
            
            % create a new java value based on the value descriptor
            JNewValue = obj.myExtractor.JInsertMatlabValue(newValue, auxParameterValueDescriptor);
            
            % Mar 2019: use selector helper to fallback to "noSelector", if required. 
            auxSelector = obj.parameterSelectorAdapter.to(auxParameterHandle);
            
            % do actual set %%%%%%%%%%%
            auxParameterHandle.setValue(auxSelector, JNewValue);
            %%%%%%%%%%%
        end
        
        function JSetRefFesa(obj, deviceName, propertyName, valueTag, newValue)
            %JSetRefFesa(obj, deviceName, propertyName, valueTag, newValue)
            % function to set a new value to a device/property[#tag]
            % reference, i.e. the value you normaly see in the workingset
            % as 'Ref'
            
            if (isempty(valueTag))
                %valueTag = 'value';
                obj.JSetRefFesaSignal([deviceName,'/',propertyName], newValue);
            else
                obj.JSetRefFesaSignal([deviceName,'/',propertyName,'#',valueTag], newValue);
            end
        end
        
        function JSetRefFesaSignal(obj, signal, newValue)
            error('Outdated function. Please contact davide.gamba@cern.ch')
            
            %JSetRefFesaSignal(obj, signal, newValue)
            % similar to JSetRefFesa(obj, deviceName, propertyName, valueTag, newValue)
            
            % create parameter handle and description
            auxParameterHandle = obj.parameterFactory.newParameter(signal);
            auxParameterValueDescriptor = auxParameterHandle.getValueDescriptor();
            % create a new java value based on the value descriptor
            JNewValue = obj.myExtractor.JInsertMatlabValue(newValue, auxParameterValueDescriptor);
            
            % prepare something to be sent to reference database
            JNewParameter = cern.japc.core.spi.FailSafeParameterValueImpl(signal, JNewValue);
            referenceLineNumber = cern.japc.ext.tgm.TgmUtil.cycleName2LineNumber(obj.cycleName);
            
            % OUTDATED - JAPC-994
            %obj.referenceService.setValue(JNewParameter, referenceLineNumber, obj.cycleName, true);
        end
        
        
        function MValue = JGetFesaSignal(obj, signal, completeDataStruct)
            %MValue = JGetFesaSignal(obj, signal [, completeDataStruct = false])
            % function to get a value from a device/property[#tag] fesa signal.
            % If you are trying to get a full property
            %   (i.e. a MAP parameter with many fields),
            % the output value will be a struct with all the fields of your MAP
            % parameter.
            % If you specify completeDataStruct = true, then the output
            % will be always a complex structure with inside the following
            % fields:
            %  parameter -> is the name of the acquired parameter (i.e. signal)
            %  value -> the matlab value of the acquired parameter
            %  error -> a string representative of the error if any.
            %  headerTimeStamp  \
            %  headerCycleStamp   -> Informations coming from value header.
            %  headerSelector   /
            
            if (nargin < 3)
                completeDataStruct = false;
            end
            
            % get the data from the machine as Java object
            auxParameterHandle = obj.parameterFactory.newParameter(signal);
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            if (completeDataStruct)
                
                % get data from JAPC
                try
                    % Mar 2019: use selector helper to fallback to "noSelector", if required.
                    auxSelector = obj.parameterSelectorAdapter.to(auxParameterHandle);
                    
                    auxParameterValue = auxParameterHandle.getValue(auxSelector);
                    % Test Dec 2018
                    if obj.isGetJavaPlaneObjects()
                        warning('Test Davide Dec 2018')
                        auxValue = auxParameterValue.getValue();
                    else
                        auxValue = obj.myExtractor.JExtractMatlabValue(auxParameterValue.getValue());
                    end
                    auxError = '';
                    
                    % extract time stamps data from header (if possible)
                    auxHeader = auxParameterValue.getHeader();
                    try
                        auxHeaderTimeStamp = int64(auxHeader.getAcqStamp());
                    catch e
                        obj.printOut(['Impossible to extract HeaderTimeStamp from ',signal,' ', char(10), e.getReport]);
                        auxHeaderTimeStamp = NaN;
                    end
                    try
                        auxHeaderCycleStamp = int64(auxHeader.getCycleStamp());
                    catch e
                        obj.printOut(['Impossible to extract HeaderCycleStamp from ',signal,' ', char(10), e.getReport]);
                        auxHeaderCycleStamp = NaN;
                    end
                    try
                        auxHeaderSelector = char(auxHeader.getSelector().toString());
                    catch e
                        obj.printOut(['Impossible to extract HeaderSelector from ',signal,' ', char(10), e.getReport]);
                        auxHeaderSelector = e.getReport;
                    end
                    
                catch e
                    auxValue = NaN(1);
                    auxError = e.getReport;
                    auxHeaderCycleStamp = NaN;
                    auxHeaderTimeStamp = NaN;
                    auxHeaderSelector = '';
                    obj.printOut(['No value for: ',signal,' error:',char(13),...
                        auxError]);
                end
                
                % generate output structure
                MValue = struct;
                MValue.parameter = signal;
                MValue.value = auxValue;
                MValue.error = auxError;
                MValue.headerTimeStamp = auxHeaderTimeStamp;
                MValue.headerCycleStamp = auxHeaderCycleStamp;
                MValue.headerSelector = auxHeaderSelector;
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            else % simpler case where one wants only the value (or an error)
                try
                    % Mar 2019: use selector helper to fallback to "noSelector", if required.
                    auxSelector = obj.parameterSelectorAdapter.to(auxParameterHandle);
                    auxParameterValue = auxParameterHandle.getValue(auxSelector).getValue();
                catch e
                    error(['Error acquiring: ',signal,':',char(10), e.getReport]);
                end
                % convert it to matlab values
                % Test Dec 2018
                if obj.isGetJavaPlaneObjects()
                    warning('Test Davide Dec 2018')
                    MValue = auxParameterValue;
                else
                    MValue = obj.myExtractor.JExtractMatlabValue(auxParameterValue);
                end
            end
        end
        
        function MValue = JGetFesa(obj, deviceName, propertyName, valueTag)
            %MValue = JGetFesa(obj, deviceName, propertyName, valueTag)
            % function to get a value from a deviceName/propertyName[#valueTag] fesa signal.
            % If you are trying to get a full property
            %   (i.e. a MAP parameter with many fields),
            % the output will be a struct with all the fields of your MAP
            % parameter.
            
            if (nargin == 2 || isempty(valueTag))
                MValue = obj.JGetFesaSignal([deviceName,'/',propertyName]);
            else
                MValue = obj.JGetFesaSignal([deviceName,'/',propertyName,'#',valueTag]);
            end
        end
        
        function MValue = JGetRefFesa(obj, deviceName, propertyName, valueTag)
            %MValue = JGetRefFesa(obj, deviceName, propertyName, valueTag)
            % function to get the reference value from a
            % device/property[#tag], i.e. the value you normaly see in the workingset
            % as 'Ref'
            
            if (nargin == 2 || isempty(valueTag))
                MValue = obj.JGetRefFesaSignal([deviceName,'/',propertyName]);
            else
                MValue = obj.JGetRefFesaSignal([deviceName,'/',propertyName,'#',valueTag]);
            end
        end
        
        function MValue = JGetRefFesaSignal(obj, signal)
            error('Outdated function. Please contact davide.gamba@cern.ch')

            %MValue = JGetRefFesaSignal(obj, signal)
            % similar to JGetRefFesa(obj, deviceName, propertyName, valueTag)
            
            referenceLineNumber = cern.japc.ext.tgm.TgmUtil.cycleName2LineNumber(obj.cycleName);
            % OUTDATED - JAPC-994
            %JParameterValue = obj.referenceService.getValue(signal, referenceLineNumber, obj.cycleName);
            
            if ~isempty(JParameterValue.getException)
                error(['Error acquiring: ',signal,':',char(JParameterValue.getException().getMessage())]);
            end
            JValue = JParameterValue.getValue();
            
            % convert it as Matlab type..
            if obj.isGetJavaPlaneObjects()
                MValue = JValue;
            else
                MValue = obj.myExtractor.JExtractMatlabValue(JValue);
            end
        end
        
        
        function outputStruct = JGetAll(obj, getSignals)
            %outputStruct = JGetAll(obj, getSignals)
            % it get the current value for every signal specified in
            % getSignals. The outputStruct is a standard matlabJapcMonitor
            % structure that can be passed to JSetSignals.
            %
            % [DEPRECATED] - use JGetSignals instead.
            
            outputStruct = obj.JGetSignals(getSignals);
        end
        
        function outputStruct = JGetSignals(obj, getSignals)
            %outputStruct = JGetSignals(obj, getSignals)
            % it get the current value for every signal specified in
            % getSignals. The outputStruct is a compatible with a matlabJapcMonitor
            % structure that can be passed to JSetSignals.
            
            % convert single char signal to cell array
            if ischar(getSignals)
                getSignals = {getSignals};
            end
            
            outputStruct = struct;
            for i=1:length(getSignals)
                try
                    tmpData = obj.JGetFesaSignal(getSignals{i}, true);
                catch e
                    auxError = e.getReport();
                    warning(['Impossible to GET ', getSignals{i}, char(10), auxError]);
                    tmpData = struct;
                    tmpData.value = NaN;
                    tmpData.error = auxError;
                    tmpData.headerTimeStamp = -1;
                    tmpData.headerCycleStamp = -1;
                    tmpData.headerSelector = -1;
                end
                outputStruct = matlabDataAndSignalsHelper.simpleSetAll2Struct(getSignals{i}, tmpData, outputStruct);
            end
            % add some other useful parameters
            outputStruct.parameters = getSignals(:);
            outputStruct.cycleName = obj.cycleName;
        end
        
        % other functions to change behavior of this object
        function setGetEnumAsInt(obj, setValue)
            %setGetEnumAsInt(obj, setValue)
            % If you set it to true, then it will return the ENUMs as their
            % own code instead of the string related to them.
            obj.myExtractor.setGetEnumAsInt(setValue);
        end
        function value = isGetEnumAsInt(obj)
            %value = isGetEnumAsInt(obj)
            % It returns true if the object is set such to return the ENUMs
            % as theirs code instead of the string related to them.
            value = obj.myExtractor.isGetEnumAsInt();
        end
    end % end public methods
    
    %% %%%%%%%%%%%%%%%%%%%%
    % generic static methods
    methods(Static)
        % some useful static methods to get/set values without having to
        % handle with a matlab object
        %
        function out = staticGet(cycleName, deviceName, propertyName, valueTag)
            %out = staticGet(cycleName, deviceName, propertyName, valueTag)
            % It makes a single GET of "deviceName/propertyName#valueTag"
            % and returns a matlab-converted value.
            % The get is done on the cycleName specified, e.g.
            % "SCT.USER.SETUP"
            
            tmpObj=matlabJapc(cycleName);
            out=tmpObj.JGetFesa(deviceName, propertyName, valueTag);
        end
        function out=staticGetSignal(cycleName, signal, completeDataStruct)
            %out = staticGetSignal(cycleName, signal [, completeDataStruct = false])
            % It makes a single GET of "signal" and returns a matlab-converted value.
            % The get is done on the cycleName specified, e.g.
            % "SCT.USER.SETUP"
            %
            % The optional true/false completeDataStruct is to be set true
            % in case you want to get a full data structure with all the
            % headers informations, i.e. acquisition error, timestamps...
            
            if nargin < 3
                completeDataStruct = false;
            end
            
            tmpObj=matlabJapc(cycleName);
            out=tmpObj.JGetFesaSignal(signal, completeDataStruct);
        end
        function out=staticGetAll(cycleName, getSignals)
            %out = staticGetAll(cycleName, getSignals)
            % [DEPRECATED] it get the current value for every signal specified in the
            % cell array getSignals.
            % The outputStruct is a standard matlabJapcMonitor structure.
            
            warning('This function has been renamed to staticGetSignals... Please use that name in the future.');
            out = matlabJapc.staticGetSignals(cycleName, getSignals);
        end
        function out=staticGetSignals(cycleName, getSignals)
            %out = staticGetSignals(cycleName, getSignals)
            % it get the current value for every signal specified in the
            % cell array getSignals.
            % The outputStruct is a standard matlabJapcMonitor structure.
            
            tmpObj=matlabJapc(cycleName);
            out=tmpObj.JGetSignals(getSignals);
        end
        function out = staticGetRef(cycleName, deviceName, propertyName, valueTag)
            %out = staticGetRef(cycleName, deviceName, propertyName, valueTag)
            % It gets the "Reference" value (if any) currently used for the
            % "deviceName/propertyName#valueTag".
            
            tmpObj=matlabJapc(cycleName);
            out=tmpObj.JGetRefFesa(deviceName, propertyName, valueTag);
        end
        function out = staticGetRefSignal(cycleName, signal)
            %out = staticGetRefSignal(cycleName, signal)
            % It gets the "Reference" value (if any) currently used for the
            % "signal".
            
            tmpObj=matlabJapc(cycleName);
            out=tmpObj.JGetRefFesaSignal(signal);
        end
        
        
        function staticSet( cycleName, deviceName, propertyName, valueTag, newValue)
            %staticSet( cycleName, deviceName, propertyName, valueTag, newValue)
            % It set "newValue" to "deviceName/propertyName#valueTag" on
            % the cycle defined by "cycleName".
            
            tmpObj=matlabJapc(cycleName);
            tmpObj.JSetFesa(deviceName, propertyName, valueTag, newValue);
        end
        function staticSetSignal( cycleName, signal, newValue)
            %staticSetSignal( cycleName, signal, newValue)
            % similar to staticSet, but here "signal" =
            % "deviceName/propertyName#valueTag" is a single string.
            
            tmpObj=matlabJapc(cycleName);
            tmpObj.JSetFesaSignal(signal, newValue);
        end
        function staticSetSignals(cycleName, setSignals, valueDataStruct)
            %out = staticSetSignals(cycleName, setSignals, valueDataStruct)
            % it sets the values store inside valueDataStruct to every signal specified in the
            % cell array setSignals.
            % The valueDataStruct can be standard matlabJapcMonitor
            % structure, or a struct coming from
            % staticGetSignals(cycleName, setSignals)
            
            tmpObj=matlabJapc(cycleName);
            tmpObj.JSetSignals(setSignals, valueDataStruct);
        end
        function staticSetRef(cycleName, deviceName, propertyName, valueTag, newValue)
            %staticSetRef(cycleName, deviceName, propertyName, valueTag, newValue)
            % It set the new Reference "newValue" for "deviceName/propertyName#valueTag"
            
            tmpObj=matlabJapc(cycleName);
            tmpObj.JSetRefFesa(signal, deviceName, propertyName, valueTag, newValue);
        end
        function staticSetRefSignal(cycleName, signal, newValue)
            %staticSetRefSignal(cycleName, signal, newValue)
            % similar to staticSetRef, but here "signal" =
            % "deviceName/propertyName#valueTag" is a single string.
            
            tmpObj=matlabJapc(cycleName);
            tmpObj.JSetRefFesaSignal(signal, newValue);
        end
        
        
        %%
        function staticINCAify(accelerator)
            %staticINCAify(accelerator)
            % INCAify the current Matlab instance. Accelerator can be skept, or
            % it can be any of 'AD','CTF','ISOLDE','LEIR','LHC','LINAC4',
            % 'NORTH','PS','PSB','SCT','SPS','ELENA'
            %
            % WARNING: this static method has to be run at MATLAB startup
            % before any JAPC command. If not, JAPC calls will go directly
            % to the hardware without passing by the INCA servers.
            %
            % WARNING-2: in order to fully profit of INCA/RBAC features,
            % one should also make a staticRBACAuthenticate(<user, <password>>)
            % if no username/password are provided, then the
            % LoginPolicy.LOCATION will be used (it should always work in control rooms).
            %
            
            % import CernAccelerator with def of InCA accelerators
            import cern.accsoft.commons.domain.CernAccelerator
            % create InCAconfigurator object
            InCAconfigurator = cern.japc.ext.inca.IncaConfigurator();
            
            if InCAconfigurator.isConfigured()
                warning(['This session of MATLAB is already configured for ', ...
                    char(InCAconfigurator.getAccelerator()), ...
                    ' InCA server. You cannot re-configure it!'])
                return
            end
            
            if nargin == 0
                accelerator = '';
            end
            switch accelerator
                case ''
                    auxaccelerator = '';
                case 'AD'
                    auxaccelerator = CernAccelerator.AD;
                case 'AWAKE'
                    auxaccelerator = CernAccelerator.AWAKE;
                case 'CTF'
                    auxaccelerator = CernAccelerator.CTF;
                case 'ELENA'
                    auxaccelerator = CernAccelerator.ELENA;
                case 'ISOLDE'
                    auxaccelerator = CernAccelerator.ISOLDE;
                case 'LEIR'
                    auxaccelerator = CernAccelerator.LEIR;
                case 'LHC'
                    auxaccelerator = CernAccelerator.LHC;
                case 'LINAC3'
                    auxaccelerator = CernAccelerator.LINAC3;
                case 'LINAC4'
                    auxaccelerator = CernAccelerator.LINAC4;
                case 'NORTH'
                    auxaccelerator = CernAccelerator.NORTH;
                case 'PS'
                    auxaccelerator = CernAccelerator.PS;
                case 'PSB'
                    auxaccelerator = CernAccelerator.PSB;
                case 'REX'
                    auxaccelerator = CernAccelerator.REX;
                case 'SPS'
                    auxaccelerator = CernAccelerator.SPS;
                otherwise
                    error(['matlabJapc:: unknown accelerator: ', accelerator]);
            end
            InCAconfigurator.configure(auxaccelerator);

            % disable dirservice
            disp(['By default, if you incaify your application, I disable the ',...
                'use of CCS to retrieve the parameter descriptors. If you really need it, then re-enable it ',...
                'with the matlabJapc.staticUseDirservice(true);']);
            matlabJapc.staticUseDirservice(false);

        end
        
        function staticUseDirservice(setValue)
            % matlabJAPC 2.1.2 re-enables this option using japc-svc-ccs
            %    matlabJapc.staticUseDirservice(true);
            
            if nargin == 0
                disp('matlabJapc::staticUseDirservice: called without arguments. Enabling CCS lookup');
                setValue = true;
            end
            
            if setValue
                java.lang.System.setProperty('japc.ccs.descriptors.enabled','true');
            else
                java.lang.System.clearProperty('japc.ccs.descriptors.enabled');
            end
        end

        function staticUseFgc(setValue)
            error('Outdated function. Please contact davide.gamba@cern.ch')
            
            % Since JAPC 3.0.0 the japc-ext-fgc is outdated and not
            % used anymore by default.
            % I don't know what this is for, but in case you need it, you
            % would need to run at MATLAB startup(!!):
            %    matlabJapc.staticUseFgc(true);
            
            if nargin == 0
                disp('matlabJapc::staticUseFgc: called without arguments. enabling it');
                setValue = true;
            end
            
            if setValue
                java.lang.System.setProperty('japc.fgc.desc.provider.enabled','true');
            else
                java.lang.System.clearProperty('japc.fgc.desc.provider.enabled');
            end
        end
        
        function staticRBACAuthenticate(~)
            %token = staticRBACAuthenticate([explicit])
            % it executes the RBAC authentication to access/set protected
            % parameters.
            % If no input is specify, it will try to login by LOCATION.
            % Otherwise the standard Java Dialog provided by rbac-client
            % will pop up and will ask for username/password...
            %
            
            %
            % create RBAC authentication
            % for LS2 update see:
            %   https://wikis.cern.ch/display/MW/RBAC+API+Renovation+for+Start-LS2#RBACAPIRenovationforStart-LS2-RBACJavaclientAPI
            try
                if nargin == 0
                    %%% Possible way:
                    %%token = cern.rbac.client.authentication.AuthenticationClient.create().loginLocation();
                    %%% add token to client holder:
                    %%cern.rbac.util.holder.ClientTierTokenHolder.setRbaToken(token);
                    %
                    %%% Better? (so I can specify application name)
                    cern.rbac.util.authentication.LoginServiceBuilder.newInstance().applicationName('matlabJAPC').loginPolicy(cern.rbac.common.authentication.LoginPolicy.LOCATION).build().loginNewUser();
                elseif nargin == 1
                    token = cern.rbac.client.authentication.LoginDialog([], 'matlabJAPC').login();
                    %
                    % Could be done like this: but better to use CO-provided tools...
                    %                 rbacService = cern.rbac.client.authentication.AuthenticationClient.create();
                    %                 % ask for a password and try to do explicit login
                    %                 try
                    %                     auxPassword=logindlg('Title',['RBAC(',username,')'],'Password','only');
                    %                     if isempty(auxPassword)
                    %                         error('You did not provided a password.')
                    %                     end
                    %                     token = rbacService.loginExplicit(username, auxPassword);
                    %                     auxPassword = [];
                    %                     clear(auxPassword);
                    %                 catch e
                    %                     auxPassword = [];
                    %                     clear(auxPassword);
                    %                     error(['staticRBACAuthenticate:: Something went wrong:',char(10),e.getReport])
                    %                 end
                    
                    % add token to client holder:
                    cern.rbac.util.holder.ClientTierTokenHolder.setRbaToken(token);
                end
                
            catch e
                error(['staticRBACAuthenticate:: Something went wrong:',char(10),e.getReport])
            end
        end
        
        function staticRBACLogout()
            %staticRBACLogout()
            % it logs out the current RBAC user.
            try
                cern.rbac.util.authentication.LoginServiceBuilder.newInstance().applicationName('matlabJAPC').build().logout();
            catch e
                error(['staticRBACLogout:: Something went wrong:',char(10),e.getReport])
            end
        end
        
        function token = staticRBACGetToken()
            %   See https://wikis.cern.ch/display/MW/RBAC+API+Renovation+for+Start-LS2#RBACAPIRenovationforStart-LS2-RBACJavaclientAPI for LS2 update
            % staticRBACGetToken()
            % retrives the Java RBA token. Useful to understand if RBAC is working or not
            token = cern.rbac.util.lookup.RbaTokenLookup.findRbaToken();
        end
        
        function staticSetJavaDebug(value)
            %staticSetJavaDebug(value)
            % It changes the log level for som e java classes used by
            % matlabJapc. (in particular, japc).
            % This allows the user to eventually have even more debug
            % informations that are normally hidden.
            if nargin == 0
                value = 0;
            end
            if value > 0
                aux = org.apache.log4j.Logger.getLogger('cern.ctf.customjarsmatlab');
                aux.setLevel(org.apache.log4j.Level.DEBUG);
                aux = org.apache.log4j.Logger.getLogger('cern.japc');
                aux.setLevel(org.apache.log4j.Level.DEBUG);
            elseif value == 0
                aux = org.apache.log4j.Logger.getLogger('cern.ctf.customjarsmatlab');
                aux.setLevel(org.apache.log4j.Level.WARN);
                aux = org.apache.log4j.Logger.getLogger('cern.japc');
                aux.setLevel(org.apache.log4j.Level.WARN);
            else
                aux = org.apache.log4j.Logger.getLogger('cern.ctf.customjarsmatlab');
                aux.setLevel(org.apache.log4j.Level.ERROR);
                aux = org.apache.log4j.Logger.getLogger('cern.japc');
                aux.setLevel(org.apache.log4j.Level.ERROR);
            end
        end
        
        
        %%%
        %%%%%%%%%%%%%%%%%%%
    end
    
    % %%%%%%%%%%%%%%%%
    methods (Access=protected)
        % internally used function to display running informations
        function printOut(obj,text)
            if obj.verbose
                disp(text);
            end
        end
        
        % internally used function to setup data selector.
        function privateSetSelector(obj, cycleName, dataFilterStruct)
            %privateSetSelector(obj, cycleName, dataFilterStruct)
            % Private version of setSelector. Needed because
            % matlabJapcMonitor has to properly overload the setSelector
            % function without destroing everything.
            
            % import SelectorFactory
            import cern.japc.core.factory.SelectorFactory
            import cern.japc.core.Selectors

            % default values
            if nargin < 3
                dataFilterStruct = [];
            end
            if nargin < 2
                cycleName = [];
            end
            
            % generate dataFilter
            JDataFilter = obj.myExtractor.JGenerateDataFilter(dataFilterStruct);
            
            % create selector
            if isempty(cycleName)
                if isempty(JDataFilter)
                    obj.parameterSelector = Selectors.NO_SELECTOR;
                else
                    obj.parameterSelector = SelectorFactory.newSelector(JDataFilter);
                end
            else
                obj.parameterSelector = SelectorFactory.newSelector(cycleName, JDataFilter);
            end
            
            % March 2019 - create adapter
            obj.parameterSelectorAdapter = Selectors.adapt(obj.parameterSelector);
            
            %
            % store cycle to be used (in case there is a PULL.XXXX@, i.e. get
            % cycle, I will keep only the second part).
            tmp = regexp(cycleName,'@','split');
            if length(tmp) > 1
                obj.cycleName=tmp(2);
            else
                obj.cycleName=cycleName;
            end
        end
        
    end % end private methods
end

