classdef matlabJapcExtractor < handle
    %matlabJapcExtractor
    % This class is used to speed up the extraction of data from Java
    % objects.
    %
    % The main idea is that the check of the type is taking too much time,
    % so I use instead the hashCode given by the Type class.
    %
    % It also introduce the possibility to treat Enums as int instead of
    % string when acquired.
    %
    % LS2 baseline update in Dec 2018

    
    properties
        % from cern.japc.value.Type
        SIMPLE            = cern.japc.value.Type.SIMPLE.hashCode();
        MAP               = cern.japc.value.Type.MAP.hashCode();
        
        % from cern.japc.value.ValueType
        UNDEFINED         = cern.japc.value.ValueType.UNDEFINED.hashCode();
        BOOLEAN           = cern.japc.value.ValueType.BOOLEAN.hashCode();
        BYTE              = cern.japc.value.ValueType.BYTE.hashCode();
        DOUBLE            = cern.japc.value.ValueType.DOUBLE.hashCode();
        FLOAT             = cern.japc.value.ValueType.FLOAT.hashCode();
        INT               = cern.japc.value.ValueType.INT.hashCode();
        LONG              = cern.japc.value.ValueType.LONG.hashCode();
        SHORT             = cern.japc.value.ValueType.SHORT.hashCode();
        STRING            = cern.japc.value.ValueType.STRING.hashCode();
        BOOLEAN_ARRAY     = cern.japc.value.ValueType.BOOLEAN_ARRAY.hashCode();
        BYTE_ARRAY        = cern.japc.value.ValueType.BYTE_ARRAY.hashCode();
        DOUBLE_ARRAY      = cern.japc.value.ValueType.DOUBLE_ARRAY.hashCode();
        FLOAT_ARRAY       = cern.japc.value.ValueType.FLOAT_ARRAY.hashCode();
        INT_ARRAY         = cern.japc.value.ValueType.INT_ARRAY.hashCode();
        LONG_ARRAY        = cern.japc.value.ValueType.LONG_ARRAY.hashCode();
        SHORT_ARRAY       = cern.japc.value.ValueType.SHORT_ARRAY.hashCode();
        STRING_ARRAY      = cern.japc.value.ValueType.STRING_ARRAY.hashCode();
        BOOLEAN_ARRAY_2D  = cern.japc.value.ValueType.BOOLEAN_ARRAY_2D.hashCode();
        BYTE_ARRAY_2D     = cern.japc.value.ValueType.BYTE_ARRAY_2D.hashCode();
        DOUBLE_ARRAY_2D   = cern.japc.value.ValueType.DOUBLE_ARRAY_2D.hashCode();
        FLOAT_ARRAY_2D    = cern.japc.value.ValueType.FLOAT_ARRAY_2D.hashCode();
        INT_ARRAY_2D      = cern.japc.value.ValueType.INT_ARRAY_2D.hashCode();
        LONG_ARRAY_2D     = cern.japc.value.ValueType.LONG_ARRAY_2D.hashCode();
        SHORT_ARRAY_2D    = cern.japc.value.ValueType.SHORT_ARRAY_2D.hashCode();
        STRING_ARRAY_2D   = cern.japc.value.ValueType.STRING_ARRAY_2D.hashCode();
        ENUM              = cern.japc.value.ValueType.ENUM.hashCode();
        ENUM_SET          = cern.japc.value.ValueType.ENUM_SET.hashCode();
        DISCRETE_FUNCTION = cern.japc.value.ValueType.DISCRETE_FUNCTION.hashCode();
        DISCRETE_FUNCTION_LIST= cern.japc.value.ValueType.DISCRETE_FUNCTION_LIST.hashCode();
        ENUM_ARRAY        = cern.japc.value.ValueType.ENUM_ARRAY.hashCode();
        ENUM_SET_ARRAY    = cern.japc.value.ValueType.ENUM_SET_ARRAY.hashCode();
        ENUM_ARRAY_2D     = cern.japc.value.ValueType.ENUM_ARRAY_2D.hashCode();
        ENUM_SET_ARRAY_2D = cern.japc.value.ValueType.ENUM_SET_ARRAY_2D.hashCode();
    end
    
    properties (Access=private)
        % to get enum as Int, this variable has to be true.
        getEnumAsInt = false;
        

    end
    
    methods
        function obj=matlabJapcExtractor()
            % empty constructor
        end
        
        function MValue = JExtractMatlabValue(obj, JValue)
            %MValue = JExtractMatlabValue(JValue)
            % it extract all the fields from a MAP or the value from a SIMPLE parameter value
            %
            % see if JValue is a map or a simple object
            JValueTypeHash = JValue.getType().hashCode();
            if (JValueTypeHash == obj.SIMPLE)
                MValue = obj.JExtractSimpleMatlabValue(JValue);
            elseif (JValueTypeHash == obj.MAP)
                % since this is a big structure, I will give back a matlab
                % structure with all the fields inside.
                MValue = struct;
                
                % here the names of the fields
                auxFieldNames = JValue.getNames();
                
                % now extract them all
                for i=1:numel(auxFieldNames)
                    %%for DEBUG
                    %disp('^--------------V')
                    %disp(auxFieldNames(i))
                    auxJSingleValue = obj.JExtractSimpleMatlabValue(JValue.get(auxFieldNames(i)));
                    
                    % remove possible points from the fieldname (April 2018)
                    aux = strrep(char(auxFieldNames(i)),'.','_');
                    MValue.(aux) = auxJSingleValue;
                end
            else
                error('matlabJapcExtractor::unknown Java Value type. Probably an OBJECT -> not supported');
            end
        end % end JExtractMatlabValue static function
        
        function MValue = JExtractSimpleMatlabValue(obj, JValue)
            %MValue = JExtractSimpleMatlabValue(JValue)
            % extracts a matlab understandable value from a Java Simple
            % Parameter Value.
            
            % get JAPC value type
            JType = JValue.getValueType().hashCode();
            
            % properly extract old value
            if (JType == obj.DOUBLE_ARRAY || JType == obj.DOUBLE_ARRAY_2D)
                MValue = JValue.getDoubles();
            elseif (JType == obj.DOUBLE )
                MValue = JValue.getDouble();
                
            elseif (JType == obj.FLOAT_ARRAY || JType == obj.FLOAT_ARRAY_2D)
                MValue = JValue.getFloats();
            elseif (JType == obj.FLOAT )
                MValue = JValue.getFloat();
                
            elseif (JType == obj.INT_ARRAY || JType == obj.INT_ARRAY_2D)
                MValue = JValue.getInts();
            elseif (JType == obj.INT )
                MValue = JValue.getInt();
                
            elseif (JType == obj.LONG_ARRAY || JType == obj.LONG_ARRAY_2D)
                MValue = JValue.getLongs();
            elseif (JType == obj.LONG )
                MValue = JValue.getLong();
                
            elseif (JType == obj.SHORT_ARRAY || JType == obj.SHORT_ARRAY_2D)
                MValue = JValue.getShorts();
            elseif (JType == obj.SHORT )
                MValue = JValue.getShort();
                
            elseif (JType == obj.BYTE_ARRAY || JType == obj.BYTE_ARRAY_2D)
                MValue = JValue.getBytes();
            elseif (JType == obj.BYTE )
                MValue = JValue.getByte();
                
            elseif (JType == obj.ENUM_ARRAY || JType == obj.ENUM_ARRAY_2D)
                % introduced on 27 April 2018:
                auxEnums = JValue.getEnumItems();
                if obj.getEnumAsInt
                    MValue = nan(size(auxEnums));
                    for i=1:size(auxEnums, 1)
                        for j=1:size(auxEnums, 2)
                            MValue(i, j) = auxEnums(i, j).getCode();
                        end
                    end
                else
                    MValue = cell(size(auxEnums));
                    for i=1:size(auxEnums, 1)
                        for j=1:size(auxEnums, 2)
                            MValue{i, j} = char(auxEnums(i, j).getString());
                        end
                    end
                end
            elseif (JType == obj.ENUM)
                if obj.getEnumAsInt
                    MValue = JValue.getEnumItem().getCode();
                else
                    MValue = char(JValue.getEnumItem().getString());
                end
                
            elseif (JType == obj.ENUM_SET_ARRAY || JType == obj.ENUM_SET_ARRAY_2D)
                exception = MException('matlabJapcExtractor:read','Please contact davide.gamba@cern.ch and give him an example of what you are trying to acquire! ENUM_SETs arrays are not supported at the moment.');
                throw(exception);
            elseif (JType == obj.ENUM_SET)
                if obj.getEnumAsInt
                    MValue = JValue.getInt();
                else
                    MValue = cell(0);
                    auxEnumSize = 0;
                    auxEnumIterator = JValue.getEnumItemSet().iterator();
                    while (auxEnumIterator.hasNext())
                        auxEnumSize = auxEnumSize+1;
                        auxEnum = auxEnumIterator.next();
                        MValue{auxEnumSize} = char(auxEnum.getString());
                    end
                end
                
            elseif (JType == obj.BOOLEAN_ARRAY || JType == obj.BOOLEAN_ARRAY_2D)
                MValue = JValue.getBooleans();
            elseif (JType == obj.BOOLEAN)
                MValue = JValue.getBoolean();
                
            elseif (JType == obj.STRING_ARRAY || JType == obj.STRING_ARRAY_2D)
                auxStrings = JValue.getStrings();
                MValue = cell(size(auxStrings));
                for i=1:size(MValue,1)
                    for j=1:size(MValue,2)
                        MValue{i,j} = char(auxStrings(i,j));
                    end
                end
            elseif (JType == obj.STRING)
                MValue = char(JValue.getString());
                
            elseif (JType == obj.DISCRETE_FUNCTION)
                MValue = struct;
                auxFunction = JValue.getDiscreteFunction();
                MValue.X = auxFunction.getXArray();
                MValue.Y = auxFunction.getYArray();
            elseif (JType == obj.DISCRETE_FUNCTION_LIST)
                MValue = struct;
                auxFunctionList = JValue.getDiscreteFunctionList().getFunctions();
                for iFunction = 1:auxFunctionList.size()
                    auxFunction = auxFunctionList.get(iFunction-1);
                    MValue(iFunction).X = auxFunction.getXArray();
                    MValue(iFunction).Y = auxFunction.getYArray();
                end
            else
                %exception = MException('matlabJapcExtractor:read',['Not supported value type! Unable to get current value for: ',JValue.]);
                exception = MException('matlabJapcExtractor:read',['Not supported value type (',char(JType.toString()),...
                    ')! Unable to get current value for some field...']);
                throw(exception);
            end
            
            % if we are dealing with an array2D, then we can extract
            % correctly the associated matrix with the right dimensions.
            % Rremember Java is somehow row-major, Matlab is column-major.
            if (JValue.getValueType().isArray2d())
                auxNRows = JValue.getRowCount();
                auxNColumns = JValue.getColumnCount();
                MValue = reshape(MValue, auxNColumns, auxNRows)';
            end
            
        end % end JExtractSimpleMatlabValue static function
        
        function JNewValue = JInsertMatlabValue(obj, MValue, JValueDescriptor)
            %JNewValue = JInsertMatlabValue(MValue, JValueDescriptor)
            % sets a matlab understandable value into a Java Parameter Value
            % if the JNewValue is a Map Parameter Value, then MValue has to be a
            % structure with any of the fields present in the current JNewValue
            % Map.
            
            % import a SimpleParameterValueFactory
            import cern.japc.core.factory.SimpleParameterValueFactory
            import cern.japc.core.factory.MapParameterValueFactory
            
            
            if isempty(JValueDescriptor)
                % try to just guess the type as SIMPLE and let
                % newPaameterValue create something...
                %
                % This code is mainly for Georges and Francesco. It should
                % be done in a bit better way.
                %
                try
                    if isstruct(MValue)
                        auxDiscreteFunctionFactory = cern.japc.value.spi.value.InternalDiscreteFunctionFactory();
                        auxFunctionValue = auxDiscreteFunctionFactory.newDiscreteFunction(MValue.X, MValue.Y);
                        JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(auxFunctionValue);
                    else
                        JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(MValue);
                    end
                catch e
                    error(['Error creating new parameterValue. I don''t have any descriptor! I tried to do somethign but I failed: ',char(10), e.getReport]);
                end
            else
                % see if JNewValue is a map or a simple object
                JValueTypeHash = JValueDescriptor.getType().hashCode();
                if (JValueTypeHash == obj.SIMPLE)
                    JNewValue = obj.JInsertSimpleMatlabValue(MValue, JValueDescriptor);
                elseif (JValueTypeHash == obj.MAP)
                    % since this is a big structure, I need a struct as MValue
                    if ~isstruct(MValue)
                        error('matlabJapcExtractor:: you are trying to set a MAP parameter with a simple value. Impossible!')
                    end
                    % here the names of the fields
                    auxFieldNames = fieldnames(MValue);
                    
                    % create a new Map Value based on descriptor
                    try
                        JNewValue = MapParameterValueFactory.newValue(JValueDescriptor);
                    catch e
                        error(['Error creating new parameterValue: ',char(10), e.getReport]);
                    end
                    
                    % now set all of them
                    for i=1:numel(auxFieldNames)
                        % get single field descriptor
                        auxJSingleValueDescriptor = JValueDescriptor.get(auxFieldNames(i));
                        if isempty(auxJSingleValueDescriptor)
                            error(['matlabJapcExtractor:: no "',auxFieldNames{i},'" field in ', char(JValueDescriptor.getName())]);
                        end
                        
                        % insert the matlab value in it
                        auxJSingleValue = obj.JInsertSimpleMatlabValue(MValue.(char(auxFieldNames(i))), auxJSingleValueDescriptor);
                        % insert this simpleParameterValue in the mapParameterValue
                        JNewValue.put(auxFieldNames(i), auxJSingleValue);
                    end
                else
                    error('matlabJapcExtractor::unknown Java Value type. Probably an OBJECT -> not supported');
                end
            end
        end
        
        function JNewValue = JInsertSimpleMatlabValue(obj, MValue, JValueDescriptor)
            %JNewValue = JInsertSimpleMatlabValue(MValue, JValueDescriptor)
            % sets a matlab understandable value into a Java Simple Parameter Value
            
            % import a SimpleParameterValueFactory
            import cern.japc.core.factory.SimpleParameterValueFactory
            
            % In case the JValueType is not provided, I need to guess
            % the type by myself, i.e. I let the newParameterValue function decide...
            % I just take in consideration the case of an array2d.
            % In this case I just try to re-shuffle it properly.
            if nargin == 1 || isempty(JValueDescriptor)
                % I need to guess the type by myself
                auxArray2dSize = size(MValue);
                if min(auxArray2dSize) > 1
                    MValue = MValue';
                    MValue = MValue(:);
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(MValue, auxArray2dSize);
                else
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(MValue);
                end
                return
            else
                % get valueType from descriptor
                JValueType = JValueDescriptor.getValueType();
            end
            
            % The normal behavior (with JValueType give) is here.
            
            % in case of an array2d I have to reshuffle a bit the sizes
            % taking into account that Java is somehow row-major, Matlab is
            % column-major.
            auxArray2dSize = NaN;
            if (JValueType.isArray2d)
                auxArray2dSize = size(MValue);
                MValue = MValue';
                MValue = MValue(:);
            end
            
            % convert JValueType to hashCode
            JValueTypeHash = JValueType.hashCode();
            
            % properly create the new parameterValue
            try
                if (JValueTypeHash == obj.DOUBLE_ARRAY )
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(double(MValue));
                elseif (JValueTypeHash == obj.DOUBLE_ARRAY_2D )
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(double(MValue), auxArray2dSize);
                elseif (JValueTypeHash == obj.DOUBLE )
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(double(MValue));
                    
                elseif (JValueTypeHash == obj.FLOAT_ARRAY )
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(single(MValue));
                elseif (JValueTypeHash == obj.FLOAT_ARRAY_2D )
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(single(MValue), auxArray2dSize);
                elseif (JValueTypeHash == obj.FLOAT )
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(single(MValue));
                    
                elseif (JValueTypeHash == obj.INT_ARRAY )
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(int32(MValue));
                elseif (JValueTypeHash == obj.INT_ARRAY_2D )
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(int32(MValue), auxArray2dSize);
                elseif (JValueTypeHash == obj.INT )
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(int32(MValue));
                    
                elseif (JValueTypeHash == obj.LONG_ARRAY )
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(int64(MValue));
                elseif (JValueTypeHash == obj.LONG_ARRAY_2D )
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(int64(MValue), auxArray2dSize);
                elseif (JValueTypeHash == obj.LONG )
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(int64(MValue));
                    
                elseif (JValueTypeHash == obj.SHORT_ARRAY )
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(int16(MValue));
                elseif (JValueTypeHash == obj.SHORT_ARRAY_2D )
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(int16(MValue), auxArray2dSize);
                elseif (JValueTypeHash == obj.SHORT )
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(int16(MValue));
                    
                elseif (JValueTypeHash == obj.BYTE_ARRAY )
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(int8(MValue));
                elseif (JValueTypeHash == obj.BYTE_ARRAY_2D )
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(int8(MValue), auxArray2dSize);
                elseif (JValueTypeHash == obj.BYTE )
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(int8(MValue));
                    
                elseif (JValueTypeHash == obj.ENUM_ARRAY || JValueTypeHash == obj.ENUM_ARRAY_2D)
                    exception = MException('matlabJapcExtractor:set','Please contact davide.gamba@cern.ch and give him an example of what you are trying to set! ENUMs arrays are not supported at the moment.');
                    throw(exception);
                elseif (JValueTypeHash == obj.ENUM)
                    auxEnumType = JValueDescriptor.getEnumType();
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(auxEnumType.valueOf(MValue));
                    
                elseif (JValueTypeHash == obj.ENUM_SET_ARRAY || JValueTypeHash == obj.ENUM_SET_ARRAY_2D)
                    exception = MException('matlabJapcExtractor:set','Please contact davide.gamba@cern.ch and give him an example of what you are trying to set! ENUM_SETs arrays are not supported at the moment.');
                    throw(exception);
                elseif (JValueTypeHash == obj.ENUM_SET)
                    % This code is WORKING, but not sure how reliable/correct it is...
                    auxEnumType = JValueDescriptor.getEnumType();
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(JValueType, auxEnumType);
                    
                    % Jan 2016
                    % If I get an integer to set, then I expect to be a bit
                    % pattern. If I get a cellarray, then is the set of
                    % enum names I need to set
                    auxEnumValueSetCode = 0;
                    if iscell(MValue)
                        for i=1:length(MValue)
                            auxEnumValueSetCode = auxEnumValueSetCode+auxEnumType.valueOf(MValue{i}).getCode;
                        end
                    elseif isinteger(MValue)
                        auxEnumValueSetCode = MValue;
                    else
                        exception = MException('matlabJapcExtractor:set',['You are trying to set an "ENUM_SET" object. ',char(10),...
                            'At the moment either you give me a cell array of strings you want to SET, ',char(10),...
                            'or an INTEGER representing the bit pattern you want.',char(10),char(10),...
                            'If you think this is not enough for you, please let me know. (davide.gamba@cern.ch)']);
                        throw(exception);
                    end
                    JNewValue.setEnumItemSet(cern.japc.value.spi.value.EnumItemSetImpl(auxEnumType, auxEnumValueSetCode));
                    
                elseif (JValueTypeHash == obj.BOOLEAN_ARRAY)
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(logical(MValue));
                elseif (JValueTypeHash == obj.BOOLEAN_ARRAY_2D)
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(logical(MValue), auxArray2dSize);
                elseif (JValueTypeHash == obj.BOOLEAN)
                    JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(logical(MValue));
                    
                elseif (JValueTypeHash == obj.STRING_ARRAY)
                    if isnumeric(MValue)
                        auxStrings = cell(numel(MValue),1);
                        for i=1:numel(MValue)
                            auxStrings{i} = num2str(MValue(i));
                        end
                        JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(auxStrings);
                    else
                        JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(MValue);
                    end
                elseif (JValueTypeHash == obj.STRING_ARRAY_2D)
                    if isnumeric(MValue)
                        auxStrings = cell(numel(MValue),1);
                        for i=1:numel(MValue)
                            auxStrings{i} = num2str(MValue(i));
                        end
                        JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(auxStrings, auxArray2dSize);
                    else
                        JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(MValue, auxArray2dSize);
                    end
                elseif (JValueTypeHash == obj.STRING)
                    if isnumeric(MValue)
                        JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(num2str(MValue));
                    else
                        JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(MValue);
                    end
                    
                elseif (JValueTypeHash == obj.DISCRETE_FUNCTION)
                    if isnumeric(MValue)
                        % Per Georgess -> if numeric, just try with an array of doubles...
                        warn('matlabJAPC: Something strange here... will try to continue....')
                        JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(double(MValue));
                    else
                        % I expect MValue is a struct with X and Y fields
                        try
                            auxXarray = MValue.X;
                            auxYarray = MValue.Y;
                            if length(auxXarray) ~= length(auxYarray)
                                error('X and Y sizes are not compatible.')
                            end
                        catch e
                            error(['matlabJapcExtractor::JInsertSimpleMatlabValue: In order to ',...
                                'set a discrete function value, I need a struct with "X" ',...
                                'and "Y" fields of the same size! ', char(10), e.getReport]);
                        end
                        
                        auxDiscreteFunctionFactory = cern.japc.value.spi.value.InternalDiscreteFunctionFactory();
                        auxFunctionValue = auxDiscreteFunctionFactory.newDiscreteFunction(auxXarray, auxYarray);
                        JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(auxFunctionValue);
                    end
                    
                elseif (JValueTypeHash == obj.DISCRETE_FUNCTION_LIST)
                    if ~isstruct(MValue)
                        error(['matlabJapcExtractor::JInsertSimpleMatlabValue: I need ',...
                            'for sure an array of struct to set a DISCRETE_FUNCTION_LIST.']);
                    end
                    
                    auxDiscreteFunctionFactory = cern.japc.value.spi.value.InternalDiscreteFunctionFactory();
                    JNewList = java.util.ArrayList;
                    auxNFunctions = length(MValue);
                    for i=1:auxNFunctions
                        auxXarray = MValue(i).X;
                        auxYarray = MValue(i).Y;
                        auxFunctionValue = auxDiscreteFunctionFactory.newDiscreteFunction(auxXarray, auxYarray);
                        JNewList.add(auxFunctionValue);
                    end
                    if auxNFunctions == 1
                        JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(auxFunctionValue);
                    elseif auxNFunctions > 1
                        JNewValue = SimpleParameterValueFactory.newSimpleParameterValue(auxDiscreteFunctionFactory.newDiscreteFunctionList(JNewList));
                    else
                        error('Something wrong with creating DiscreteFunctionList...');
                    end
                else
                    exception = MException('matlabJapcExtractor:set',['Not supported value type (',char(JValueType.toString()),')! Unable to set current value for:',' write to davide.gamba@cern.ch for support']);
                    throw(exception);
                end
            catch e
                error(['Error creating new parameterValue: ',char(10), e.getReport]);
            end
        end
        
        function JDataFilter = JGenerateDataFilter(obj, MValue)
            %JDataFilter = JGenerateDataFilter(obj, MValue)
            % given a matlab structure or a single value, it generates a
            % dataFilter used for some (uncommon) selectors..
            
            % import a SimpleParameterValueFactory
            import cern.japc.core.factory.SimpleParameterValueFactory
            
            if isempty(MValue)
                JDataFilter = [];
            elseif isstruct(MValue)
                % create a Map Value
                JDataFilter = SimpleParameterValueFactory.newSimpleParameterValue();
                
                % get list of fields in structure
                auxFieldNames = fieldnames(MValue);
                % and populate parameterValue
                for i=1:numel(auxFieldNames)
                    % insert the matlab value in it
                    auxJSingleValue = obj.JInsertSimpleMatlabValue(MValue.(char(auxFieldNames(i))), []);
                    JDataFilter.put(auxFieldNames(i), auxJSingleValue);
                end
                
            else %assuming that filter is just a simple value.
                JDataFilter = obj.JInsertSimpleMatlabValue(MValue, []);
            end
        end
        
        
        function setGetEnumAsInt(obj, setValue)
            %setGetEnumAsInt(obj, setValue)
            % If you set it to true, then it will return the ENUMs as their
            % own code instead of the string related to them.
            obj.getEnumAsInt = logical(setValue);
        end
        function value = isGetEnumAsInt(obj)
            %value = isGetEnumAsInt(obj)
            % It returns true if the object is set such to return the ENUMs
            % as theirs code instead of the string related to them.
            value = obj.getEnumAsInt;
        end
        
    end
    methods(Static)
        function MValue = staticJExtractMatlabValue(JValue)
            % static method to extract a complex JValue
            
            aux = matlabJapcExtractor();
            MValue = aux.JExtractMatlabValue(JValue);
        end
        function MValue = staticJExtractSimpleMatlabValue(JValue)
            % static method to extract a simple JValue
            
            aux = matlabJapcExtractor();
            MValue = aux.JExtractSimpleMatlabValue(JValue);
        end
        function JNewValue = staticJInsertMatlabValue(MValue, JValueDescriptor)
            % static method to create a complex JValue
            aux = matlabJapcExtractor();
            JNewValue = aux.JInsertMatlabValue(MValue, JValueDescriptor);
        end
        function JNewValue = staticJInsertSimpleMatlabValue(MValue, JValueDescriptor)
            % static method to create a simple JValue
            aux = matlabJapcExtractor();
            JNewValue = aux.JInsertSimpleMatlabValue(MValue, JValueDescriptor);
        end
        function JDataFilter = staticJGenerateDataFilter(MValue)
            % static method to create a data filter
            aux = matlabJapcExtractor();
            JDataFilter = aux.JGenerateDataFilter(MValue);
        end
    end
end

