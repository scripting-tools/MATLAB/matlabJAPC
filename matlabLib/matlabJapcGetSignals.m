function output = matlabJapcGetSignals( cycleName, getSignals, nAcquisitions, timeOut_s)
%output = matlabJapcGetSignals( cycleName, getSignals, [nAcquisitions = 1, [timeOut_s = Inf]])
%
% This function is a simple wrapper of matlabJapc and matlabJapcMonitor to
% make single acquisitions of nShots of the given signals.
% If one acquisition is required, it will just use matlabJapc.
% If many acquisitions are required, a monitor will be created and it will
% be stopped/deleted when the acquisition is finished.
%
% This function will block the execution of your script!
% You can also specify a timeout, after which the function exit anyway with
% eventually less acquisitions..
%
%
% davideg - Jun 2015


% standard behavior of matlabJapc
if (nargin == 2) || (nAcquisitions == 1)
    output = {matlabJapc.staticGetSignals(cycleName, getSignals)};
    return
end
if nargin == 3
    timeOut_s = Inf;
end

% if nAcquisitions is given, then it will create a monitor, collectect n
% acquisitions, and return them 
myCellArray= cell(0);

% define callback function
function myFillCellArray(newData)
    myCellArray{end+1} = newData;
end

% 
monitorObject = matlabJapcMonitor(cycleName, getSignals, @(data)myFillCellArray(data));
monitorObject.useFastStrategy(1);
monitorObject.start();
try
    % wait for data
    tic
    while length(myCellArray) < nAcquisitions
        pause(0.2) % pause some time
        if toc > timeOut_s
            disp('Timeout!... giving you what I have.')
            break
        end
    end
catch e
    disp('Some error happend...')
    disp(e.getReport);
end
% stop monitor
monitorObject.stop();
monitorObject.delete();

% return values
if length(myCellArray) > nAcquisitions
    output = myCellArray(1:nAcquisitions);
else
    output = myCellArray;
end

end


