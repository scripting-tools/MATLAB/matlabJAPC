classdef matlabJapcMonitor < matlabJapc
    %MATLABJAPCMONITOR Subclass of matlabJapc to allow monitoring of
    %devices.
    % Acquisition monitor class for linearFeedback and general purpose.
    %
    % Davide Gamba - Guido Sterbini
    % Jun 2014
    % LS2 baseline update in Dec 2018
    
    % %%%%%%%%%%%%%%%%%%%%
    properties
        % matlab structure with last data
        lastDataStruct=struct;
        
        % number of recorded cycles
        recordedCycles=0;
        % if true, it saves all the acquired data in separate .mat files
        saveData=false;
        % where to save data
        saveDataPath='./';
        
        % buffer size (number of shots I will always keep)
        bufferSize = 1;
        
        % The function handle that is called after an acquisition.
        % It must accept an argument, that will contain the lastDataStruct
        % with the data of last acquisition.
        % You can also specify a two arguments function. In this case the
        % second one is a reference to the calling matlabJapcMonitor
        % object.
        externalFunction
        
        % If true, the external function is called in a ~ synchronized way.
        % If the external function is still working when new data is arriving, it
        % doesn't call the function with the new data, but it can still
        % save it to file if requested.
        synchronizedExternalCalls = true;
        
        % If true, the data delivery is suspended, but the subscription is
        % still ongoing. Use it with care!
        isPaused = false;
        
        % if true, it will try to get values that didn't manage to arrive
        % in time. Of course this might be very bad in terms of
        % synchronization between the data. Use at your own risk
        isTryingToGetValues = false;
    end
    
    % %%%%%%%%%%%%%%%%%%%%
    properties (Access=private)
        % signal list
        signalList
        signalListPaths % onlyUsed if not using the rawDataStruct system.
        
        % The handles to subscritptions used to acquire real data
        subscriptionHandles = {};
        subscriptionListenerHandles = {};
        nSubscriptions
        monitorsIdxs
        signalIndexesPerMonitor={};
        
        % working copy of the dataStruct
        currentDataStruct;
        % and related data
        currentLastValues;
        currentLastCycleNames;
        currentLastCycleStamps;
        currentLastTimeStamps;
        currentLastErrors;
        
        % variable to check if external function is working or not, for
        % synchronizedExternalCalls only!
        isExternalFunctionWorking = false;
        
        % if true, the dataStruct used is just an ensamble of arrays
        % instead of the more "complex" structure with all the parameters
        % names split...
        isUsingRawDataStruct = false;
        % an empty data structure that is used every time as a starting
        % copy for the data.
        emtpyDataStruct = struct;
        
        % comment added to all data structure acquired
        comment
        
        % fast strategy related variables
        subscriptionStrategyHandles = {};
        fastStrategyEnabled = false;
        fastStrategyGetOnChangeAndConstantValuesEnabled = false;
        fastStrategyTimeout = -1;
        fastStrategyAllowMultipleUpdatesPerCycle = false;
    end
    
    % %%%%%%%%%%%%%%%%%%%%
    methods
        % constructor
        function obj=matlabJapcMonitor(cycleName, signalList, externalFunction, comment, ...
                monitorsIdxs)
            % create object
            obj=obj@matlabJapc(cycleName);
            
            obj.cycleName=cycleName;
            obj.signalList=signalList;
            
            if nargin < 3
                externalFunction = '';
                comment = '';
            elseif nargin < 4
                comment = '';
            end
            obj.externalFunction = externalFunction;
            obj.comment=comment;
            
            if (nargin > 4)
                % I also have a monitors indexes
                if length(monitorsIdxs) ~= length(signalList)
                    error('monitorsIdxs must have the same size of signalList');
                end
            else
                monitorsIdxs = ones(size(signalList));
            end
            obj.monitorsIdxs = monitorsIdxs;
            
            % create subscription
            obj.initialiseSubscription();
            % initialise also empty data struct
            obj.initialiseEmptyDataStruct();
        end
        
        function setSelector(obj, cycleName, dataFilterStruct)
            %setSelector(obj, cycleName, onChange, dataFilterStruct)
            % cycleName (default = []) is a string in the form like 'SCT.USER.SETUP', i.e.
            %   'accelerator.USER.period'
            % dataFilterStruct (default = []) if specified, it is supposed to be
            %   either a value or a matlab structure from which construct the
            %   dataFilter necessary for the acquisition of the data
            %   required.
            
            % default values
            if nargin < 3
                dataFilterStruct = [];
            end
            if nargin < 2
                cycleName = [];
            end
            
            % call original method first.
            setSelector@matlabJapc(obj, cycleName, dataFilterStruct);
            
            % re-Initialize Subscription
            obj.initialiseSubscription();
            % re-Initialize Empty data struct
            obj.initialiseEmptyDataStruct();
        end
        
        
        function start(obj, bufferSize)
            %start(obj, bufferSize)
            % start data acquisition. if bufferSize is specified, the data
            % will be given as an array of structures with the last
            % <bufferSize> shots.
            
            % reset "Last" variables
            obj.resetLastVariables();
            
            if nargin == 1
                bufferSize = 1;
            elseif (~(isnumeric(bufferSize) && ~hasInfNaN(bufferSize)) || bufferSize < 1)
                disp('Invalid bufferSize specified. Using 1.')
                bufferSize = 1;
            end
            obj.bufferSize = bufferSize;
            
            for i=1:obj.nSubscriptions
                obj.subscriptionHandles{i}.startMonitoring();
            end
            
            % be sure that object is not paused.
            obj.isPaused = false;
            
            % reset status of external function working. (needed only for
            % synchronizedExternalFunciton enabled)
            obj.isExternalFunctionWorking = false;
            
            if obj.verbose
                pause(0.5);
                for i=1:obj.nSubscriptions
                    if obj.subscriptionHandles{i}.isMonitoring()
                        obj.printOut(['Monitor object ',num2str(i),' Started'])
                    else
                        obj.printOut(['Monitor object ',num2str(i),' DID NOT Start'])
                    end
                end
            end
        end
        
        
        
        function stop(obj)
            %stop(obj)
            % stop data acquisition
            % If you need to stop/start subscription many times in your
            % application, consider using the
            %   pauseOn(obj)/pauseOff(obj)
            % functions that are faster and safer.
            
            for i=1:obj.nSubscriptions
                obj.subscriptionHandles{i}.stopMonitoring();
            end
            
            if obj.verbose
                pause(2);
                for i=1:obj.nSubscriptions
                    if ~obj.subscriptionHandles{i}.isMonitoring()
                        obj.printOut(['Monitor ',num2str(i),' object stopped...'])
                    else
                        % Davide - Feb 2025: unclear to me why we added this reset in Sep.2024
                        %resetLastVariables(obj)
                        obj.printOut(['Monitor ',num2str(i),' object DID NOT Stop!'])
                    end
                end
            end
        end
        
        function pauseOn(obj)
            %pauseOn(obj)
            % set the monitor in puase mode. In this mode the subscription
            % is still running, but nor the external function, nor the
            % saving are enabled.
            obj.isPaused = true;
        end
        function pauseOff(obj)
            %pauseOff(obj)
            % It restarts calling the external function and saving data (if
            % the monitoring is started...
            obj.isPaused = false;
        end
        
        function out = isAcquiringData(obj)
            %out = isAcquiringData(obj)
            % It returns true if the subscription is running and it is not
            % paused.
            
            out=false;
            if obj.isPaused
                return %useless to continue
            end
            for i=1:obj.nSubscriptions
                out = out || obj.subscriptionHandles{i}.isMonitoring();
            end
        end
        
        
        %
        % here start some functions related to fast strategy for Califes
        % Signals
        %
        function useFastStrategy(obj, value)
            %useFastStrategy(obj, value)
            % With this method you can enable/disable the custom made CTF
            % fast strategy for the subscription. This allows you to
            % subscribe for data that receive more than one update per
            % Basic Period. This might be usefull for Califes beam, where
            % somethimes we have higher repetition rate (>0.86 Hz).
            % It is normally safe to use this FastStrategy in any
            % condition/machine.
            %
            if (value ~= obj.fastStrategyEnabled)
                obj.fastStrategyEnabled = value;
                
                % I need to recreate subscription...
                obj.deleteSubscription();
                obj.initialiseSubscription();
                obj.initialiseEmptyDataStruct();
            end
        end
        function isit = isUsingFastStrategy(obj)
            %isit = isUsingFastStrategy( obj )
            % returns if it is using the CTF custom fast strategy to
            % acquire data.
            isit = obj.fastStrategyEnabled;
        end
        function setFastStrategyTimeOut(obj, value)
            %setFastStrategyTimeOut(obj, value)
            % if it is using the custom fast strategy for subscription
            % (enable it with useFastStrategy(obj, true)), then it sets a
            % timeout after which data is given back to you even if not all
            % the parameters have been collected.
            % To disable this feature, just set it to a value < 0
            % (default = -1)
            %
            % value has to be in [ms].
            
            if obj.isUsingFastStrategy()
                obj.stop();
                try
                    for i=1:obj.nSubscriptions
                        obj.subscriptionStrategyHandles{i}.setFastStrategyTimeOut(int64(value));
                    end
                    obj.fastStrategyTimeout = int64(value);
                catch e
                    error(['matlabJapcMonitor:: Impossible to set Fast Strategy timeout: ', char(10), e.getReport])
                end
            else
                error('matlabJapcMonitor:: Fast Strategy is not in use in this monitor.')
            end
        end
        function value = getFastStrategyTimeOut(obj)
            %value = getFastStrategyTimeOut(obj)
            % returns the current timeOut used for the FastStrategy (if in
            % use).
            if obj.isUsingFastStrategy()
                value = obj.fastStrategyTimeout;
                return
            else
                error('matlabJapcMonitor:: Fast Strategy is not in use in this monitor.')
            end
        end
        function forceFastStrategyGetOnChangeAndConstantValues(obj, value)
            %forceFastStrategyGetOnChangeAndConstantValues(obj, value)
            % If the Fast Strategy is in use for this monitor, then it
            % enables/disables the feature to Get onChange and Constant
            % JAPC Parameters in case they did not arrive to the monitor.
            %
            % (default = false)
            if obj.isUsingFastStrategy()
                obj.stop();
                try
                    for i=1:obj.nSubscriptions
                        obj.subscriptionStrategyHandles{i}.setFastStrategyForceGetOnChangeAndConstantValues(logical(value));
                    end
                    obj.fastStrategyGetOnChangeAndConstantValuesEnabled = logical(value);
                catch e
                    error(['matlabJapcMonitor:: Impossible to set Fast Strategy "forceGetOnChangeAndConstant": ', char(10), e.getReport])
                end
            else
                error('matlabJapcMonitor:: Fast Strategy is not in use in this monitor.')
            end
        end
        function value = isFastStrategyForceGetOnChangeAndConstantValues(obj)
            %value = isFastStrategyForceGetOnChangeAndConstantValues(obj)
            % It returns if, using the Fast Strategy, it tryes to GET
            % OnChange and Constant parameters.
            if isUsingFastStrategy()
                value = obj.fastStrategyGetOnChangeAndConstantValuesEnabled;
            else
                error('matlabJapcMonitor:: Fast Strategy is not in use in this monitor.')
            end
        end
        function allowManyUpdatesPerCycle(obj, value)
            %allowManyUpdatesPerCycle(obj, value)
            % If the Fast Strategy is in use for this monitor, then it
            % enables/disables the ability to give updates faster than
            %
            % (default = false)
            if obj.isUsingFastStrategy()
                obj.stop();
                try
                    for i=1:obj.nSubscriptions
                        obj.subscriptionStrategyHandles{i}.setFastStrategyAllowManyUpdatesPerCycle(logical(value));
                    end
                    obj.fastStrategyAllowMultipleUpdatesPerCycle = logical(value);
                catch e
                    error(['matlabJapcMonitor:: Impossible to allow Fast Strategy for multiple updates per cycle: ', char(10), e.getReport])
                end
            else
                error('matlabJapcMonitor:: Fast Strategy is not in use in this monitor.')
            end
        end
        function value = isAllowManyUpdatesPerCycle(obj)
            %value = isAllowManyUpdatesPerCycle(obj)
            % It returns if, using the Fast Strategy, it is allowed to get
            % two updates for the same cyclestamp.
            if obj.isUsingFastStrategy()
                value = obj.fastStrategyAllowMultipleUpdatesPerCycle;
            else
                error('matlabJapcMonitor:: Fast Strategy is not in use in this monitor.')
            end
        end
        
        function useRawDataStruct(obj, setValue)
            %useRawDataStruct(obj, (setValue = true))
            % if set to true, then the dataStruct used is just an ensamble of arrays
            % instead of the more "complex" structure with all the parameters
            % names split...
            if nargin == 1
                setValue = true;
            end
            obj.isUsingRawDataStruct = setValue;
            obj.initialiseEmptyDataStruct();
        end
        function output = getUseRawDataStruct(obj)
            %output = getUseRawDataStruct(obj)
            % it returns true if is in use the rawDataStruct system.
            output = obj.isUsingRawDataStruct;
        end
        
        function delete(obj)
            % properly delete object
            
            % delete subscriptions
            obj.deleteSubscription();
            
            % just a message...
            obj.printOut('Handle deleted.')
        end
    end % end Public methods
    
    % %%%%%%%%%%%%%%%%
    methods (Static)
        
    end % end Static methods
    
    % %%%%%%%%%%%%%%%%
    methods (Access=private)
        function initialiseSubscription(obj)
            % function that creates the subscriptions
            
            uniqueIndexes = unique(obj.monitorsIdxs);
            uniqueIndexes = sort(uniqueIndexes);
            obj.nSubscriptions = length(uniqueIndexes);
            % debug Piotr
            %obj.subscriptionStrategyHandles = cell(length(uniqueIndexes),1);
            
            % initialize signalListPaths
            obj.signalListPaths = cell(length(obj.signalList),1);
            
            % initialize Java Object(s)
            for i=1:obj.nSubscriptions
                obj.signalIndexesPerMonitor{i} = find(obj.monitorsIdxs == uniqueIndexes(i));
                
                if obj.fastStrategyEnabled
                    % create groupStrategy object (custom CTF made!)
                    obj.subscriptionStrategyHandles{i} = cern.ctf.customjarsmatlab.GroupSubscriptionStrategyFactoryCTF(true);
                    %
                    obj.subscriptionStrategyHandles{i}.setFastStrategyAllowManyUpdatesPerCycle(logical(obj.fastStrategyAllowMultipleUpdatesPerCycle));
                    obj.subscriptionStrategyHandles{i}.setFastStrategyForceGetOnChangeAndConstantValues(logical(obj.fastStrategyGetOnChangeAndConstantValuesEnabled));
                    obj.subscriptionStrategyHandles{i}.setFastStrategyTimeOut(int64(obj.fastStrategyTimeout));
                    % create standard group Jobject with injected strategy
                    auxGrp = cern.japc.core.spi.group.ParameterGroupImpl(obj.subscriptionStrategyHandles{i});
                else
                    % create standard group Jobject
                    auxGrp = cern.japc.core.spi.group.ParameterGroupImpl();
                end
                % add all the parameters
                auxIndexesList = obj.signalIndexesPerMonitor{i};
                for iSignal = 1:length(auxIndexesList)
                    tmpParameter = obj.parameterFactory.newParameter(obj.signalList{auxIndexesList(iSignal)});
                    auxGrp.add(tmpParameter);
                    
                    % I use this loop also to decompose the parameters as
                    % it will be seen by the data collector afterwards...
                    auxParameterName = char(tmpParameter.getName());
                    myStr = matlabDataAndSignalsHelper.convertSignalToStructPath(char(auxParameterName));
                    obj.signalListPaths{auxIndexesList(iSignal)} = strsplit(myStr,'.');
                end
                
                % create subscription listener handles
                obj.subscriptionListenerHandles{i} = cern.ctf.customjarsmatlab.MatlabJapcMonitorListener();
                
                % create subscription
                obj.subscriptionHandles{i} = auxGrp.createSubscription(obj.parameterSelector, obj.subscriptionListenerHandles{i});
                
                % for each listener handles, run the same data collecting
                % function
                auxHandler = handle(obj.subscriptionListenerHandles{i},'CallbackProperties');
                set(auxHandler,'GotEventCallback',@(h,e)obj.myDataCollector(e, i));
                % DEBUG: this was the old way of setting callback
                %set(obj.subscriptionListenerHandles{i},'GotEventCallback',@(h,e)obj.myDataCollector(e));
                % DEBUG: this is an other way to set callback, but not sure
                %   it works:
                %handle.listener(handle(obj.subscriptionListenerHandles{i}),'GotEventCallback',@(h,e)obj.myDataCollector(e));
            end
            
            % it always uses the first monitor(the one with lower index) as
            % beam trigger. so for this one the callback function is a bit
            % different
            auxHandler = handle(obj.subscriptionListenerHandles{1},'CallbackProperties');
            set(auxHandler,'GotEventCallback',@(h,e)obj.myDataDelivery(e));
            % DEGUG: old
            %set(obj.subscriptionListenerHandles{1},'GotEventCallback',@(h,e)obj.myDataDelivery(e));
            
        end
        
        function initialiseEmptyDataStruct(obj)
            % function that just creates the emtpyDataStruct used as a
            % model for every new acquisition to store new data.
            obj.emtpyDataStruct = struct;
            
            % also clear currentDataStruct (not to mix different kind of
            % structures if buffering)
            obj.currentDataStruct = cell(0);
            
            % fill data structures
            if obj.isUsingRawDataStruct
                obj.emtpyDataStruct.values = cell(length(obj.signalList),1);
                obj.emtpyDataStruct.cycleNames = cell(length(obj.signalList),1);
                obj.emtpyDataStruct.errors = cell(length(obj.signalList),1);
            else
                for i=1:length(obj.signalList)
                    myStr = obj.signalListPaths{i};
                    switch length(myStr)
                        case 1
                            obj.emtpyDataStruct.(myStr{1}).value = NaN;
                            obj.emtpyDataStruct.(myStr{1}).timeStamp = NaN;
                            obj.emtpyDataStruct.(myStr{1}).cycleStamp = NaN;
                            obj.emtpyDataStruct.(myStr{1}).cycleName = '';
                            obj.emtpyDataStruct.(myStr{1}).error = 'No update received yet';
                        case 2
                            obj.emtpyDataStruct.(myStr{1}).(myStr{2}).value = NaN;
                            obj.emtpyDataStruct.(myStr{1}).(myStr{2}).timeStamp = NaN;
                            obj.emtpyDataStruct.(myStr{1}).(myStr{2}).cycleStamp = NaN;
                            obj.emtpyDataStruct.(myStr{1}).(myStr{2}).cycleName = '';
                            obj.emtpyDataStruct.(myStr{1}).(myStr{2}).error = 'No update received yet';
                        case 3
                            obj.emtpyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).value = NaN;
                            obj.emtpyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).timeStamp = NaN;
                            obj.emtpyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).cycleStamp = NaN;
                            obj.emtpyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).cycleName = '';
                            obj.emtpyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).error = 'No update received yet';
                        case 4
                            obj.emtpyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).value = NaN;
                            obj.emtpyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).timeStamp = NaN;
                            obj.emtpyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).cycleStamp = NaN;
                            obj.emtpyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).cycleName = '';
                            obj.emtpyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).error = 'No update received yet';
                        case 5
                            obj.emtpyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).(myStr{5}).value = NaN;
                            obj.emtpyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).(myStr{5}).timeStamp = NaN;
                            obj.emtpyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).(myStr{5}).cycleStamp = NaN;
                            obj.emtpyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).(myStr{5}).cycleName = '';
                            obj.emtpyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).(myStr{5}).error = 'No update received yet';
                        otherwise
                            error(['Parameter ' obj.signalList{i} ' has a too long/short structure. Not supported! Write to davide.gamba@cern.ch!'])
                    end
                end
            end
            
            % create other fields (commons to both types of struct)
            obj.emtpyDataStruct.parameters = obj.signalList(:);
            obj.emtpyDataStruct.cycleName = obj.cycleName;
            obj.emtpyDataStruct.comment = obj.comment;
            % timestamps
            obj.emtpyDataStruct.localTimeStamp_s = -1;
            obj.emtpyDataStruct.localTimeStamp_ms = -1;
            obj.emtpyDataStruct.headerTimeStamps = NaN(length(obj.signalList),1);
            obj.emtpyDataStruct.headerCycleStamps = NaN(length(obj.signalList),1);
            obj.emtpyDataStruct.seqNumber = -1;
        end
        
        function resetLastVariables(obj)
            % Initialize here the current "Last" properties
            obj.currentLastValues = cell(length(obj.signalList),1);
            obj.currentLastCycleNames = cell(length(obj.signalList),1);
            obj.currentLastCycleStamps = NaN(length(obj.signalList),1);
            obj.currentLastTimeStamps = NaN(length(obj.signalList),1);
            obj.currentLastErrors = cell(length(obj.signalList),1);
            
            % And clear also the currentDataStruct
            obj.currentDataStruct = cell(0);
        end
        
        function deleteSubscription(obj)
            % function to correclty delete subscription
            
            % fist stop subscription
            for i=1:obj.nSubscriptions
                if obj.subscriptionHandles{i}.isMonitoring();
                    obj.stop;
                    break;
                end
            end
            % remove all the call backs
            for i=1:obj.nSubscriptions
                auxHandler = handle(obj.subscriptionListenerHandles{i},'CallbackProperties');
                set(auxHandler,'GotEventCallback','');
                % DEGUG: old
                %set(obj.subscriptionListenerHandles{i},'GotEventCallback','');
            end
            
            % then delete objects
            obj.subscriptionHandles = {};
            obj.subscriptionStrategyHandles = {};
            obj.subscriptionListenerHandles = {};
            obj.signalIndexesPerMonitor = {};
        end
        
        function myDataCollector(obj, event, subscriptionIndex)
            % for debug
            obj.printOut([char(13), '%%% Executing dataCollection #', num2str(obj.recordedCycles)]);
            
            % extract the FailSafeParameterValue
            auxMyFailSafeParameterValues = event.myJValues;
            auxNFailSafeValues = length(auxMyFailSafeParameterValues);
            
            % let's see which parameter I am supposed to extract
            auxIndexesList = obj.signalIndexesPerMonitor{subscriptionIndex};
            
            % redoundant check
            if length(auxIndexesList) ~= auxNFailSafeValues
                error('We have a bug here! nParameters for this monitor is different than n-received!')
            end
            
            % extract all the informations
            for i=1:auxNFailSafeValues
                auxParameterValue = auxMyFailSafeParameterValues(i);
                auxParameterName = obj.signalList{auxIndexesList(i)};
                
                obj.printOut([char(13),' - Processing ', auxParameterName]);
                
                if isempty(auxParameterValue.getException())
                    try
                        % Test Dec 2018
                        if obj.isGetJavaPlaneObjects()
                            warning('Test Davide Dec 2018')
                            auxValue = auxParameterValue.getValue();
                        else
                            auxValue = obj.myExtractor.JExtractMatlabValue(auxParameterValue.getValue());
                        end
                        auxError = '';
                    catch e
                        auxValue = NaN(1);
                        auxError = e.getReport;
                    end
                else
                    % let's see if I shoud try to get this value
                    if obj.isTryingToGetValues
                        try
                            obj.printOut(['Trying to GET ',auxParameterName, '...']);
                            auxValue = obj.JGetFesaSignal(auxParameterName);
                            auxError = '';
                        catch e
                            auxValue = NaN(1);
                            auxError = e.getReport;
                            obj.printOut(['No value for: ',auxParameterName,' received! Even impossible to GET it: ',char(13),...
                                auxError]);
                        end
                    else
                        auxValue = NaN(1);
                        auxError = char(auxParameterValue.getException().getMessage());
                        obj.printOut(['No value for: ',auxParameterName,' received! JAPC error:',char(13),...
                            auxError]);
                    end
                end
                
                
                % common to get data from header (if possible)
                auxHeader = auxParameterValue.getHeader();
                try
                    auxHeaderTimeStamp = int64(auxHeader.getAcqStamp());
                catch e
                    obj.printOut(['Impossible to extract HeaderTimeStamp from ',auxParameterName,' ', char(10), e.getReport]);
                    auxHeaderTimeStamp = NaN;
                end
                try
                    auxHeaderCycleStamp = int64(auxHeader.getCycleStamp());
                catch e
                    obj.printOut(['Impossible to extract HeaderCycleStamp from ',auxParameterName,' ', char(10), e.getReport]);
                    auxHeaderCycleStamp = NaN;
                end
                try
                    auxHeaderSelector = char(auxHeader.getSelector().toString());
                catch e
                    obj.printOut(['Impossible to extract HeaderSelector from ',auxParameterName,' ', char(10), e.getReport]);
                    auxHeaderSelector = e.getReport;
                end
                
                % set everything to "Last" parameters
                obj.currentLastValues{auxIndexesList(i)} = auxValue;
                obj.currentLastCycleNames{auxIndexesList(i)} = auxHeaderSelector;
                obj.currentLastCycleStamps(auxIndexesList(i)) = auxHeaderCycleStamp;
                obj.currentLastTimeStamps(auxIndexesList(i)) = auxHeaderTimeStamp;
                obj.currentLastErrors{auxIndexesList(i)} = auxError;
            end
            
            % for debug
            obj.printOut(['%%% End executing dataCollection #', num2str(obj.recordedCycles), char(13)]);
        end
        
        function myDataDelivery(obj, event)
            % this function is called by the first groupSubscription
            % (remember we can have many of them in the same
            % matlabJapcMonitor).
            % This is responsible to also call the user function, assuming
            % that all the data has already been acquired, and this is the
            % last data arraving to complete the set required by the
            % other monitors.
            
            % check if we are not paused
            if obj.isPaused
                return
            end
            
            if obj.verbose
                tic
            end
            
            % for debug
            obj.printOut([char(13), '%%% Executing dataDelivery #', num2str(obj.recordedCycles)]);
            
            
            % collect also this data (for the other monitors data is
            % already collected in the current structure...)
            obj.myDataCollector(event, 1); % is always the first index this!
            
            % I assume that like this in the "Last" arrays we have a full
            % set of new data, that will be overwritten by next iterations
            % eventually.
            
            
            % generate the newData structure and add it to currentOne
            if (obj.bufferSize > 1)
                if isempty(obj.currentDataStruct)
                    obj.currentDataStruct = cell(0);
                end
                obj.currentDataStruct{end+1} = obj.generateDataStructFromCurrentData();
                % trim currentDataStruct to the requested nShot size
                if length(obj.currentDataStruct) > obj.bufferSize
                    obj.currentDataStruct(1:(length(obj.currentDataStruct) - obj.bufferSize)) = '';
                end
            else
                obj.currentDataStruct = obj.generateDataStructFromCurrentData();
            end
            
            % increase sequential number of recored cycles
            obj.recordedCycles = obj.recordedCycles + 1;
            
            % create a copy of currentDataStruct
            myDataStruct = obj.currentDataStruct;
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%% I am ready to talk with the user.
            
            % record new data inside obj itself
            obj.lastDataStruct=myDataStruct;
            
            % save acquired data if desired
            if obj.saveData
                try
                    auxTimeString = unixtime2HumanTime(myDataStruct(end).localTimeStamp_s);
                    auxFileName = fullfile(obj.saveDataPath, [auxTimeString '.mat']);
                    auxI = 0;
                    while exist(auxFileName, 'file') == 2
                        auxI = auxI + 1;
                        auxFileName = fullfile(obj.saveDataPath, [auxTimeString '_' num2str(auxI) '.mat']);
                    end
                    save(auxFileName,'myDataStruct')
                catch e
                    disp(['Impossible to save data: ', char(10), e.getReport]);
                end
            end
            
            % calling externalFunction if present
            if isa(obj.externalFunction, 'function_handle')
                % externalFunction is a handle
                try
                    if obj.synchronizedExternalCalls && obj.isExternalFunctionWorking
                        disp('Warning: your externalFunction is too slow for the data repetition rate. Loosing some data')
                    else
                        obj.isExternalFunctionWorking = true;
                        %obj.externalFunction(myDataStruct);
                        try
                            aux = nargin(obj.externalFunction);
                        catch e
                            % I cannot get the number of input parameters
                            % needed... sending both, hoping it is fine.
                            aux = 2;
                        end
                        switch aux
                            case 1
                                obj.externalFunction(myDataStruct);
                            case 2
                                obj.externalFunction(myDataStruct, obj);
                            case -1
                                obj.isExternalFunctionWorking = false;
                                error('matlabJapcMonitor: I cannot understand how many input has your callbackFunction. Supported are 1 or 2, and you should explicitly specify it!')
                            otherwise
                                obj.isExternalFunctionWorking = false;
                                error('Your external function has wrong number of input variables. Supported only 1 or 2.')
                        end
                        obj.isExternalFunctionWorking = false;
                    end
                catch e
                    disp(['matlabJapcMonitor: something wrong in the external function: ',char(10), e.getReport]);
                    obj.isExternalFunctionWorking = false;
                end
            else
                obj.printOut('No external function defined.');
            end
            
            % external notification of newBeam event
            notify(obj,'newBeam')
            obj.printOut(['newBeam event emitted with seq #',num2str(obj.recordedCycles-1)]);
            
            if obj.verbose
                toc
            end
        end
        
        function readyDataStruct = generateDataStructFromCurrentData(obj)
            % take an empty copy of outputStructure
            readyDataStruct = obj.emtpyDataStruct;
            
            % take a copy of all the "Last" parameter (just becaouse it could be better for synchronization issues?!)
            auxCurrentLastValues      = obj.currentLastValues;
            auxCurrentLastCycleNames  = obj.currentLastCycleNames;
            auxCurrentLastCycleStamps = obj.currentLastCycleStamps;
            auxCurrentLastTimeStamps  = obj.currentLastTimeStamps;
            auxCurrentLastErrors      = obj.currentLastErrors;
            
            % common stuff
            auxTime = etime(clock,[1970 1 1 0 0 0]);
            readyDataStruct.localTimeStamp_s = auxTime;
            readyDataStruct.localTimeStamp_ms = auxTime*1000;
            readyDataStruct.seqNumber = obj.recordedCycles;
            for i=1:length(obj.signalList)
                readyDataStruct.headerTimeStamps(i) = auxCurrentLastTimeStamps(i);
                readyDataStruct.headerCycleStamps(i) = auxCurrentLastCycleStamps(i);
            end
            
            % structure-dependent stuff.
            if obj.isUsingRawDataStruct
                % fill data structures
                for i=1:length(obj.signalList)
                    readyDataStruct.values{i}     = auxCurrentLastValues{i};
                    readyDataStruct.cycleNames{i} = auxCurrentLastCycleNames{i};
                    readyDataStruct.errors{i}     = auxCurrentLastErrors{i};
                end
            else
                % fill data structures
                for i=1:length(obj.signalList)
                    myStr = obj.signalListPaths{i};
                    switch length(myStr)
                        case 1
                            readyDataStruct.(myStr{1}).value      = auxCurrentLastValues{i};
                            readyDataStruct.(myStr{1}).timeStamp  = auxCurrentLastTimeStamps(i);
                            readyDataStruct.(myStr{1}).cycleStamp = auxCurrentLastCycleStamps(i);
                            readyDataStruct.(myStr{1}).cycleName  = auxCurrentLastCycleNames{i};
                            readyDataStruct.(myStr{1}).error      = auxCurrentLastErrors{i};
                        case 2
                            readyDataStruct.(myStr{1}).(myStr{2}).value      = auxCurrentLastValues{i};
                            readyDataStruct.(myStr{1}).(myStr{2}).timeStamp  = auxCurrentLastTimeStamps(i);
                            readyDataStruct.(myStr{1}).(myStr{2}).cycleStamp = auxCurrentLastCycleStamps(i);
                            readyDataStruct.(myStr{1}).(myStr{2}).cycleName  = auxCurrentLastCycleNames{i};
                            readyDataStruct.(myStr{1}).(myStr{2}).error      = auxCurrentLastErrors{i};
                        case 3
                            readyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).value      = auxCurrentLastValues{i};
                            readyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).timeStamp  = auxCurrentLastTimeStamps(i);
                            readyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).cycleStamp = auxCurrentLastCycleStamps(i);
                            readyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).cycleName  = auxCurrentLastCycleNames{i};
                            readyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).error      = auxCurrentLastErrors{i};
                        case 4
                            readyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).value      = auxCurrentLastValues{i};
                            readyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).timeStamp  = auxCurrentLastTimeStamps(i);
                            readyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).cycleStamp = auxCurrentLastCycleStamps(i);
                            readyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).cycleName  = auxCurrentLastCycleNames{i};
                            readyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).error      = auxCurrentLastErrors{i};
                        case 5
                            readyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).(myStr{5}).value      = auxCurrentLastValues{i};
                            readyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).(myStr{5}).timeStamp  = auxCurrentLastTimeStamps(i);
                            readyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).(myStr{5}).cycleStamp = auxCurrentLastCycleStamps(i);
                            readyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).(myStr{5}).cycleName  = auxCurrentLastCycleNames{i};
                            readyDataStruct.(myStr{1}).(myStr{2}).(myStr{3}).(myStr{4}).(myStr{5}).error      = auxCurrentLastErrors{i};
                        otherwise
                            error(['Parameter ' obj.signalList{i} ' has a too long/short structure. Not supported! Write to davide.gamba@cern.ch!'])
                    end
                end
            end
        end
        
    end
    
    % %%%%%% STATIC
    methods(Static)
    end
    % %%%%%% END STATIC
    
    % %%%%%%%%%%%%%%%%
    events
        newBeam
    end
    
end

