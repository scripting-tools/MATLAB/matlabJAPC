classdef matlabLSA
    %MATLABLSA In this class there are few static methods to interact with
    % LSA database.
    % LS2 baseline update in Dec 2018
    % For LS2 baseline update, see
    %   https://wikis.cern.ch/display/InCA/LS2+backward+incompatible+changes
    
    properties
    end
    
    methods(Static)
        
        %%% Methods to extract settings from Trim history
        function outputStruct = staticGetTrimSignals(LSAcycle, signals, date_ns)
            %outputStruct = staticGetTrimSignals(LSAcycle, signals, date_ms)
            % it will try to get the status of parameters (cell array)
            % identified by "signals" at the given date ([ns] from 1-1-1970,
            % similar to a cycleStamp or a timeStamp).
            % The outputStruct can be used to do a JAPC setSignals.
            
            % convert single char signal to cell array
            if ischar(signals)
                signals = {signals};
            end
            
            outputStruct = struct;
            for i=1:length(signals)
                try
                    tmpData = matlabLSA.staticGetTrimSignal(LSAcycle, signals{i}, date_ns);
                catch e
                    auxError = e.getReport();
                    warning(['Impossible to GET ', signals{i}, char(10), auxError]);
                    tmpData = struct;
                    tmpData.value = NaN;
                    tmpData.error = auxError;
                    tmpData.headerTimeStamp = -1;
                    tmpData.headerCycleStamp = -1;
                    tmpData.headerSelector = -1;
                end
                outputStruct = matlabDataAndSignalsHelper.simpleSetAll2Struct(signals{i}, tmpData, outputStruct);
            end
            
        end
        
        function MValue = staticGetTrimSignal(LSAcycle, signal, date_ns)
            %MValue = staticGetTrimSignal(LSAcycle, signal, date_ms)
            %it will try to get the status of the parameter
            % identified by "signal" at the given date ([ns] from 1-1-1970,
            % like a cyclestamp or a timestamp).
            % The output MValue is a struct describing the acquired data
            
            % initialise LSA services
            try
                import cern.japc.core.factory.SelectorFactory
                import cern.japc.core.Selectors
                
                parameterController = cern.lsa.client.ServiceLocator.getService('ParameterService');
                settingService = cern.lsa.client.ServiceLocator.getService('SettingService');
                requestBuilder = cern.lsa.domain.settings.ContextSettingsRequestBuilder();
            catch e
                msgID = 'matlabLSA:LSAerror';
                msgtext = 'Impossbile to initalize LSA services';
                ME = MException(msgID,msgtext);
                ME.addCause(e);
                throw(ME);
            end
            
            % get drivableContext from LSAcycle
            drivableContext = cern.inca.client.impl.context.ContextUtilities.findCycle(LSAcycle);
            if isempty(drivableContext)
                msgID = 'matlabLSA:LSAerror';
                msgtext = ['Could not found DrivableContext associated with lsaCycle ', LSAcycle];
                ME = MException(msgID,msgtext);
                throw(ME);
            end
            
            % try to get the StandAloneContext associated to the drivableContext
            standAloneContext = cern.lsa.domain.settings.Contexts.getStandAloneContext(drivableContext);
            
            % find LSA parameter
            parameterRequest = cern.lsa.domain.settings.factory.ParametersRequestBuilder();
            parameterRequest.setParameterName(signal);
            foundParams = parameterController.findParameters(parameterRequest.build());
            
            % set data to builder:
            if (nargin == 2 || isempty(date_ns))
                myRequest = requestBuilder().standAloneContext(standAloneContext).parameters(foundParams).build();
            else
                myRequest = requestBuilder().standAloneContext(standAloneContext).at(java.util.Date(date_ns*1e-6).toInstant).parameters(foundParams).build();
            end
            
            % make the actual request:
            contextSettings = settingService.findContextSettings(myRequest);
             
            % see inside if we found our parameter.
            if (isempty(contextSettings))
                auxValue = NaN(1);
                auxError = 'Empty contextSettings obtained.';
                auxHeaderCycleStamp = NaN;
                auxHeaderTimeStamp = NaN;
                auxHeaderSelector = '';
                %
                auxCreationDate = '';
                auxTrimId = NaN;
            else
                parameterSettings = contextSettings.getParameterSettings(signal);
                if (isempty(parameterSettings))
                    auxValue = NaN(1);
                    auxError = 'Empty parameterSettings obtained.';
                    auxHeaderCycleStamp = NaN;
                    auxHeaderTimeStamp = NaN;
                    auxHeaderSelector = '';
                    %
                    auxCreationDate = '';
                    auxTrimId = NaN;
                else
                    try
                        immutableValue = cern.lsa.domain.settings.Settings.computeContextValue(drivableContext, parameterSettings);
                        if drivableContext.getUser().isEmpty()
                            auxSelector = Selectors.NO_SELECTOR;
                        else
                            auxSelector = SelectorFactory.newSelector(drivableContext.getUser());
                        end
                        auxParameterValue = cern.inca.client.impl.refarchives.ArchiveUtilities.immutableValueToFailSafeParameterValue(...
                            auxSelector, signal, immutableValue);
                        
                        auxValue = matlabJapcExtractor.staticJExtractMatlabValue(auxParameterValue.getValue());
                        auxError = '';
                        
                        % extract time stamps data from header (if possible)
                        auxHeader = auxParameterValue.getHeader();
                        try
                            auxHeaderTimeStamp = int64(auxHeader.getAcqStamp());
                        catch e
                            warning(['Impossible to extract HeaderTimeStamp from ',signal,' ', char(10), e.getReport]);
                            auxHeaderTimeStamp = NaN;
                        end
                        try
                            auxHeaderCycleStamp = int64(auxHeader.getCycleStamp());
                        catch e
                            warning(['Impossible to extract HeaderCycleStamp from ',signal,' ', char(10), e.getReport]);
                            auxHeaderCycleStamp = NaN;
                        end
                        try
                            auxHeaderSelector = char(auxHeader.getSelector().toString());
                        catch e
                            warning(['Impossible to extract HeaderSelector from ',signal,' ', char(10), e.getReport]);
                            auxHeaderSelector = e.getReport;
                        end
                        
                        % try to extract some other informations
                        auxList = parameterSettings.getCurrentSettings();
                        auxListIterator = auxList.iterator();
                        if (auxListIterator.hasNext)
                            auxSetting = auxListIterator.next();
                            auxCreationDate = char(auxSetting.getCreationDate().toGMTString());
                            auxTrimId = auxSetting.getTrimId();
                        else
                            warning(['Very strange... no settings obtained for this signal?! (',signal,')'])
                            auxCreationDate = '';
                            auxTrimId = NaN;
                        end
                        if (auxListIterator.hasNext)
                            warning(['Very strange... many settings for the same parameter obtained?! (',signal,')'])
                        end
                        
                    catch e
                        auxValue = NaN(1);
                        auxError = e.getReport;
                        auxHeaderCycleStamp = NaN;
                        auxHeaderTimeStamp = NaN;
                        auxHeaderSelector = '';
                        %
                        auxCreationDate = '';
                        auxTrimId = NaN;
                        %
                        warning(['No value for: ',signal,' error:',char(13),...
                            auxError]);
                    end
                end
            end
            
            % generate output structure
            MValue = struct;
            MValue.parameter = signal;
            MValue.value = auxValue;
            MValue.error = auxError;
            MValue.headerTimeStamp = auxHeaderTimeStamp;
            MValue.headerCycleStamp = auxHeaderCycleStamp;
            MValue.headerSelector = auxHeaderSelector;
            %
            MValue.trimCreationDate = auxCreationDate;
            MValue.trimId = auxTrimId;
        end
        
        
        %%% Methods to extract Ref settings
        function MValue = staticGetRefSignal(plsCondition, signal)
            %MValue = staticGetRefSignal(plsCondition, signal)
            %
            % it will look for the refence value of the given signal and
            % plsCondition.
            %
            
            try
                % extract drivableContext used at the moment.
                drivableContext = cern.inca.client.impl.context.ContextUtilities.findDrivableContextFromPlsCondition(plsCondition);
                
                % GET the reference value from the database
                compositeContextSettings = cern.inca.client.impl.refarchives.ArchiveUtilities.findReferenceValues(drivableContext, signal);
                
                % correct for GM classes (adding value field if necessary)
                signal = char(cern.inca.client.impl.refarchives.ArchiveUtilities.getLongGMParameterName(signal));
                auxParameterValue = cern.inca.client.impl.refarchives.ArchiveUtilities.immutableValueToAcquiredParameterValue(signal, compositeContextSettings.getValue(drivableContext, signal));
                
                % converting to Matlab value
                if isempty(auxParameterValue)
                    msgID = 'matlabLSA:LSAerror';
                    msgtext = ['Impossbile to find any reference for ', signal, ' in LSA Reference database'];
                    ME = MException(msgID,msgtext);
                    %ME.addCause(e);
                    throw(ME);
                end
                auxValue = matlabJapcExtractor.staticJExtractMatlabValue(auxParameterValue.getValue());
                auxError = '';
                
                % extract time stamps data from header (if possible)
                auxHeader = auxParameterValue.getHeader();
                if isempty(auxHeader)
                    auxError = 'Empty header.';
                    auxHeaderTimeStamp = -1;
                    auxHeaderCycleStamp = -1;
                    auxHeaderSelector = -1;
                else
                    try
                        auxHeaderTimeStamp = int64(auxHeader.getAcqStamp());
                    catch e
                        warning(['Impossible to extract HeaderTimeStamp from ',signal,' ', char(10), e.getReport]);
                        auxHeaderTimeStamp = NaN;
                    end
                    try
                        auxHeaderCycleStamp = int64(auxHeader.getCycleStamp());
                    catch e
                        warning(['Impossible to extract HeaderCycleStamp from ',signal,' ', char(10), e.getReport]);
                        auxHeaderCycleStamp = NaN;
                    end
                    try
                        auxHeaderSelector = char(auxHeader.getSelector().toString());
                    catch e
                        warning(['Impossible to extract HeaderSelector from ',signal,' ', char(10), e.getReport]);
                        auxHeaderSelector = e.getReport;
                    end
                end
            catch e
                auxError = e.getReport();
                warning(['Impossbile to GET LSA Reference for ', char(signal), ':',...
                    char(10), auxError]);
                auxValue = NaN;
                auxHeaderTimeStamp = -1;
                auxHeaderCycleStamp = -1;
                auxHeaderSelector = -1;
            end
            
            % generate output structure
            MValue = struct;
            MValue.parameter = signal;
            MValue.value = auxValue;
            MValue.error = auxError;
            MValue.headerTimeStamp = auxHeaderTimeStamp;
            MValue.headerCycleStamp = auxHeaderCycleStamp;
            MValue.headerSelector = auxHeaderSelector;
        end
        
        function staticSetRefSignal(plsCondition, signal, value)
            %staticSetRefSignal(plsCondition, signal, value)
            %
            % it will set a new refence value of the given signal and
            % plsCondition.
            %
            error('matlabLSA:method not implemented yet! Contact davide.gamba@cern.ch.')
        end
        
        function MValue = staticGetArchiveSignal(archiveID, signal)
            error('matlabLSA:deprecated! Contact davide.gamba@cern.ch.')
            %MValue = staticGetArchiveSignal(archiveID, signal)
            %
            % it gets the value of a signal from an archive.
            %
            
            % initialize output struct
            MValue = cell(0);
            
            try
                % find the archive by provided ID
                archive = cern.inca.client.impl.refarchives.ArchiveUtilities.findArchiveById(archiveID);
                
                % get the archive version you need to work with
                archiveVersion = archive.getLatestVersion();
                
                % GET the archive values from the database
                compositeContextSettings = cern.inca.client.impl.refarchives.ArchiveUtilities.findArchiveSettings(archiveVersion, signal);
                
                % as the archive can contain values for several DreivableContext in teh case of a Supercycle
                % we need to get all these DrivableContext contained in the Archive StandAloneContext
                drivableContexts = cern.inca.client.impl.context.ContextUtilities.findDrivableContextsByContext(archive.getStandAloneContext());
                
                %
                if isempty(drivableContexts)
                    msgID = 'matlabLSA:LSAerror';
                    msgtext = ['Impossbile to find any drivableContexts for ', signal, ' in LSA Archive id: ',num2str(archiveID)];
                    ME = MException(msgID,msgtext);
                    %ME.addCause(e);
                    throw(ME);
                end
                
                % Getting  archives values associated with each DrivableContext
                for ctxIndex=1:length(drivableContexts)
                    %System.out.println(" Archives values for Context : " + drivableContexts[ctxIndex]);
                    % correct for GM classes (adding value field if necessary)
                    signal = cern.inca.client.impl.refarchives.ArchiveUtilities.getLongGMParameterName(signal);
                    
                    auxParameterValue = cern.inca.client.impl.refarchives.ArchiveUtilities.immutableValueToAcquiredParameterValue(signal, ...
                        compositeContextSettings.getValue(drivableContexts(ctxIndex), signal));
                    
                    % converting to Matlab value
                    if isempty(auxParameterValue)
                        msgID = 'matlabLSA:LSAerror';
                        msgtext = ['Impossbile to find an archive value for ', signal, ...
                            ' in LSA Archive id: ',num2str(archiveID), ' - drivableContexts ', drivableContexts(ctxIndex).toString()];
                        ME = MException(msgID,msgtext);
                        throw(ME);
                    end
                    
                    auxValue = matlabJapcExtractor.staticJExtractMatlabValue(auxParameterValue.getValue());
                    auxError = '';
                    
                    % extract time stamps data from header (if possible)
                    auxHeader = auxParameterValue.getHeader();
                    try
                        auxHeaderTimeStamp = int64(auxHeader.getAcqStamp());
                    catch e
                        warning(['Impossible to extract HeaderTimeStamp from ',signal,' ', char(10), e.getReport]);
                        auxHeaderTimeStamp = NaN;
                    end
                    try
                        auxHeaderCycleStamp = int64(auxHeader.getCycleStamp());
                    catch e
                        warning(['Impossible to extract HeaderCycleStamp from ',signal,' ', char(10), e.getReport]);
                        auxHeaderCycleStamp = NaN;
                    end
                    try
                        auxHeaderSelector = char(auxHeader.getSelector().toString());
                    catch e
                        warning(['Impossible to extract HeaderSelector from ',signal,' ', char(10), e.getReport]);
                        auxHeaderSelector = e.getReport;
                    end
                    
                    % generate output structure
                    auxIdx = length(MValue) + 1;
                    MValue{auxIdx}.parameter = signal;
                    MValue{auxIdx}.value = auxValue;
                    MValue{auxIdx}.error = auxError;
                    MValue{auxIdx}.headerTimeStamp = auxHeaderTimeStamp;
                    MValue{auxIdx}.headerCycleStamp = auxHeaderCycleStamp;
                    MValue{auxIdx}.headerSelector = auxHeaderSelector;
                end
                
            catch e
                auxError = e.getReport();
                warning(['Impossbile to GET LSA Archive for ', signal, ' in LSA Archive id: ',num2str(archiveID),...
                    char(10), auxError]);
                MValue = struct;
                MValue.parameter = signal;
                MValue.value = '';
                MValue.error = auxError;
                MValue.headerTimeStamp = NaN;
                MValue.headerCycleStamp = NaN;
                MValue.headerSelector = '';
                return;
            end
            
            % just convert to single struct if only one archive value found.
            if length(MValue) == 1
                MValue = MValue{1};
            end
        end
        
        
        %%% Generic useful functions
        function beamProcesses = staticGetCurrentBeamProcesses(plsCondition)
            %beamProcesses = staticGetCurrentBeamProcesses(plsCondition)
            drivableContext = cern.inca.client.impl.context.ContextUtilities.findDrivableContextFromPlsCondition(plsCondition);
            beamProcesses = drivableContext.getBeamProcesses().toArray();
        end
        
        
    end
end

