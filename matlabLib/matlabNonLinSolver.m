classdef matlabNonLinSolver < handle
    %MATLABNONLINEARSOLVER solver for non linear systems. Never been really
    %used/tested!
    % Use at your own risk!
    %
    % Generic non-linear correction class
    %
    % Davide Gamba - Guido Sterbini
    % feb 2013
    
    properties
        % as you may immagine.
        verbose=false
        debug=false
        
        % One can give more important to a particular observable or actuator
        % (!) Is a nonsense to use here a single value for all observable/actuator != 1
        observablesWeights = 1;
        
        % vector of the same size of actuators or empty
        % if you want to disable an actuator, just put its index at 0
        % this feature is used only when trying to find a solution
        enabledActuators = [];
        
        %
        actuatorVariableNames
        functionMonomialElements
        %
        % RESPONSE MATRIX:
        % The system is described by responseMatrix*evaluated(functionMonomialElements)
        %  tipically functionMonomialElements={'1','x','y','x^2','xy','y^2',...}
        responseMatrix = [];
        % This responseMatrix shape is how the response matrix should look
        % like. It has to have 0 values where the response matrix is expected
        % to be 0, values == 1 where the response matrix is expected != 0.
        % This feature should be used when you know that for example a
        % corrector cannot act on the beam position readed from a BPM upstream
        responseMatrixShape = [];
        % the error on each matrix element
        responseMatrixErrors = [];
        
        %
        % Used during the improvement process.
        % If true, the information enclosed in the current response matrix
        % are also used to determin the new one.
        % If false, only the training history is taken into account
        trustResponseMatrix = true;
        
        %
        % Used in all the operations, if an SVD value is below this limit,
        % it is set to zero for further computation.
        % It may be unsafe to set this value to zero.
        minSVDvalue = 0.000001;
        
        %
        % OTHER OPTIONS
        %
        % max size of the training value set to keep in memory
        maxHistorySize
        % decay time: time (in number of evenet) after which an historical
        %  excitation is considered as half (and so the outcome observation).
        %
        decayTime
        
        
        %% uncomment this after debug!
        %end % end public properties
        
        %% uncomment this after debug!
        %properties (Access=private)
        historyTrainingObservations = [];
        historyTrainingObservationsErrors = [];
        historyTrainingExcitationMonomials = [];

        % some sizes commonly used
        nActuators
        nMonomialElements
        nObservables
        maxDegree
        zeroDegree % true if the zero degree (1) has to be included in the polynomial expansion
    end % end private properties
    
    methods
        %
        % constructor
        %
        function obj=matlabNonLinSolver(nObservables, nActuators, maxDegree, zeroDegree)
            % initialize some sizes commonly used
            obj.nActuators = nActuators;
            obj.nObservables = nObservables;
            obj.maxDegree = maxDegree;
            if nargin < 4
                obj.zeroDegree = false;
            else
                obj.zeroDegree = logical(zeroDegree);
            end
            
            % prepare my actuator variable names
            obj.actuatorVariableNames = cell(obj.nActuators,1);
            for i=1:obj.nActuators
                obj.actuatorVariableNames{i}=['x(',num2str(i),')'];
            end
            
            % prepare my function
            obj.functionMonomialElements=allPolynomialGenerator(obj.actuatorVariableNames, obj.maxDegree, obj.zeroDegree);
            obj.nMonomialElements = numel(obj.functionMonomialElements);
            
            % now I now the dimension of the response matrix I'm looking
            obj.responseMatrix=zeros(obj.nObservables, obj.nMonomialElements);
            obj.responseMatrixErrors=zeros(obj.nObservables, obj.nMonomialElements);
            
            % reasonable maxHistory size
            %obj.decayTime = max(size(obj.responseMatrix));
            %obj.maxHistorySize = 10*obj.decayTime;
            obj.decayTime = Inf;
            obj.maxHistorySize = Inf;
        end
        
        %
        % methods
        %
        
        % get the correction to apply in order to obtain the wanted wantedObservation.
        % optimalExcitation, if given, is
        function solution = solve(obj, wantedObservation, currentActuators, lowerActuatorsLimits, upperActuatorsLimits)
            
            %
            % change behaviour depending on number of arguments
            %
            switch nargin
                case 2 % obj and wantedObservation only provided. no actuators limits provided
                    myTmpLowerActuatorsLimits = -Inf*ones(obj.nActuators, 1);
                    myTmpUpperActuatorsLimits = Inf*ones(obj.nActuators, 1);
                    myTmpcurrentActuators = zeros(obj.nActuators, 1);
                case 3 % given an initial actuator values
                    myTmpLowerActuatorsLimits = -Inf*ones(obj.nActuators, 1);
                    myTmpUpperActuatorsLimits = Inf*ones(obj.nActuators, 1);
                    myTmpcurrentActuators = currentActuators;
                case 5 % Actuators limits provided!
                    myTmpLowerActuatorsLimits = lowerActuatorsLimits;
                    myTmpUpperActuatorsLimits = upperActuatorsLimits;
                otherwise
                    obj.errorOut('Wrong number of arguments for solve function.');
                    return;
            end
            
            %
            % Consistency checks
            %
            % check the correct size of wantedObservation array
            if (min(size(wantedObservation)) ~= 1 || max(size(wantedObservation)) ~= obj.nObservables)
                obj.errorOut('Wrong wantedObservation dimension for the current response Matrix. \n Check your code!')
                return
            end
            myTmpWantedObservation = reshape(wantedObservation, obj.nObservables, 1);
            
            % check the correct size and shape of Actuators limits
            if (min(size(myTmpLowerActuatorsLimits)) ~= 1 || max(size(myTmpLowerActuatorsLimits)) ~= obj.nActuators)
                obj.errorOut('Wrong lowerActuatorsLimits dimension for the current response Matrix. \n Check your code!')
                return
            end
            myTmpLowerActuatorsLimits = reshape(myTmpLowerActuatorsLimits, obj.nActuators, 1);
            if (min(size(myTmpUpperActuatorsLimits)) ~= 1 || max(size(myTmpUpperActuatorsLimits)) ~= obj.nActuators)
                obj.errorOut('Wrong upperActuatorsLimits dimension for the current response Matrix. \n Check your code!')
                return
            end
            myTmpUpperActuatorsLimits = reshape(myTmpUpperActuatorsLimits, obj.nActuators, 1);
            
            if sum(myTmpUpperActuatorsLimits < myTmpcurrentActuators) > 0 || ...
                    sum(myTmpLowerActuatorsLimits > myTmpcurrentActuators)
                obj.errorOut('currentActuator values are not compatible with limits. \n Check your code!')
                return
            end
            
            %
            % assemble other ingredients
            %
            myTmpMatrix = matlabNonLinSolver.regolarizeMatrix(obj.responseMatrix, obj.minSVDvalue);
            myTmpObservablesWeights = obj.observablesWeights;
            
            % assemble weights
            if (length(myTmpObservablesWeights) == 1)
                obj.debugOut('No or unique observablesWeights provided. Not using this feature');
                myTmpObservablesWeights = ones(obj.nObservables,1);
            elseif (length(myTmpObservablesWeights) ~= obj.nObservables)
                obj.warningOut('Wrong number of ObservablesWeights. Not using this feature');
                myTmpObservablesWeights = ones(obj.nObservables,1);
            end
            % apply weights
            for i=1:obj.nObservables
                myTmpMatrix(i,:) = myTmpMatrix(i,:)*myTmpObservablesWeights(i);
                myTmpWantedObservation(i) = myTmpWantedObservation(i)*myTmpObservablesWeights(i);
            end
            
            %
            % Calculate solution with EXCITATION LIMITS
            %
            % http://www.mathworks.ch/ch/help/optim/ug/lsqlin.html
            % x = lsqlin(C,d,A,b,Aeq,beq,lb,ub)
            %   defines a set of lower and upper bounds on the design variables in
            %   x so that the solution is always in the range lb ??? x ??? ub.
            %   Set Aeq = [] and beq = [] if no equalities exist.
            try
                solution = lsqnonlin(@(act)obj.solverUsedFunction(myTmpWantedObservation,act),x0,myTmpLowerActuatorsLimits, myTmpUpperActuatorsLimits);
            catch exception
                newexception = MException('matlabNonLinSolver:solve', ...
                    ['Unable to use lsqnonlin with current conditions. lsqnonlin says: ', exception.message]);
                throw(newexception);
            end
        end
        
        %
        % Improves the response matrix using data that satisfy:
        % gotObservation = M_real * usedExcitation (eq. 1)
        % gotObservation and usedExcitation can be two single vectors or two matrices with the same
        %  amount of columns, each columns corresponding to a set of two vectors that satisfy
        %  (eq. 1)
        %
        function improve(obj, gotObservation, usedExcitation, gotObservationErrors)
            % Check number of arguments
            switch nargin
                case 3 % all good, but no error given on observations
                    gotObservationErrors = zeros(size(gotObservation));
                case 4 % Also the error on observation is given
                    % everything is provided
                otherwise
                    obj.errorOut('Wrong number of arguments for improve function.');
                    return;
            end
            
            % initial checks
            if (size(gotObservation,2) ~= size(usedExcitation,2))
                obj.errorOut(['gotObservation and usedExitation have ',...
                    'different number of columns. Unable to improve with this data.']);
                return
            end
            if (hasInfNaN(gotObservation) || hasInfNaN(usedExcitation) || hasInfNaN(gotObservationErrors))
                obj.warningOut('There are Inf or NaN data as input. Unable to improve matrix.')
                return
            end
            if (size(gotObservation) ~= size(gotObservationErrors))
                obj.errorOut('gotObservation and relative gotObservationErrors are different in size! Unable to continue');
                return
            end
            
            tmpHistorySize = size(obj.historyTrainingObservations,2);
            
            % just a couple of conistency checks
            if (tmpHistorySize ~= size(obj.historyTrainingExcitationMonomials,2))
                obj.debugOut('Inconsistency in the training History! Check the code! Resetting history.')
                obj.resetTrainingHistory();
            end
            if (size(gotObservation,1) ~= obj.nObservables)
                obj.errorOut('Wrong gotObservation size! Unable to improve the matrix like this');
                return;
            end
            if (size(usedExcitation,1) ~= obj.nActuators)
                obj.errorOut('Wrong usedExcitation size! Unable to improve the matrix like this');
                return;
            end
            
            % convert usedExcitation to monomial values
            usedMonomialValues=NaN(obj.nMonomialElements,size(usedExcitation,2));
            for i=1:size(usedExcitation,2)
                usedMonomialValues(:,i)=obj.actuatorsValuesToMonomialValues(usedExcitation(:,i));
            end
            if (hasInfNaN(usedMonomialValues))
                obj.errorOut('There are Inf or NaN data in calculated monomial values.')
                return;
            end

            
            % assemble history
            obj.historyTrainingObservations = [obj.historyTrainingObservations, gotObservation];
            obj.historyTrainingObservationsErrors = [obj.historyTrainingObservationsErrors, gotObservationErrors];
            obj.historyTrainingExcitationMonomials = [obj.historyTrainingExcitationMonomials, usedMonomialValues];

            
            % remove too old values
            tmpHistorySize = size(obj.historyTrainingObservations,2);
            if (tmpHistorySize > obj.maxHistorySize)
                obj.debugOut(['Removing ',num2str(tmpHistorySize-obj.maxHistorySize),' old set of values/correction from the history']);
                obj.historyTrainingObservations = obj.historyTrainingObservations(:,1+tmpHistorySize-obj.maxHistorySize:end);
                obj.historyTrainingObservationsErrors = obj.historyTrainingObservationsErrors(:,1+tmpHistorySize-obj.maxHistorySize:end);
                obj.historyTrainingExcitationMonomials = obj.historyTrainingExcitationMonomials(:,1+tmpHistorySize-obj.maxHistorySize:end);
                tmpHistorySize = obj.maxHistorySize;
            end
            
            % doing the proper job...
            
            % exponential decay in time to follow changes
            decayMatrix = zeros(tmpHistorySize);
            decayConstant = abs(log(0.5)/obj.decayTime);
            if ~isfinite(decayConstant)
                decayMatrix = 0;
            else
                for k=1:tmpHistorySize
                    decayMatrix(tmpHistorySize-k+1,tmpHistorySize-k+1) = exp(-decayConstant*(k-1));
                end
            end
            %
            allExcitations = obj.historyTrainingExcitationMonomials*decayMatrix;
            allObservables = obj.historyTrainingObservations*decayMatrix;
            allObservablesErrors = obj.historyTrainingObservationsErrors*decayMatrix;
            
            % add also current response matrix as information
            % TODO: THIS PART IS FOR SURE TO DEBUG!
            if (obj.trustResponseMatrix)
                [U, S, V] = matlabNonLinSolver.regolarizeMatrix(obj.responseMatrix, obj.minSVDvalue+1000*eps);
                Uerrors = U*obj.responseMatrixErrors;
                
                U = U*S;
                [s1,s2] = size(S);
                if s1 > s2
                    % this case is not too good...
                elseif s2 > s1
                    U(:,(s1+1):end) = [];
                    Uerrors(:,(s1+1):end) = [];
                    V(:,(s1+1):end) = [];
                end
                mySingularValues=diag(S);
                for i=1:length(mySingularValues)
                    if mySingularValues(i) == 0
                        U(:,i:end) = [];
                        Uerrors(:,i:end) = [];
                        V(:,i:end) = [];
                        mySingularValues(i:end)=[];
                        break
                    end
                end

                if numel(mySingularValues) > 0
                    allExcitations=[allExcitations, V];
                    allObservables=[allObservables, U];
                    % I don't know how to specify a good value for the error here
                    allObservablesErrors=[allObservablesErrors, Uerrors*diag(mySingularValues)];
                end
            end
            

            % check to have a proper responseMatrixShape
            if (size(obj.responseMatrixShape) ~= size(obj.responseMatrix))
                obj.debugOut('Not a good responseMatrixShape provided. Resetting to ones');
                obj.responseMatrixShape = logical(ones(obj.nObservables, obj.nMonomialElements));
            elseif ~islogical(obj.responseMatrixShape)
                obj.debugOut('responseMatrixShape is not a logical matrix. Converting it.');
                obj.responseMatrixShape = logical(obj.responseMatrixShape);
            end
            
            
            % now do the proper job, row by row of the response matrix
            errorWeights = 1./power((1+allObservablesErrors),2);
            
            % I can apply the weights to allObservables alread:
            allObservablesWeighted = allObservables.*errorWeights;
            allObservablesErrorsWeighted = allObservablesErrors.*errorWeights;
            
            
            newResponseMatrix = zeros(obj.nObservables, obj.nMonomialElements);
            newResponseMatrixErrors = zeros(obj.nObservables, obj.nMonomialElements);
            % now construct row by row the response matrix
            for i=1:obj.nObservables
                aux = pinv(allExcitations(obj.responseMatrixShape(i,:),:)*diag(errorWeights(i,:)) , obj.minSVDvalue);
                newResponseMatrix(i,obj.responseMatrixShape(i,:)) = allObservablesWeighted(i,:)*aux;
                newResponseMatrixErrors(i,obj.responseMatrixShape(i,:)) = sqrt(power(allObservablesErrorsWeighted(i,:),2)*power(aux,2));
            end
            obj.responseMatrix = newResponseMatrix;
            obj.responseMatrixErrors = newResponseMatrixErrors;
        end
        
        %
        % reset values history used to improve the response matrix
        function resetTrainingHistory(obj)
            obj.debugOut('Resetting training history...')
            obj.historyTrainingObservations = [];
            obj.historyTrainingObservationsErrors = [];
            obj.historyTrainingExcitationMonomials = [];
        end
        
        %
        % it convert some actuators values to the monomial values
        % related...
        function output=actuatorsValuesToMonomialValues(obj,aValues) 
            if numel(aValues) ~= obj.nActuators
                error('error::actuatorsValuesToExpression: wrong number of values compared to in use actuators!');
            end
            output=allPolynomialGenerator(aValues, obj.maxDegree, obj.zeroDegree)';
        end
        
        function [outputValues, outputErrors]=actuatorsValuesToMonomialValuesWithErrors(obj,aValues,aErrors) 
            if numel(aValues) ~= obj.nActuators
                error('error::actuatorsValuesToExpression: wrong number of values compared to in use actuators!');
            end
            [outputValues, outputErrors]=allPolynomialGeneratorWithErrors(aValues, aErrors, obj.maxDegree, obj.zeroDegree);
            outputValues=outputValues';
            outputErrors=outputErrors';
        end
        
        
        %
        % is the function used by the solver to reach wanted observation.
        function output=solverUsedFunction(obj,wantedObservation,values)
            if numel(wantedObservation) ~= obj.nMonomialElements
                error(['error::solverUsedFunction: wrong size for wanted observation. ',...
                    num2str(obj.nMonomialElements),' expected']);
            end
            output=obj.evaluateSystem(values)-wantedObservation;
        end
        
        %
        % is the function used by the solver to reach wanted observation.
        function output=evaluateSystem(obj,actuatorValues)
            if numel(actuatorValues) ~= obj.nActuators
                error('error::evaluateSystem: wrong number of values compared to in use actuators!');
            end
            output=obj.responseMatrix*obj.actuatorsValuesToMonomialValues(actuatorValues);
        end
        
        function delete(obj)
            obj.debugOut('Delete handler called (matlabNonLinSolver)...')
        end
    end % end public methods
    
    methods (Static)
        function varargout = regolarizeMatrix(matrix, minSVD)
            %regolarizeMatrix [ argout ] = regolarizeMatrix(matrix, minSVD)
            % given an input matrix and a minimum svd value, it returns a new
            % matrix where all singular values <= minSVD are set to 0;
            % if nargout = 1 -> it return the full matrix recomposed
            % if nargout = 3 -> it return the three matrices U S V of SVD
            %   decomposition
            
            [U S V] = svd(matrix);
            nsingulars = min(size(matrix));
            for i=nsingulars:-1:1
                if S(i,i) <= minSVD
                    S(i,i) = 0;
                else
                    %disp(['There were ',num2str(i),'/',num2str(nsingulars),' good svds.'])
                    break;
                end
                
                if i == 1
                    disp(['Warning! There was no good SVD > ',num2str(minSVD)])
                end
            end
            
            if nargout < 2
                varargout{1} = U*S*(V');
            elseif nargout == 3
                varargout{1} = U;
                varargout{2} = S;
                varargout{3} = V;
            else
                error(['Wrong number of output requested:', char(10),...
                    '1 to obtain the filtered matrix only, 3 to obtain its SVD decomposition']);
            end
        end
        
        function output = myGeneralWeightedEvaluationFunction(x, matrix, matrixErrors, wantedValue, wantedValueErrors)
            output = matrix*x-wantedValue;
            weights = 1+power(matrixErrors*x,2) + power(wantedValueErrors,2);
            %
            output = output./weights;
        end
    end
    
    methods (Access=private)
        function debugOut(obj,text)
            if (obj.debug)
                disp(text);
            end
        end
        function warningOut(obj,text)
            if (obj.verbose || obj.debug)
                disp(['WARNING:matlabNonLinSolver: ',text]);
            end
        end
        function errorOut(obj,text)
            disp(['ERROR:matlabNonLinSolver: ',text]);
        end
    end % end private methods
end

