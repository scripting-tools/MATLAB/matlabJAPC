classdef matlabSolver < handle
    %MATLABSOLVER Generic solver for linear sistems.
    %
    % Generic linear correction class
    %
    % Davide Gamba - Guido Sterbini
    % April 2014
    
    properties
        % as you may immagine.
        verbose=false
        debug=false
        
        % One can give more important to a particular observable or actuator
        % (!) Is a nonsense to use here a single value for all observable/actuator ~= 1
        observablesWeights = 1;
        
        % indexes of response matrix columns that are "disabled"
        % for those, the response matrix used for solving system will be 0.
        disabledActuatorsIdx = [];
        
        %
        % RESPONSE MATRIX
        responseMatrix = [];
        
        % This responseMatrix shape is how the response matrix should look
        % like. It has to have 0 values where the response matrix is expected
        % to be 0, values == 1 where the response matrix is expected != 0.
        % This feature should be used when you know that for example a
        % corrector cannot act on the beam position readed from a BPM upstream
        responseMatrixShape = [];
        
        % the error on each matrix element
        responseMatrixErrors = [];
        
        %
        % Used during the improvement process.
        % If > 0, the information enclosed in the current response matrix
        % are also used to determin the new one.
        % If == 0, only the training history is taken into account
        trustResponseMatrixWeight = 0;
        
        %
        % Used during the improvement process.
        % If > 0 it makes it avoid expoding of RM in case of noise. Usually
        % it should be set to a value of the average RM element expected
        learningRegolarizationWeight = 0;
        
        %
        % Used during solving
        % If > 0 it makes the solution to be also closer to the nominal a
        % wanted value (given when calling the solve method, zero
        % otherwise).
        excitationOptimizationWeight = 0;
        
        %
        % Used during solving
        % If > 0 it makes the solution be sort of smooth in the actuators.
        excitationSmoothingWeight = 0;
        
        %
        % OTHER OPTIONS
        %
        % max size of the training value set to keep in memory
        maxHistorySize = Inf;
        % decay time: time (in number of evenet) after which an historical
        %  excitation is considered as half (and so the outcome observation).
        %
        decayTime = Inf;
        
        %
        % new feature: stochastic learning
        %
        useStochasticLearning = false;
        useStochasticLearningAlpha = 0.01;
        useStochasticLearningRepetitions = 1;
        
        %% uncomment this after debug!
        %end % end public properties
        %% uncomment this after debug!
        %properties (Access=private)
        
        historyTrainingObservations = [];
        historyTrainingObservationsErrors = [];
        historyTrainingExcitations = [];
        
        % some sizes commonly used
        nActuators
        nObservables
    end % end private properties
    
    methods
        %
        % constructor
        %
        function obj=matlabSolver(arg1, arg2)
            % I'm expecting the dimensions of the repose matrix or an initial response matrix.
            % arg1 = nRow
            % arg2 = nColumns
            %
            % - or -
            %
            % arg1 = initial response matrix
            %
            
            if (nargin == 2)
                % just setup a eye reponseMatrix with the right dimension
                obj.responseMatrix = eye(arg1, arg2);
            elseif (nargin == 1)
                % utilize given response matrix
                obj.responseMatrix = arg1;
            else
                obj.errorOut('expecting one (initial Response Matrix) or two arguments (nObservables, nActuators)');
            end
            
            % reasonable maxHistory size
            obj.maxHistorySize = 10*obj.decayTime;
            
            % initialize some sizes commonly used
            obj.nActuators = size(obj.responseMatrix,2);
            obj.nObservables = size(obj.responseMatrix,1);
            
            % create responseMatrixErrors
            obj.responseMatrixErrors=zeros(size(obj.responseMatrix));
            
        end
        
        %
        % Copy function
        %
        function objCopy = copy(obj)
            objCopy = matlabSolver(obj.responseMatrix);
            
            objCopy.verbose =obj.verbose;
            objCopy.debug = obj.debug;
            objCopy.observablesWeights = obj.observablesWeights;
            objCopy.responseMatrixShape = obj.responseMatrixShape;
            objCopy.responseMatrixErrors = obj.responseMatrixErrors;
            objCopy.trustResponseMatrixWeight = obj.trustResponseMatrixWeight;
            
            objCopy.learningRegolarizationWeight = obj.learningRegolarizationWeight;
            objCopy.excitationOptimizationWeight = obj.excitationOptimizationWeight;
            objCopy.excitationSmoothingWeight = obj.excitationSmoothingWeight;
            
            objCopy.useStochasticLearning = obj.useStochasticLearning;
            objCopy.useStochasticLearningAlpha = obj.useStochasticLearningAlpha;
            objCopy.useStochasticLearningRepetitions = obj.useStochasticLearningRepetitions;
            
            objCopy.maxHistorySize = obj.maxHistorySize;
            objCopy.decayTime = obj.decayTime;
            objCopy.historyTrainingObservations = obj.historyTrainingObservations;
            objCopy.historyTrainingObservationsErrors = obj.historyTrainingObservationsErrors;
            objCopy.historyTrainingExcitations = obj.historyTrainingExcitations;
        end
        
        %
        % methods
        %
        

        function varargout = solve(obj, wantedObservation, ...
                lowerActuatorsLimits, upperActuatorsLimits,...
                optimalExcitation)
            % get the correction to apply in order to obtain the wanted wantedObservation.
            % optimalExcitation, if given, is the optimal excitation one would
            % love to have. This is weighted by obj.excitationOptimizationWeight
            
            %
            % change behaviour depending on number of arguments
            %
            switch nargin
                case 2 % obj and wantedObservation only provided. no actuators limits provided
                    myTmpLowerActuatorsLimits = -Inf*ones(obj.nActuators, 1);
                    myTmpUpperActuatorsLimits = Inf*ones(obj.nActuators, 1);
                    myTmpOptimalExcitation = zeros(obj.nActuators, 1);
                case 4 % Actuators limits provided!
                    myTmpLowerActuatorsLimits = lowerActuatorsLimits;
                    myTmpUpperActuatorsLimits = upperActuatorsLimits;
                    myTmpOptimalExcitation = zeros(obj.nActuators, 1);
                case 5
                    myTmpLowerActuatorsLimits = lowerActuatorsLimits;
                    myTmpUpperActuatorsLimits = upperActuatorsLimits;
                    myTmpOptimalExcitation = optimalExcitation;
                otherwise
                    obj.errorOut('Wrong number of arguments for solve function.');
                    return;
            end
            
            % check number of output requested
            % if two, first is solution, second is error done.
            if nargout > 2
                obj.errorOut('Wrong number of output arguments for solve function.');
                return
            end
            
            %
            % Consistency checks
            %
            % check the correct size of wantedObservation array
            myTmpWantedObservation = wantedObservation(:);
            if (length(myTmpWantedObservation) ~= obj.nObservables)
                obj.errorOut('Wrong wantedObservation dimension for the current response Matrix. \n Check your code!')
                return
            end
            
            % check the correct size and shape of Actuators limits
            myTmpLowerActuatorsLimits = myTmpLowerActuatorsLimits(:);
            if (length(myTmpLowerActuatorsLimits) ~= obj.nActuators)
                obj.errorOut('Wrong lowerActuatorsLimits dimension for the current response Matrix. \n Check your code!')
                return
            end
            
            myTmpUpperActuatorsLimits = myTmpUpperActuatorsLimits(:);
            if (length(myTmpUpperActuatorsLimits) ~= obj.nActuators)
                obj.errorOut('Wrong upperActuatorsLimits dimension for the current response Matrix. \n Check your code!')
                return
            end
            
            
            % apply weights
            [myWeightedRM, myWeightedWantedObservation] = obj.weightResponseMatrixAndWantedObservation(myTmpWantedObservation);
            
            % remove disabled correctors
            [myWeightedRM, myGoodCorrectorsIdx] = obj.removeDisabledCorrectors(myWeightedRM, myTmpLowerActuatorsLimits, myTmpUpperActuatorsLimits);
            myTmpOptimalExcitation = myTmpOptimalExcitation(myGoodCorrectorsIdx);
            
            % add smoothing and excitation minimization features to RM
            [myWeightedRM, myWeightedWantedObservation] = obj.applySmoothingAndMinimizationFeature(myWeightedRM, myWeightedWantedObservation, myTmpOptimalExcitation);
            
            if hasInfNaN(myWeightedRM) || hasInfNaN(myWeightedWantedObservation)
                obj.errorOut('matlabSolver::solve: There are NaN or Inf in the reconstructed Response Matrix or wanted Observation. Impossible to continue.')
                return
            end
            
            %
            % Calculate solution with EXCITATION LIMITS
            %
            % http://www.mathworks.ch/ch/help/optim/ug/lsqlin.html
            % x = lsqlin(C,d,A,b,Aeq,beq,lb,ub)
            %   defines a set of lower and upper bounds on the design variables in
            %   x so that the solution is always in the range lb ??? x ??? ub.
            %   Set Aeq = [] and beq = [] if no equalities exist.
            try
                solution = lsqlin(myWeightedRM, myWeightedWantedObservation, [], [], [], [], ...
                    myTmpLowerActuatorsLimits(myGoodCorrectorsIdx), myTmpUpperActuatorsLimits(myGoodCorrectorsIdx), ...
                    (myTmpUpperActuatorsLimits(myGoodCorrectorsIdx)+myTmpLowerActuatorsLimits(myGoodCorrectorsIdx))/2,...
                    optimset('Display','off'));
                %                 solution = lsqlin(myWeightedRM, myWeightedWantedObservation, [], [], [], [], ...
                %                     myTmpLowerActuatorsLimits(myGoodCorrectorsIdx), myTmpUpperActuatorsLimits(myGoodCorrectorsIdx), ...
                %                     (myTmpUpperActuatorsLimits(myGoodCorrectorsIdx)-myTmpLowerActuatorsLimits(myGoodCorrectorsIdx))/2,...
                %                     optimset('Display','off','LargeScale','off'));
                if length(solution) ~= size(myWeightedRM,2)
                    % this lsqlin sometimes is stupid.. solution not found!
                    newexception = MException('matlabSolver:solve', ...
                        'lsqlin did something wired... forgive it...');
                    throw(newexception);
                end
            catch exception
                newexception = MException('matlabSolver:solve', ...
                    ['Unable to use lsqlin with current conditions. lsqlin says: ', exception.getReport]);
                throw(newexception);
            end
            
            varargout{1} = zeros(obj.nActuators,1);
            varargout{1}(myGoodCorrectorsIdx) = solution;
            if nargout == 2
                % compute the solution error.
                varargout{2} = sqrt(power(obj.responseMatrixErrors,2)*power(varargout{1},2));
            end
        end
        
        %
        function improve(obj, gotObservation, usedExcitation, gotObservationErrors)
            % obj.improve(gotObservation, usedExcitation, [gotObservationErrors])
            % Improves the response matrix using data that satisfy:
            % gotObservation = M_real * usedExcitation (eq. 1)
            % gotObservation and usedExcitation can be two single vectors or two matrices with the same
            %  amount of columns, each columns corresponding to a set of two vectors that satisfy
            %  (eq. 1)
            %
            
            % Check number of arguments
            switch nargin
                case 3 % all good, but no error given on observations
                    gotObservationErrors = zeros(size(gotObservation));
                case 4 % Also the error on observation is given
                    % everything is provided
                otherwise
                    obj.errorOut('Wrong number of arguments for improve function.');
                    return;
            end
            
            % initial checks
            if (size(gotObservation,2) ~= size(usedExcitation,2))
                obj.errorOut(['gotObservation and usedExitation have ',...
                    'different number of columns. Unable to improve with this data.']);
                return
            end
            if (hasInfNaN(gotObservation) || hasInfNaN(usedExcitation) || hasInfNaN(gotObservationErrors))
                obj.warningOut('There are Inf or NaN data as input. Unable to improve matrix.')
                return
            end
            if (size(gotObservation) ~= size(gotObservationErrors))
                obj.errorOut('gotObservation and relative gotObservationErrors are different in size! Unable to continue');
                return
            end
            if norm(usedExcitation) < 2*max(size(usedExcitation))*eps
                obj.warningOut('usedExcitation is very small or zero! no data to extract from here');
                return
            end
            % check to have a proper responseMatrixShape
            if (size(obj.responseMatrixShape) ~= size(obj.responseMatrix))
                obj.debugOut('Not a good responseMatrixShape provided. Resetting to ones');
                obj.responseMatrixShape = true(obj.nObservables, obj.nActuators);
            elseif ~islogical(obj.responseMatrixShape)
                obj.debugOut('responseMatrixShape is not a logical matrix. Converting it.');
                obj.responseMatrixShape = logical(obj.responseMatrixShape);
            end
            
            % adding new data to history
            tmpHistorySize = size(obj.historyTrainingObservations,2);
            % just a couple of consistency checks on history
            if (tmpHistorySize ~= size(obj.historyTrainingExcitations,2))
                obj.debugOut('Inconsistency in the training History! Check the code! Resetting history.')
                obj.resetTrainingHistory();
            end
            if (size(gotObservation,1) ~= obj.nObservables)
                obj.errorOut('Wrong gotObservation size! Unable to improve the matrix like this');
                return;
            end
            if (size(usedExcitation,1) ~= obj.nActuators)
                obj.errorOut('Wrong usedExcitation size! Unable to improve the matrix like this');
                return;
            end
            % assemble history
            obj.historyTrainingObservations = [obj.historyTrainingObservations, gotObservation];
            obj.historyTrainingObservationsErrors = [obj.historyTrainingObservationsErrors, gotObservationErrors];
            obj.historyTrainingExcitations = [obj.historyTrainingExcitations, usedExcitation];
            % remove too old values
            tmpHistorySize = size(obj.historyTrainingObservations,2);
            if (tmpHistorySize > obj.maxHistorySize)
                obj.debugOut(['Removing ',num2str(tmpHistorySize-obj.maxHistorySize),' old set of values/correction from the history']);
                obj.historyTrainingObservations = obj.historyTrainingObservations(:,1+tmpHistorySize-obj.maxHistorySize:end);
                obj.historyTrainingObservationsErrors = obj.historyTrainingObservationsErrors(:,1+tmpHistorySize-obj.maxHistorySize:end);
                obj.historyTrainingExcitations = obj.historyTrainingExcitations(:,1+tmpHistorySize-obj.maxHistorySize:end);
                tmpHistorySize = obj.maxHistorySize;
            end
            
            % generate new empty matrices
            newResponseMatrix = zeros(obj.nObservables, obj.nActuators);
            newResponseMatrixErrors = zeros(obj.nObservables, obj.nActuators);
            
            
            % exponential decay in time to follow changes
            decayMatrix = zeros(tmpHistorySize);
            decayConstant = abs(log(0.5)/obj.decayTime);
            allExcitations = usedExcitation;
            allObservables = gotObservation;
            allObservablesErrors = gotObservationErrors;
            if ~isfinite(decayConstant)
                obj.warningOut('You may have 0 as decay time. This lead to not using the history!');
            else
                for k=1:tmpHistorySize
                    decayMatrix(tmpHistorySize-k+1,tmpHistorySize-k+1) = exp(-decayConstant*(k-1));
                end
                allExcitations = obj.historyTrainingExcitations*decayMatrix;
                allObservables = obj.historyTrainingObservations*decayMatrix;
                allObservablesErrors = obj.historyTrainingObservationsErrors*decayMatrix;
            end
            
            % add also current response matrix as information
            % this is necessary only if NOT using the stochastic
            % learning
            if ((obj.trustResponseMatrixWeight > 0) && ~obj.useStochasticLearning)
                [U, S, V] = matlabSolver.regolarizeMatrix(obj.responseMatrix.*obj.trustResponseMatrixWeight, (obj.nActuators*obj.nObservables)*eps);
                U = U*S;
                mySingularValues=diag(S);
                for i=1:length(mySingularValues)
                    if mySingularValues(i) == 0
                        U(:,i:end) = [];
                        V(:,i:end) = [];
                        mySingularValues(i:end)=[];
                        break
                    end
                end
                
                if numel(mySingularValues) > 0
                    % FIXME: I still don't know if I should use the error
                    % or not.
                    Uerrors = sqrt(power(obj.responseMatrixErrors.*obj.trustResponseMatrixWeight,2)*power(V,2));
                    Uerrors = zeros(size(Uerrors)); % NOT USING THE ERROR...:(
                    
                    allExcitations=[allExcitations, V];
                    allObservables=[allObservables, U];
                    allObservablesErrors=[allObservablesErrors, Uerrors];
                else
                    obj.warningOut('The current responseMatrix seems not to have any useful information inside.');
                end
            end
            
            
            % calculate some weight based on observables errors.
            errorWeights = 1./(1+power((allObservablesErrors),2));
            % errorWeights = ones(size(allObservablesErrors));
            
            % I can apply the weights to allObservables already:
            allObservablesWeighted = allObservables.*errorWeights;
            allObservablesErrorsWeighted = allObservablesErrors.*errorWeights;
            
            % now construct row by row the response matrix
            for i=1:obj.nObservables
                tmpWeightedExcitations = allExcitations(obj.responseMatrixShape(i,:),:)*diag(errorWeights(i,:));
                
                if obj.useStochasticLearning
                    auxNdata = size(allExcitations,2);
                    newResponseMatrix(i,obj.responseMatrixShape(i,:)) = obj.responseMatrix(i,obj.responseMatrixShape(i,:));
                    
                    for iRepetition = 1:obj.useStochasticLearningRepetitions
                        for iData = randperm(auxNdata)
                            auxStocExcitation = tmpWeightedExcitations(:,iData);
                            auxStocObservation = allObservablesWeighted(i,iData);
                            
                            auxCurrentRMpart = newResponseMatrix(i,obj.responseMatrixShape(i,:));
                            newResponseMatrix(i,obj.responseMatrixShape(i,:)) = ...
                                auxCurrentRMpart*(1-obj.useStochasticLearningAlpha*obj.learningRegolarizationWeight) - ...
                                obj.useStochasticLearningAlpha*(auxCurrentRMpart*auxStocExcitation - auxStocObservation)*auxStocExcitation';
                            
                        end
                    end
                else
                    tmpRegolarizationMatrix = obj.learningRegolarizationWeight.*eye(size(tmpWeightedExcitations,1));
                    
                    tmpInverseWeightedExcitations = pinv(tmpWeightedExcitations*tmpWeightedExcitations' + tmpRegolarizationMatrix)*...
                        tmpWeightedExcitations;
                    
                    newResponseMatrix(i,obj.responseMatrixShape(i,:)) = tmpInverseWeightedExcitations*(allObservablesWeighted(i,:)');
                    
                    % just from error propagation + naive interpretation of
                    % residuals
                    % sigma_M^T=sqrt(sum((c^T^-1 * sigma_b^T)^2))
                    % to sigma_b I add in quadrature the error that I am doing
                    % on the history data.
                    % finally:
                    %  Sigma_M = sqrt( [(sigma_b)^2 + (newM*historyC - historyB)^2]*[c^(-1)^2])
                    % newResponseMatrixErrors(i,obj.responseMatrixShape(i,:)) = sqrt(power(allObservablesErrorsWeighted(i,:),2)*power(aux,2));
                    newMatrixErrorOnHistorySquare = 1*power(newResponseMatrix(i,obj.responseMatrixShape(i,:))*tmpWeightedExcitations - allObservablesWeighted(i,:),2);
                    newResponseMatrixErrors(i,obj.responseMatrixShape(i,:)) = sqrt((power(allObservablesErrorsWeighted(i,:),2)+newMatrixErrorOnHistorySquare)*power(tmpInverseWeightedExcitations',2));
                end
            end
            
            % set new RM to the object
            obj.responseMatrix = newResponseMatrix;
            obj.responseMatrixErrors = newResponseMatrixErrors;
        end
        

        
        
        function removableExcitation = computeRemovableExcitation(obj, fromExcitation)
            %removableExcitation = computeRemovableExcitation(obj, fromExcitation)
            % it tryis to compute the excitation that is not doing anything
            % on the observables of the system, i.e. some excitation that
            % you can just remove and your system will not move (if the RM
            % is right! ;))
            
            auxRM = [obj.responseMatrix; eye(obj.nActuators)];
            auxOutcome = [zeros(obj.nObservables,1); fromExcitation];
            removableExcitation = pinv(auxRM)*auxOutcome;
        end
        
        
        function varargout = computeOutcome(obj, excitation)
            % obj.computeOutcome(excitation)
            % It is just a multiplication between the response matrix and the
            % excitation. If required, it will give also the error according to
            % response matrix error.
            
            outcome = obj.responseMatrix * excitation;
            varargout{1} = outcome;
            if nargout == 2
                % compute the solution error.
                varargout{2} = sqrt(power(obj.responseMatrixErrors,2)*power(excitation,2));
            end
        end
        
        %
        function resetTrainingHistory(obj)
            % reset values history used to improve the response matrix
            obj.debugOut('Resetting training history...')
            obj.historyTrainingObservations = [];
            obj.historyTrainingObservationsErrors = [];
            obj.historyTrainingExcitations = [];
        end
        
        function varargout = findBestRegularizationWeight(obj, minValue, maxValue, nSteps, nIterationsPerStep)
            % findBestRegularizationWeight(obj, maxValue, nSteps, nIterationsPerStep)
            
            if nargin == 4
                nIterationsPerStep = 1;
            end
            
            % initialize used variables
            myLambdas = linspace(minValue,maxValue,nSteps);
            myCostsTrain = zeros(nSteps,1);
            myCostsCv = zeros(nSteps,1);
            
            auxHistSize = size(obj.historyTrainingObservations,2);
            myNTrain = round(auxHistSize*2/3);
            myNCv = auxHistSize-myNTrain;
            if myNTrain <= 10 || myNCv <= 10
                % error('Impossible to find best regularization weight: too small data history.')
            end
            
            % do many iterations for all the lambda values and average
            % Cost function for training and control data set
            for iIteration = 1:nIterationsPerStep
                % generate a random permutation of history data and get the
                % training data set and control data set.
                myRandIdx = randperm(auxHistSize);
                
                myObservationsTrain = obj.historyTrainingObservations(:,myRandIdx(1:myNTrain));
                myExcitationsTrain = obj.historyTrainingExcitations(:,myRandIdx(1:myNTrain));
                myObservationsCv = obj.historyTrainingObservations(:,myRandIdx(myNTrain+1:end));
                myExcitationsCv = obj.historyTrainingExcitations(:,myRandIdx(myNTrain+1:end));
                
                for i = 1:nSteps
                    auxRM = obj.simpleComputeNewRM(myObservationsTrain, myExcitationsTrain, myLambdas(i));
                    myCostsTrain(i) = myCostsTrain(i) + norm(myObservationsTrain-auxRM*myExcitationsTrain,'fro')/sqrt(myNTrain*obj.nObservables);
                    myCostsCv(i) = myCostsCv(i) + norm(myObservationsCv-auxRM*myExcitationsCv,'fro')/sqrt(myNCv*obj.nObservables);
                end
            end
            % do averaging
            myCostsTrain = myCostsTrain./nIterationsPerStep;
            myCostsCv = myCostsCv./nIterationsPerStep;

            [~, bestLambda] = min(myCostsCv);
            bestLambda = myLambdas(bestLambda);
            
            if nargout == 1
                varargout{1} = bestLambda;
            else
                varargout{1} = bestLambda;
                varargout{2} = myCostsTrain;
                varargout{3} = myCostsCv;
            end
        end
        
        function newRM = simpleComputeNewRM(obj, observations, excitations, lambda)
            % obj.simpleComputeNewRM(observations, excitations, lambda)
            % simple function to compute a new RM based on "observations" and
            % "excitations".
            % "lambda" is used as regolarization parameter.
            
            % generate new empty matrices
            newRM = zeros(obj.nObservables, obj.nActuators);
            for i=1:obj.nObservables
                tmpWeightedExcitations = excitations(obj.responseMatrixShape(i,:),:);
                
                if obj.useStochasticLearning
                    auxNdata = size(excitations,2);
                    newRM(i,obj.responseMatrixShape(i,:)) = newRM(i,obj.responseMatrixShape(i,:));
                    
                    for iRepetition = 1:obj.useStochasticLearningRepetitions
                        for iData = randperm(auxNdata)
                            auxStocExcitation = tmpWeightedExcitations(:,iData);
                            auxStocObservation = observations(i,iData);
                            
                            auxCurrentRMpart = newRM(i,obj.responseMatrixShape(i,:));
                            newRM(i,obj.responseMatrixShape(i,:)) = ...
                                auxCurrentRMpart*(1-obj.useStochasticLearningAlpha*lambda) - ...
                                obj.useStochasticLearningAlpha*(auxCurrentRMpart*auxStocExcitation - auxStocObservation)*auxStocExcitation';
                            
                        end
                    end
                else
                    tmpRegolarizationMatrix = lambda.*eye(size(tmpWeightedExcitations,1));
                    
                    tmpInverseWeightedExcitations = pinv(tmpWeightedExcitations*tmpWeightedExcitations' + tmpRegolarizationMatrix)*...
                        tmpWeightedExcitations;
                    
                    newRM(i,obj.responseMatrixShape(i,:)) = tmpInverseWeightedExcitations*(observations(i,:)');
                end
            end
        end
        
        function delete(obj)
            obj.debugOut('Delete handler called...')
        end
    end % end public methods
    
    methods (Static)
        function varargout = regolarizeMatrix(matrix, minSVD)
            %regolarizeMatrix [ argout ] = regolarizeMatrix(matrix, minSVD)
            % given an input matrix and a minimum svd value, it returns a new
            % matrix where all singular values <= minSVD are set to 0;
            % if nargout = 1 -> it return the full matrix recomposed
            % if nargout = 3 -> it return the three matrices U S V of SVD
            %   decomposition
            
            [U S V] = svd(matrix);
            nsingulars = min(size(matrix));
            for i=nsingulars:-1:1
                if S(i,i) <= minSVD
                    S(i,i) = 0;
                else
                    %disp(['There were ',num2str(i),'/',num2str(nsingulars),' good svds.'])
                    break;
                end
                
                if i == 1
                    disp(['Warning! There was no good SVD > ',num2str(minSVD)])
                end
            end
            
            if nargout < 2
                varargout{1} = U*S*(V');
            elseif nargout == 3
                varargout{1} = U;
                varargout{2} = S;
                varargout{3} = V;
            else
                error(['Wrong number of output requested:', char(10),...
                    '1 to obtain the filtered matrix only, 3 to obtain its SVD decomposition']);
            end
        end
    end
    
    methods (Access=private)
        function [weightedRM, weightedWantedObservation] = weightResponseMatrixAndWantedObservation(obj, wantedObservation)
            if (length(wantedObservation) ~= obj.nObservables)
                obj.errorOut('wantedObservation length is not consistent with number of system observables.')
                return
            end
            
            myTmpMatrix = obj.responseMatrix;
            myTmpObservablesWeights = obj.observablesWeights;
            
            % assemble weights
            if (length(myTmpObservablesWeights) == 1)
                obj.debugOut('No or unique observablesWeights provided. Not using this feature');
                myTmpObservablesWeights = ones(obj.nObservables,1);
            elseif (length(myTmpObservablesWeights) ~= obj.nObservables)
                obj.warningOut('Wrong number of ObservablesWeights. Not using this feature');
                myTmpObservablesWeights = ones(obj.nObservables,1);
            end
            
            % apply weights to RM and observables
            for i=1:obj.nObservables
                myTmpMatrix(i,:) = myTmpMatrix(i,:)*myTmpObservablesWeights(i);
                wantedObservation(i) = wantedObservation(i)*myTmpObservablesWeights(i);
            end
            weightedRM = myTmpMatrix;
            weightedWantedObservation = wantedObservation;
        end
        
        function [weightedRM, weightedWantedObservation] = applySmoothingAndMinimizationFeature(obj, weightedRM, wantedObservation, wantedExcitation)
            auxNCorr = size(weightedRM,2);
            %
            if nargin == 3;
                wantedExcitation = zeros(auxNCorr,1);
            end
            
            % create some matrixes
            auxId = eye(auxNCorr);
            auxSmooth = eye(auxNCorr);
            for i=2:auxNCorr
                auxSmooth(i-1,i) = -1;
                auxSmooth(i,i-1) = -1;
            end
            
            % compose new RM
            weightedRM = [weightedRM; ...
                obj.excitationOptimizationWeight.*auxId; ...
                obj.excitationSmoothingWeight.*auxSmooth; ...
                ];
            
            weightedWantedObservation = [wantedObservation; ...
                obj.excitationOptimizationWeight.*wantedExcitation; ...
                zeros(auxNCorr,1); ...
                ];
        end
        
        function [RM, goodCorrectorsIdx] = removeDisabledCorrectors(obj, RM, lowerActuatorsLimits, upperActuatorsLimits)
            % consider first limits
            auxIdx = lowerActuatorsLimits >= upperActuatorsLimits;
            
            % then internally disabled correctors
            auxIdx(obj.disabledActuatorsIdx) = 1;
            
            % keep trace of what we saved
            goodCorrectorsIdx = ~auxIdx;
            
            % disable unwanted correctors
            try
                RM(:,auxIdx) = [];
            catch e
                obj.errorOut(['Unable to disable (un)wanted correctors (',num2str(auxIdx),'): ',e.message]);
            end
        end
        
        function debugOut(obj,text)
            if (obj.debug)
                disp(text);
            end
        end
        function warningOut(obj,text)
            if (obj.verbose || obj.debug)
                disp(['WARNING:matlabSolver: ',text]);
            end
        end
        function errorOut(obj,text)
            disp(['ERROR:matlabSolver: ',text]);
        end
    end % end private methods
end

