classdef matlabeLogbookEvent
    %MATLABELOGBOOKEVENT Summary of this class goes here
    %   This class just create a new logbook entry and give the possibility
    %   to add an attachment or change the content later
    
    properties(Access=private)
        myElogbookEventReference
    end
    
    methods
        function obj = matlabeLogbookEvent(logbookName, inputText)
            % obj = matlabeLogbookEvent(logbookName, inputText)
            % A possible logbookName is 'CTF' for CTF3OP
            % inputText is the content of the event you want to create.
            % (default = '')
            
            if nargin == 0
                error('The first argument to create a new event has to be a correct logbook name. For CTF3 is ''CTF3''');
            end
            if nargin == 1
                inputText = '';
            end
            % create event
            obj.myElogbookEventReference = cern.op.elogbook.client.ELBExternalSources.addEvent(logbookName,inputText);
        end
        
        function setNewTextContent(obj, inputText)
            % setNewTextContent(obj, inputText)
            % it just set the content of the holded event as inputText.
            % This will overwrite the current content!!!
            cern.op.elogbook.client.ELBExternalSources.updEventComment(obj.myElogbookEventReference,inputText);
        end
        
        function addNewAttachment(obj, filename, comment)
            % addNewAttachment(obj, filename, comment)
            % it will attach the file identified by filename to the holded
            % event. If you want you can also specify a comment for it,
            % default = '';
            
            if nargin == 2
                comment = '';
            end
            
            auxj=java.io.File(filename);
            cern.op.elogbook.client.ELBExternalSources.updEventAddAttachment(obj.myElogbookEventReference,comment,auxj);
        end
        function changeEventTag(obj, newTag)
            % changeEventTag(obj, newTag)
            % changes the tag of an event. 
            % the newTag must be like the following example:
            % For the elogbook TESTS, tag should look like this TestTagGrp:L11.L21
            cern.op.elogbook.client.ELBExternalSources.updEventSetTag(obj.myElogbookEventReference, newTag);
        end
    end
    
end

