function output_args = unixtime2HumanTime( input_args )
% output_args = unixtime2HumanTime( input_args )
% input_args is a unix time stamps (seconds since Jan 1, 1970)
% output_args is date in the form 'yyyy.mm.dd.HH.MM.SS.FFF'
%
% G. Sterbini
%
output_args=datestr(unixtime2mat(input_args),'yyyy.mm.dd.HH.MM.SS.FFF');
end

