::
:: Script to start matlab using the current JAPC libraries in Windows
::
:: davide.gamba@cern.ch

:: First go to the current directory where javaclasspath.txt is
:: The best would be to use:
::cd %~dp0
:: ... but it doesn't work because the libraries are installed over dfs, i.e.
::     \\cern.ch\dfs\Users\c\ctf3op\JavaCoInterfave2\
::     and cmd.exe is not able to go to such a directory...
:: Trying instead to assume \\cern.ch\dfs is mounted on G:
G:
cd G:\\Users\c\ctf3op\JavaCoInterfave2_1.4.4\

::
:: start matlab (hoping that a matlab 2013b+ is installed...)
matlab
